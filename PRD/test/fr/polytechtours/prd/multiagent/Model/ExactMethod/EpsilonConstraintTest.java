package fr.polytechtours.prd.multiagent.Model.ExactMethod;

import java.io.File;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import fr.polytechtours.prd.multiagent.Model.Modelisation.Instance;
import fr.polytechtours.prd.multiagent.Model.Modelisation.ParetoSolution;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * 
 * Test case to test epsilon constraint algorithm to verify if the implementation gives the correct solutions.<br>
 * One instance of test with 8 jobs where 50% jobs belong to agent A is used.<br>
 * The optimal pareto solutions are got and verified before:
 * <ul>
 * <li>A: 0, B: 2</li>
 * <li>A: 2, B: 1</li>
 * </ul>
 * 
 * 
 * @author  Kake Abdoulaye
 * @version 1.0
 * @since 24 Mars, 2018 modified by kake : February 18, 2019
 *
 */
public class EpsilonConstraintTest {
	/**
	 * pareto front
	 */
	Set<ParetoSolution> front = new HashSet<ParetoSolution>();
	/**
	 * data to use
	 */
	Instance data;
	/**
	 * optimal solutions verified
	 */
	int[][] optimalSolutions;

	@Before
	public void setUp() throws Exception {
		data = new Instance("Ressource/InstancesTest/instance-8-2-3-1_2agent_2machine.data");

		File file = new File("Ressource/Resultat/MIPtemps_EpsilonContrainteTest.data");
		if(file.exists()) file.delete();

		EpsilonConstraint epsilonContraint = new EpsilonConstraint(data,"Ressource/Resultat/MIPtemps_EpsilonContrainteTest.data");
		front = epsilonContraint.generateParetoFront();
		
		// optimal pareto solutions for this instance of data
		optimalSolutions = new int[2][2];
		optimalSolutions[0][0] = 0;
		optimalSolutions[0][1] = 2;
		
		optimalSolutions[1][0] = 2;
		optimalSolutions[1][1] = 1;

	}

	@Test
	public void testMethodeExacte() {
		assertEquals(2, front.size());
		
		Iterator<ParetoSolution> iter = front.iterator();
		while(iter.hasNext()){
			ParetoSolution solution = iter.next();
			boolean exist = false;
			for(int i=0; i<2; i++){
				if(solution.valueObjA == optimalSolutions[i][0] && solution.valueObjB == optimalSolutions[i][1]){
					exist = true;
					break;
				}
			}
			assertTrue(exist);
		}
	}

}
