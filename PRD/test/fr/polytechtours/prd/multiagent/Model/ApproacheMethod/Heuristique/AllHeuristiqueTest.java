package fr.polytechtours.prd.multiagent.Model.ApproacheMethod.Heuristique;

import fr.polytechtours.prd.multiagent.Model.Modelisation.Instance;
import fr.polytechtours.prd.multiagent.Model.Modelisation.ParetoSolution;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import static org.junit.Assert.*;

/**
 * Cette classe test permet de tester les résultats obtenus par les différents heuristiques
 * du programme
 *
 *
 * @author kake Abdoulaye
 * @version 1.0
 * @since 19 mars 2018
 *
 */
public class AllHeuristiqueTest {
    ArrayList<String> listeNomHeuristique;
    ArrayList<Set<ParetoSolution>> listeFront ;
    ArrayList<int [][]> listeResulatHeuristique;
    Instance data;
    SchedulingMethod.Sorting sort = null;
    SchedulingMethod.Affectation affectation1 = null;
    SchedulingMethod.Affectation affectation2 = null;
    @Before
    public void setUp() throws Exception {
        listeNomHeuristique = new ArrayList<>(12);
        listeFront = new ArrayList<>(12);
        data = new Instance("Ressource/InstancesTest/instance-8-2-3-1_2agent_2machine.data");
        listeResulatHeuristique = new ArrayList<>(3);

        String nomheuristique ="";
        for(int trie=0;trie<3;trie++)
        {
            if(trie==0) sort = SchedulingMethod.Sorting.MakeSpan;
            if(trie==1) sort = SchedulingMethod.Sorting.Cmax;
            if(trie==2) sort = SchedulingMethod.Sorting.ShotestTime;

            for(int affect1=0;affect1<2;affect1++)
            {
                if(affect1==0) affectation1 = SchedulingMethod.Affectation.resolutionParMachine;
                if(affect1 ==1) affectation1 = SchedulingMethod.Affectation.resolutionParMachineMoinsCharger;
                for(int affect2=0;affect2<2;affect2++)
                {
                    if(affect2==0) affectation2 = SchedulingMethod.Affectation.resolutionParMachine;
                    if(affect2 ==1) affectation2 = SchedulingMethod.Affectation.resolutionParMachineMoinsCharger;

                    nomheuristique = "Heuristique_Tri_"+sort.toString()+"_Affectataion1_"+affectation1.toString()+"_Affectation2_"+affectation2.toString();
                    File file = new File("Ressource/Resultat/"+nomheuristique+".data");
                    if(file.exists()) file.delete();

                    SchedulingMethod  ms = new SchedulingMethod(data, sort,
                            nomheuristique,
                            affectation1,
                            affectation2,"Ressource/Resultat/"+nomheuristique+".data");
                    Set<ParetoSolution> front = new HashSet<ParetoSolution>();
                    front = ms.generateParetoFront();
                    listeFront.add(front);
                    listeNomHeuristique.add("Ressource/Resultat/"+nomheuristique+".data");

                }
            }
        }
        int solution1[][] = { {0,2},{2,1} };
        int solution2[][] = { {4,1},{0,2} };
        int solution3[][] = { {0,2},{3,1} };

        listeResulatHeuristique.add(solution1);
        listeResulatHeuristique.add(solution2);
        listeResulatHeuristique.add(solution3);



    }

    @Test
    public void testHeuristique_Tri_MakeSpan_Affectataion1_resolutionParMachine_Affectation2_resolutionParMachine() {
        int [][] optimalSolutions =listeResulatHeuristique.get(1);
        assertEquals(2, listeFront.get(0).size());
        Iterator<ParetoSolution> iter = listeFront.get(0).iterator();
        while(iter.hasNext()){
            ParetoSolution solution = iter.next();
            boolean exist = false;
            for(int i=0; i<2; i++){
                if(solution.valueObjA == optimalSolutions[i][0] && solution.valueObjB == optimalSolutions[i][1]){
                    exist = true;
                    break;
                }
            }
            assertTrue(exist);
        }

    }
    @Test
    public void testHeuristique_Tri_MakeSpan_Affectataion1_resolutionParMachine_Affectation2_resolutionParMachineMoinsCharger() {
        int [][] optimalSolutions =listeResulatHeuristique.get(2);
        assertEquals(2, listeFront.get(1).size());
        Iterator<ParetoSolution> iter = listeFront.get(1).iterator();
        while(iter.hasNext()){
            ParetoSolution solution = iter.next();
            boolean exist = false;
            for(int i=0; i<2; i++){
                if(solution.valueObjA == optimalSolutions[i][0] && solution.valueObjB == optimalSolutions[i][1]){
                    exist = true;
                    break;
                }
            }
            assertTrue(exist);
        }

    }
    @Test
    public void testHeuristique_Tri_MakeSpan_Affectataion1_resolutionParMachineMoinsCharger_Affectation2_resolutionParMachine() {
        int [][] optimalSolutions =listeResulatHeuristique.get(1);
        assertEquals(2, listeFront.get(2).size());
        Iterator<ParetoSolution> iter = listeFront.get(2).iterator();
        while(iter.hasNext()){
            ParetoSolution solution = iter.next();
            boolean exist = false;
            for(int i=0; i<2; i++){
                if(solution.valueObjA == optimalSolutions[i][0] && solution.valueObjB == optimalSolutions[i][1]){
                    exist = true;
                    break;
                }
            }
            assertTrue(exist);
        }


    }
    @Test
    public void testHeuristique_Tri_MakeSpan_Affectataion1_resolutionParMachineMoinsCharger_Affectation2_resolutionParMachineMoinsCharger() {
        int [][] optimalSolutions =listeResulatHeuristique.get(0);
        assertEquals(2, listeFront.get(3).size());
        Iterator<ParetoSolution> iter = listeFront.get(3).iterator();
        while(iter.hasNext()){
            ParetoSolution solution = iter.next();
            boolean exist = false;
            for(int i=0; i<2; i++){
                if(solution.valueObjA == optimalSolutions[i][0] && solution.valueObjB == optimalSolutions[i][1]){
                    exist = true;
                    break;
                }
            }
            assertTrue(exist);
        }


    }
    @Test
    public void testHeuristique_Tri_Cmax_Affectataion1_resolutionParMachine_Affectation2_resolutionParMachine() {
        int [][] optimalSolutions =listeResulatHeuristique.get(1);
        assertEquals(2, listeFront.get(4).size());
        Iterator<ParetoSolution> iter = listeFront.get(4).iterator();
        while(iter.hasNext()){
            ParetoSolution solution = iter.next();
            boolean exist = false;
            for(int i=0; i<2; i++){
                if(solution.valueObjA == optimalSolutions[i][0] && solution.valueObjB == optimalSolutions[i][1]){
                    exist = true;
                    break;
                }
            }
            assertTrue(exist);
        }


    }
    @Test
    public void testHeuristique_Tri_Cmax_Affectataion1_resolutionParMachine_Affectation2_resolutionParMachineMoinsCharger() {
        int [][] optimalSolutions =listeResulatHeuristique.get(2);
        assertEquals(2, listeFront.get(5).size());
        Iterator<ParetoSolution> iter = listeFront.get(5).iterator();
        while(iter.hasNext()){
            ParetoSolution solution = iter.next();
            boolean exist = false;
            for(int i=0; i<2; i++){
                if(solution.valueObjA == optimalSolutions[i][0] && solution.valueObjB == optimalSolutions[i][1]){
                    exist = true;
                    break;
                }
            }
            assertTrue(exist);
        }


    }
    @Test
    public void testHeuristique_Tri_Cmax_Affectataion1_resolutionParMachineMoinsCharger_Affectation2_resolutionParMachine() {
        int [][] optimalSolutions =listeResulatHeuristique.get(1);
        assertEquals(2, listeFront.get(6).size());
        Iterator<ParetoSolution> iter = listeFront.get(6).iterator();
        while(iter.hasNext()){
            ParetoSolution solution = iter.next();
            boolean exist = false;
            for(int i=0; i<2; i++){
                if(solution.valueObjA == optimalSolutions[i][0] && solution.valueObjB == optimalSolutions[i][1]){
                    exist = true;
                    break;
                }
            }
            assertTrue(exist);
        }


    }
    @Test
    public void testHeuristique_Tri_Cmax_Affectataion1_resolutionParMachineMoinsCharger_Affectation2_resolutionParMachineMoinsCharger() {
        int [][] optimalSolutions =listeResulatHeuristique.get(0);
        assertEquals(2, listeFront.get(7).size());
        Iterator<ParetoSolution> iter = listeFront.get(7).iterator();
        while(iter.hasNext()){
            ParetoSolution solution = iter.next();
            boolean exist = false;
            for(int i=0; i<2; i++){
                if(solution.valueObjA == optimalSolutions[i][0] && solution.valueObjB == optimalSolutions[i][1]){
                    exist = true;
                    break;
                }
            }
            assertTrue(exist);
        }


    }
    @Test
    public void testHeuristique_Tri_ShotestTime_Affectataion1_resolutionParMachine_Affectation2_resolutionParMachine() {
        int [][] optimalSolutions =listeResulatHeuristique.get(1);
        assertEquals(2, listeFront.get(8).size());
        Iterator<ParetoSolution> iter = listeFront.get(8).iterator();
        while(iter.hasNext()){
            ParetoSolution solution = iter.next();
            boolean exist = false;
            for(int i=0; i<2; i++){
                if(solution.valueObjA == optimalSolutions[i][0] && solution.valueObjB == optimalSolutions[i][1]){
                    exist = true;
                    break;
                }
            }
            assertTrue(exist);
        }


    }
    @Test
    public void testHeuristique_Tri_ShotestTime_Affectataion1_resolutionParMachine_Affectation2_resolutionParMachineMoinsCharger() {
        int [][] optimalSolutions =listeResulatHeuristique.get(2);
        assertEquals(2, listeFront.get(9).size());
        Iterator<ParetoSolution> iter = listeFront.get(9).iterator();
        while(iter.hasNext()){
            ParetoSolution solution = iter.next();
            boolean exist = false;
            for(int i=0; i<2; i++){
                if(solution.valueObjA == optimalSolutions[i][0] && solution.valueObjB == optimalSolutions[i][1]){
                    exist = true;
                    break;
                }
            }
            assertTrue(exist);
        }


    }
    @Test
    public void testHeuristique_Tri_ShotestTime_Affectataion1_resolutionParMachineMoinsCharger_Affectation2_resolutionParMachine() {
        int [][] optimalSolutions =listeResulatHeuristique.get(1);
        assertEquals(2, listeFront.get(10).size());
        Iterator<ParetoSolution> iter = listeFront.get(10).iterator();
        while(iter.hasNext()){
            ParetoSolution solution = iter.next();
            boolean exist = false;
            for(int i=0; i<2; i++){
                if(solution.valueObjA == optimalSolutions[i][0] && solution.valueObjB == optimalSolutions[i][1]){
                    exist = true;
                    break;
                }
            }
            assertTrue(exist);
        }


    }
    @Test
    public void testHeuristique_Tri_ShotestTime_Affectataion1_resolutionParMachineMoinsCharger_Affectation2_resolutionParMachineMoinsCharger() {
        int [][] optimalSolutions =listeResulatHeuristique.get(0);
        assertEquals(2, listeFront.get(11).size());
        Iterator<ParetoSolution> iter = listeFront.get(11).iterator();
        while(iter.hasNext()){
            ParetoSolution solution = iter.next();
            boolean exist = false;
            for(int i=0; i<2; i++){
                if(solution.valueObjA == optimalSolutions[i][0] && solution.valueObjB == optimalSolutions[i][1]){
                    exist = true;
                    break;
                }
            }
            assertTrue(exist);
        }
    }
}
