package fr.polytechtours.prd.multiagent.Model.ApproacheMethod.Heuristique;

import fr.polytechtours.prd.multiagent.Model.Modelisation.Instance;
import fr.polytechtours.prd.multiagent.Model.Modelisation.ParetoSolution;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;
/**
 * Cette classe test permet de vérifier les méthodes d'affectation des jobs.
 *
 *
 * @author kake Abdoulaye
 * @version 1.0
 * @since 19 mars 2018
 *
 */
public class SchedulingMethodTest {

    /**
     * Class to test
     */
    SchedulingMethod ms;
    /**
     * pareto front
     */
    Set<ParetoSolution> front = new HashSet<ParetoSolution>();
    /**
     * data to use
     */
    Instance data;

    @Before
    public void setUp() throws Exception {
        data = new Instance("Ressource/InstancesTest/instance-8-2-3-1_2agent_2machine.data");
        ms = new SchedulingMethod(data, SchedulingMethod.Sorting.MakeSpan);
    }

    @Test
    public void resolveMachineLessUsedMachine() {
        ArrayList<ArrayList<Integer>> jobsParMAchine= ms.affectationJobs(data.jobs, SchedulingMethod.Affectation.resolutionParMachineMoinsCharger);
        // verifier que le job 3 est exécuter sur la machine 1
        assertTrue(2 == jobsParMAchine.get(0).get(1));
        // verifier que le job 8 est exécuter sur la machine 2
        assertTrue(7 == jobsParMAchine.get(1).get(2));


    }

    @Test
    public void resolveByMachine() {
        ArrayList<ArrayList<Integer>> jobsParMAchine= ms.affectationJobs(data.jobs, SchedulingMethod.Affectation.resolutionParMachine);
        // verifier que le job 1 est exécuter sur la machine 1
        assertTrue(0 == jobsParMAchine.get(0).get(0));

        // verifier que le job 5 est exécuter sur la machine 2
        assertTrue(4 == jobsParMAchine.get(1).get(0));

    }


}