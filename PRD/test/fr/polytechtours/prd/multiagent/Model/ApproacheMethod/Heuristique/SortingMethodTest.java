package fr.polytechtours.prd.multiagent.Model.ApproacheMethod.Heuristique;

import fr.polytechtours.prd.multiagent.Model.Modelisation.Instance;
import fr.polytechtours.prd.multiagent.Model.Modelisation.Job;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;
/**
 * Cette classe test permet de vérifier les méthodes de triage ds jobs.
 *
 *
 * @author kake Abdoulaye
 * @version 1.0
 * @since 19 mars 2018
 *
 */
public class SortingMethodTest {

    Instance data ;
    SortingMethod method ;

    @Before
    public void setUp() throws Exception {
        data = new Instance("Ressource/InstancesTest/instance-8-2-3-1_2agent_2machine.data");
        method = new SortingMethod(data.jobs);
    }

    @Test
    public void trierCCmaxSommeRessources() {
        ArrayList <Job> tableauJobs = method.trierCCmaxSommeRessources();
        assertEquals(0,tableauJobs.get(0).id);
        assertEquals(2,tableauJobs.get(2).id);
        assertEquals(7,tableauJobs.get(7).id);
    }

    @Test
    public void trierCCmaxMaxRessources() {
        ArrayList <Job> tableauJobs = method.trierCCmaxMaxRessources();
        assertEquals(1,tableauJobs.get(1).id);
        assertEquals(4,tableauJobs.get(4).id);
        assertEquals(6,tableauJobs.get(6).id);
    }

    @Test
    public void trierDuree() {
        ArrayList <Job> tableauJobs = method.trierDuree();
        assertEquals(3,tableauJobs.get(3).id);
        assertEquals(7,tableauJobs.get(7).id);
        assertEquals(2,tableauJobs.get(2).id);
    }
}