package fr.polytechtours.prd.multiagent.Controler;

import fr.polytechtours.prd.multiagent.Model.Util.Commun;
import fr.polytechtours.prd.multiagent.View.GenerationInstance;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
/**
 * Cette classe est le contrôleur de la vue Generation d'Instance, elle permet de gérer les actions de
 * la vue.
 * Elle fait appel au model pour la création d'instance en suivant les paramètres saisie par l'utilisateur.
 *
 * @author Kake Abdoulaye
 * @version 2.0
 * @since February 18, 2019
 */
public class GenerationInstanceControler  implements ActionListener {

    /**
     * Attribut de la vue
     */
    private GenerationInstance vue ;

    /**
     * Constructeur de la classe
     * @param vue , la vue à gérer
     */
    public GenerationInstanceControler(GenerationInstance vue)
    {
        this.vue = vue;
        this.vue.getOkButton().addActionListener(this);
        this.vue.getCancelButton().addActionListener(this);
    }

    /**
     * Cette méthode permet d'afficher une vue
     */
    public  void affiche()
    {
        this.vue.setVisible(true);
    }

    /**
     * Cette méthode permet de fermer la fénètre
     */
    private void cancelButtonActionPerformed() {
            vue.dispose();
    }

    /**
     * Cette méthode permet de recupérer les valeurs des paramètres saisis par l'utilisateur après avoir cliquer
     * sur le bouton Ok.
     */
    private void okButtonActionPerformed() {

        int nombreInstance = (int) vue.getSpinnerNombreInstance().getValue();
        int nombreJobs = (int) vue.getSpinnerNombreJobs().getValue();
        int nombreMachine = (int) vue.getSpinnerNombreMachine().getValue();
        int nombreRessource = (int) vue.getSpinnerNombreRessource().getValue();
        int pourcentageA = (int) vue.getSpinnerPourcentageAgentA().getValue();
        int horizon = 0; // l'horizon sera toujours convertit en seconde
        if(vue.getComboBoxChoixUnite().getSelectedItem().toString() == "secondes")
        {
            horizon = (int) vue.getSpinnerHorizon().getValue();
        }
        if(vue.getComboBoxChoixUnite().getSelectedItem().toString() == "minutes")
        {
            horizon = (int) vue.getSpinnerHorizon().getValue() * 60;

        }
        if(vue.getComboBoxChoixUnite().getSelectedItem().toString() == "heures")
        {
            horizon = (int) vue.getSpinnerHorizon().getValue() * 3600;
        }

        execute(nombreInstance,nombreJobs,nombreMachine,nombreRessource,pourcentageA,horizon);

    }

    /**
     * Cette méthode permet de lancer un thread qui crée un fichier d'instance respectant les paramètres.
     * @param nombreInstance , le nombre d'instance
     * @param nombreJobs , le nombre de jobs
     * @param nombreMachine ,  le nombre de machine
     * @param nombreRessource , le nombre de ressource
     * @param pourcentageA , le pourcentage de nombre de job de l'agent A
     * @param horizon , l'horizon des dates de débuts et de fins
     */
    private void execute(int nombreInstance,int nombreJobs,int nombreMachine,int nombreRessource,int pourcentageA, int horizon)
    {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                Commun.createInstanceFile(nombreInstance,nombreJobs,nombreRessource,pourcentageA,nombreMachine,horizon);
            }
        });
        thread.start();
        JOptionPane.showMessageDialog(this.vue,"Fin de la génération d'instances","Information",JOptionPane.INFORMATION_MESSAGE);
       // cancelButtonActionPerformed();
    }

    @Override
    /**
     * Cette méthode permet de gérer les actions de la fénêtre.
     */
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == vue.getCancelButton())
        {
                cancelButtonActionPerformed();
        }
        else if(e.getSource() == vue.getOkButton())
        {
            okButtonActionPerformed();
        }

    }
}
