package fr.polytechtours.prd.multiagent.Controler;

import fr.polytechtours.prd.multiagent.Model.ApproacheMethod.Heuristique.SchedulingMethod;
import fr.polytechtours.prd.multiagent.Model.ApproacheMethod.MetaHeuristique.ModelisationMachine.NSGA2_M;
import fr.polytechtours.prd.multiagent.Model.ExactMethod.EpsilonConstraint;
import fr.polytechtours.prd.multiagent.Model.Modelisation.Instance;
import fr.polytechtours.prd.multiagent.View.ResolutionInstance;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

/**
 * Cette classe est le contrôleur de la vue Resolution d'Instance, elle permet de gérer les actions de
 * la vue. Plus précisement elle résoud en fonctions des méthodes choisis sur la vue un fichier d'instance
 *
 * @author Kake Abdoulaye
 * @version 2.0
 * @since February 18, 2019
 */
public class ResolutionInstanceControler implements ActionListener , DocumentListener {

    /**
     * Attribut de la vue à contrôler
     */
    private ResolutionInstance vue;

    /**
     * Le chenim du fichier à resoudre
     */
    private String cheminComplet = "";

    /**
     * Le nom du fichier à résoudre
     */
    private String nomFichier ="";

    /**
     * Le constructeur de la classe
     * @param vue , la vue de la résolution d'instance
     */

    public ResolutionInstanceControler(ResolutionInstance vue)
    {
        this.vue = vue;
        this.vue.getButtonAnnuler().addActionListener(this);
        this.vue.getButtonToutCocher().addActionListener(this);
        this.vue.getButtonToutDecocher().addActionListener(this);
        this.vue.getButtonChoisirFichier().addActionListener(this);
        this.vue.getTextFieldCheminFichierChoisi().getDocument().addDocumentListener(this);
        this.vue.getButtonResoudre().addActionListener(this);

    }

    /**
     * Cette méthode permet de fermer la vue
     */
    private void cancelButtonActionPerformed() {
        vue.dispose();
    }

    /**
     * Cette méthode permet de cocher tous les checkboxs de la vue
     */
    private void actionToutCocher()
    {
        if(!vue.getCheckBoxResIndexTemp().isSelected())
        {
            vue.getCheckBoxResIndexTemp().setSelected(true);
        }
        if(!vue.getCheckBoxResIndexJob().isSelected())
        {
            vue.getCheckBoxResIndexJob().setSelected(true);
        }
        if(!vue.getCheckBoxNSGA2().isSelected())
        {
            vue.getCheckBoxNSGA2().setSelected(true);
        }
        if(!vue.getCheckBoxTri1AffMBM().isSelected())
        {
            vue.getCheckBoxTri1AffMBM().setSelected(true);
        }
        if(!vue.getCheckBoxTri1AffPMC().isSelected())
        {
            vue.getCheckBoxTri1AffPMC().setSelected(true);
        }
        if(!vue.getCheckBoxTri2AffMBM().isSelected())
        {
            vue.getCheckBoxTri2AffMBM().setSelected(true);
        }
        if(!vue.getCheckBoxTri2AffPMC().isSelected())
        {
            vue.getCheckBoxTri2AffPMC().setSelected(true);
        }
        if(!vue.getCheckBoxTri3AffMBM().isSelected())
        {
            vue.getCheckBoxTri3AffMBM().setSelected(true);
        }
        if(!vue.getCheckBoxTri3AffPMC().isSelected())
        {
            vue.getCheckBoxTri3AffPMC().setSelected(true);
        }

    }

    /**
     * Cette méthode permet de décocher tous les checkboxs de la vue
     */
    private void actionToutDecocher()
    {
        if(vue.getCheckBoxResIndexTemp().isSelected() )
        {
            vue.getCheckBoxResIndexTemp().setSelected(false);
        }
        if(vue.getCheckBoxResIndexJob().isSelected())
        {
            vue.getCheckBoxResIndexJob().setSelected(false);
        }
        if(vue.getCheckBoxNSGA2().isSelected())
        {
            vue.getCheckBoxNSGA2().setSelected(false);
        }
        if(vue.getCheckBoxTri1AffMBM().isSelected())
        {
            vue.getCheckBoxTri1AffMBM().setSelected(false);
        }
        if(vue.getCheckBoxTri1AffPMC().isSelected())
        {
            vue.getCheckBoxTri1AffPMC().setSelected(false);
        }
        if(vue.getCheckBoxTri2AffMBM().isSelected())
        {
            vue.getCheckBoxTri2AffMBM().setSelected(false);
        }
        if(vue.getCheckBoxTri2AffPMC().isSelected())
        {
            vue.getCheckBoxTri2AffPMC().setSelected(false);
        }
        if(vue.getCheckBoxTri3AffMBM().isSelected())
        {
            vue.getCheckBoxTri3AffMBM().setSelected(false);
        }
        if(vue.getCheckBoxTri3AffPMC().isSelected())
        {
            vue.getCheckBoxTri3AffPMC().setSelected(false);
        }
    }

    /**
     * Cette méthode permet d'afficher la vue
     */
    public void affiche()
    {
        this.vue.setVisible(true);
    }

    /**
     * Cette méthode permet de choisir un fichier d'instance
     */
    private void actionChoisirFichier()
    {
        //System.getProperty("user.dir")+"/Ressource/Instances/"
        JFileChooser choix = new JFileChooser(System.getProperty("user.dir")+"/Ressource/Instances/");
        int result = 0;

        FileFilter filtre = new FileFilter("Fichier  Instance  (*.data)",".data");
        choix.addChoosableFileFilter(filtre);
        choix.setFileFilter(filtre);
        choix.setFileSelectionMode(JFileChooser.FILES_ONLY);
        choix.setMultiSelectionEnabled(false);
        result = choix.showOpenDialog(vue);

        if(result == JFileChooser.APPROVE_OPTION)
        {
            cheminComplet = choix.getSelectedFile().getPath();
            this.nomFichier = choix.getSelectedFile().getName();
            vue.getTextFieldCheminFichierChoisi().setText(cheminComplet);
        }

    }

    /**
     * Cette méthode permet de vérifier si un des checkboxs  est sélectionné
     * @return
     */
    private boolean checkCheckBox()
    {
       return vue.getCheckBoxResIndexTemp().isSelected() ||
        vue.getCheckBoxResIndexJob().isSelected() ||
        vue.getCheckBoxNSGA2().isSelected() ||
        vue.getCheckBoxTri1AffMBM().isSelected()||
        vue.getCheckBoxTri1AffPMC().isSelected() ||
        vue.getCheckBoxTri2AffMBM().isSelected() ||
        vue.getCheckBoxTri2AffPMC().isSelected() ||
        vue.getCheckBoxTri3AffMBM().isSelected() ||
        vue.getCheckBoxTri3AffPMC().isSelected();
    }

    /**
     * Cette méthode d'afficher des méssage d'erreur en cas de non conformité ou de non cochage d'une méthode
     */
    private void actionResoudre()
    {
        if(!checkCheckBox())
        {
            JOptionPane.showMessageDialog(this.vue,"Veillez cocher une des fonctionnalitées !!!","Erreur",JOptionPane.ERROR_MESSAGE);

        }
        if(vue.getTextFieldCheminFichierChoisi().getText() == null||vue.getTextFieldCheminFichierChoisi().getText().trim().length() == 0)
        {
            JOptionPane.showMessageDialog(this.vue,"Veillez choisir un fichier d'instance !!!","Erreur",JOptionPane.ERROR_MESSAGE);
        }
        else
        {
            String methode = methodeResolution();
            execute(methode);
        }

    }
    /**
     * Cette méthode permet de resoudre une instance en fonctions des fonctionnalités cocher
     */
    private void execute(String methode)
    {
        String[] listeMethode = null;
        String dossier = "Ressource/Resultat/";
        File repertoireDossier = new File(dossier);
        boolean exit = repertoireDossier.exists();
        boolean mkdirst;
        if(!exit)
        {
            mkdirst = repertoireDossier.mkdirs();
        }

        if(methode != null && methode.length()>0 )
        {
            listeMethode = methode.split("-");
            for(String fonc : listeMethode)
            {
                Instance data = new Instance(cheminComplet);
                String titre = this.nomFichier;
                String test = dossier+fonc+"-"+this.nomFichier;
                switch (fonc)
                {
                    case "MIPtemps":
                        Thread thread = new Thread(new Runnable() {
                            @Override
                            public void run() {
                                File file = new File(test);
                                if(file.exists()) file.delete();
                                EpsilonConstraint exact = new EpsilonConstraint(data,test);
                                exact.generateParetoFront();
                            }
                        });
                        thread.start();
                        ///home/kake/Documents/Projet_recherche_et_developpement/PRD_JAVA/prdordonnancementmulticriterejava/PRD/instance-8-2-3-1_2agent_2machine.data
                        break;
                    case "MIPJob":
                        Thread thread2 = new Thread(new Runnable() {
                            @Override
                            public void run() {

                            }
                        });
                        thread2.start();
                        break;
                    case "NSGA2":
                        Thread thread3 = new Thread(new Runnable() {
                            @Override
                            public void run() {
                                File file = new File(test);
                                if(file.exists()) file.delete();
                                NSGA2_M nsga2B = new NSGA2_M(data,test);
                                nsga2B.generateParetoFront();
                            }
                        });
                        thread3.start();
                        break;
                   case "Affectation1/trierCCmaxSommeRessources":
                        Thread thread4 = new Thread(new Runnable() {
                            @Override
                            public void run() {
                                String v = dossier+fonc.replace("/","-")+titre;
                                File file = new File(v);
                                if(file.exists()) file.delete();
                                SchedulingMethod ms = new SchedulingMethod(data, SchedulingMethod.Sorting.MakeSpan,"Heuristique : Trie par MakeSpan et Affectation par machine ", SchedulingMethod.Affectation.resolutionParMachine,SchedulingMethod.Affectation.resolutionParMachine,v);
                                ms.generateParetoFront();
                            }
                        });
                        thread4.start();
                        break;
                    case "Affectation2/trierCCmaxSommeRessources":
                        Thread thread5 = new Thread(new Runnable() {
                            @Override
                            public void run() {
                                String v = dossier+fonc.replace("/","-")+titre;
                                File file = new File(v);
                                if(file.exists()) file.delete();
                                SchedulingMethod ms = new SchedulingMethod(data, SchedulingMethod.Sorting.MakeSpan,"Heuristique : Trie par MakeSpan et Affectation par machine moins chargée", SchedulingMethod.Affectation.resolutionParMachineMoinsCharger,SchedulingMethod.Affectation.resolutionParMachineMoinsCharger,v);
                                ms.generateParetoFront();
                            }
                        });
                        thread5.start();
                        break;
                    case "Affectation1/trierCCmaxMaxRessources":
                        Thread thread6 = new Thread(new Runnable() {
                            @Override
                            public void run() {
                                String v = dossier+fonc.replace("/","-")+titre;
                                File file = new File(v);
                                if(file.exists()) file.delete();
                                SchedulingMethod ms = new SchedulingMethod(data, SchedulingMethod.Sorting.Cmax,"Heuristique : Trie par Cmax avec ressource maximale et Affectation par machine ", SchedulingMethod.Affectation.resolutionParMachine,SchedulingMethod.Affectation.resolutionParMachine,v);
                                ms.generateParetoFront();
                            }
                        });
                        thread6.start();
                        break;
                    case "Affectation2/trierCCmaxMaxRessources":
                        Thread thread7 = new Thread(new Runnable() {
                            @Override
                            public void run() {
                                String v = dossier+fonc.replace("/","-")+titre;
                                File file = new File(v);
                                if(file.exists()) file.delete();
                                SchedulingMethod ms = new SchedulingMethod(data, SchedulingMethod.Sorting.Cmax,"Heuristique : Trie par Cmax avec ressource maximale et Affectation par machine moins chargée ", SchedulingMethod.Affectation.resolutionParMachineMoinsCharger,SchedulingMethod.Affectation.resolutionParMachineMoinsCharger,v);
                                ms.generateParetoFront();
                            }
                        });
                        thread7.start();
                        break;
                    case "Affectation1/trierDuree":
                        Thread thread8 = new Thread(new Runnable() {
                            @Override
                            public void run() {
                                String v = dossier+fonc.replace("/","-")+titre;
                                File file = new File(v);
                                if(file.exists()) file.delete();
                                SchedulingMethod ms = new SchedulingMethod(data, SchedulingMethod.Sorting.ShotestTime,"Heuristique : Trie par Durée  et Affectation par machine ", SchedulingMethod.Affectation.resolutionParMachine,SchedulingMethod.Affectation.resolutionParMachine,v);
                                ms.generateParetoFront();
                            }
                        });
                        thread8.start();
                        break;
                    case "Affectation2/trierDuree":
                        Thread thread9 = new Thread(new Runnable() {
                            @Override
                            public void run() {
                                String v = dossier+fonc.replace("/","-")+titre;
                                File file = new File(v);
                                if(file.exists()) file.delete();
                                SchedulingMethod ms = new SchedulingMethod(data, SchedulingMethod.Sorting.ShotestTime,"Heuristique : Trie par Durée  et Affectation par machine moins chargée ", SchedulingMethod.Affectation.resolutionParMachineMoinsCharger,SchedulingMethod.Affectation.resolutionParMachineMoinsCharger,v);
                                ms.generateParetoFront();
                            }
                        });
                        thread9.start();
                        break;


                }
            }

            JOptionPane.showMessageDialog(this.vue,"Fin de la résolution d'instances","Information",JOptionPane.INFORMATION_MESSAGE);

        }

        // cancelButtonActionPerformed();
    }
    /**
     * @brief Fonction permetant de trouver la methode de résolution suivant les champs CheckBox remplis
     **/
    private  String methodeResolution()
    {
        String methode="";
        if(vue.getCheckBoxResIndexTemp().isSelected() )
        {
            methode +="MIPtemps";
        }
        if(vue.getCheckBoxResIndexJob().isSelected())
        {
            methode +="-MIPJob";
        }
        if(vue.getCheckBoxNSGA2().isSelected())
        {
            methode +="-NSGA2";
        }
        if(vue.getCheckBoxTri1AffMBM().isSelected())
        {
            methode +="-Affectation1/trierCCmaxSommeRessources";
        }
        if(vue.getCheckBoxTri1AffPMC().isSelected())
        {
            methode +="-Affectation2/trierCCmaxSommeRessources";
        }
        if(vue.getCheckBoxTri2AffMBM().isSelected())
        {
            methode +="-Affectation1/trierCCmaxMaxRessources";
        }
        if(vue.getCheckBoxTri2AffPMC().isSelected())
        {
            methode +="-Affectation2/trierCCmaxMaxRessources";
        }
        if(vue.getCheckBoxTri3AffMBM().isSelected())
        {
            methode +="-Affectation1/trierDuree";
        }
        if(vue.getCheckBoxTri3AffPMC().isSelected())
        {
            methode +="-Affectation2/trierDuree";
        }

        return methode;


    }
    /***
     * Cette méthode permet de lire le champs text pour la selection du fichier
     * et mettre à jour le chemin complet de l'instance choisie
     */
    private String lireValeue()
    {
        this.cheminComplet = this.vue.getTextFieldCheminFichierChoisi().getText();
        String tableau [] = this.cheminComplet.split("/");
        this.nomFichier = tableau[tableau.length-1];
        return this.cheminComplet;
        //System.out.println(cheminComplet);
    }

    /**
     * la gestion des différents evement enclachés par les buttons
     * @param e , l'évenement déclenché ou la source de l'action
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        if(e.getSource() == vue.getButtonAnnuler())
        {
            cancelButtonActionPerformed();
        }
        else if(e.getSource() == this.vue.getButtonToutCocher())
        {
            actionToutCocher();
        }
        else if(e.getSource() == this.vue.getButtonToutDecocher())
        {
            actionToutDecocher();
        }
        else if(e.getSource() == this.vue.getButtonChoisirFichier())
        {
            actionChoisirFichier();
        }
        else if(e.getSource() == this.vue.getButtonResoudre())
        {
            actionResoudre();
        }


    }

    /**
     *  Ces méthodes permet de gérer le chemin du fichier du fichier d'instance en cas d'insertion d'élément du champs text pour la selection du fichier
     * @param e , l'évenement déclenché ou la source de l'action
     */

    @Override
    public void insertUpdate(DocumentEvent e) {
        lireValeue();
    }

    /**
     *  Ces méthodes permet de gérer le chemin du fichier du fichier d'instance en cas de suppression d'élément du champs text pour la selection du fichier
     * @param e , la source du document
     */
    @Override
    public void removeUpdate(DocumentEvent e) {
        lireValeue();
    }

    /**
     *  Ces méthodes permet de gérer le chemin du fichier du fichier d'instance en cas de mise à jour d'élément du champs text pour la selection du fichier
     * @param e, l'évenement déclenché ou la source de l'action
     */
    @Override
    public void changedUpdate(DocumentEvent e) {
        lireValeue();
    }
}
