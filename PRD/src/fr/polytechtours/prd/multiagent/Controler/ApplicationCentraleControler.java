package fr.polytechtours.prd.multiagent.Controler;

import fr.polytechtours.prd.multiagent.View.ApplicationCentrale;
import fr.polytechtours.prd.multiagent.View.Auteur;
import fr.polytechtours.prd.multiagent.View.GenerationInstance;
import fr.polytechtours.prd.multiagent.View.ResolutionInstance;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
/**
 * Cette classe est le contrôleur de la vue Application Centrale, elle permet de gérer les actions de
 * la vue
 * <ol>
 * <li> Elle lance la vue Génération d'instance à travers le ménu qui va avec. </li>
 * <li> Elle lance la vue Resolution d'instance à travers le ménu qui va avec.</li>
 * </ol>
 * L'utilisateur doit choisir une des ménus et une action est générée.
 *
 * @author Kake Abdoulaye
 * @version 2.0
 * @since February 18, 2019
 */
public class ApplicationCentraleControler implements ActionListener {

    /**
     * Attribut de ApplicationCentrale
     */
    private ApplicationCentrale vue;

    /**
     *  Constructeur du contrôleur
     * @param vue , l'interface de l'application centrale
     */
    public ApplicationCentraleControler(ApplicationCentrale vue)
    {
        this.vue = vue;
        this.vue.getMenuItemGenerationInstance().addActionListener(this);
        this.vue.getMenuItemQuitter().addActionListener(this);
        this.vue.getMenuItemResolutionInstance().addActionListener(this);
        this.vue.getMenuItemComparaison().addActionListener(this);
        this.vue.getMenuAide().addActionListener(this);
        this.vue.getMenuItemAuteur().addActionListener(this);
    }

    /**
     * Affichage de la vue
     */
    public void affiche()
    {
        this.vue.setVisible(true);
    }

    /**
     * Cette méthode permet d'afficher la fenêtre de la génération d'instance
     */
    private void actionGenerationInstance()
    {
        GenerationInstance vue = new GenerationInstance(this.vue);
        GenerationInstanceControler controler = new GenerationInstanceControler(vue);
        controler.affiche();
    }

    /**
     * Cette méthode permet de quitter l'application
     */
    private void actionQuitter()
    {
        this.vue.dispose();
    }
    /**
     * Cette méthode permet d'afficher la fenêtre de la résolution d'instance
     */
    private void actionResolutionInstance()
    {
        ResolutionInstance vue = new ResolutionInstance(this.vue);
        ResolutionInstanceControler controler = new ResolutionInstanceControler(vue);
        controler.affiche();
    }

    @Override
    /**
     * Cette méthode permet de gérer les actions de la vue
     */
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == this.vue.getMenuItemGenerationInstance())
        {
            actionGenerationInstance();
        }
        else if(e.getSource() == this.vue.getMenuItemQuitter())
        {
            actionQuitter();
        }
        else if(e.getSource() == this.vue.getMenuItemResolutionInstance())
        {
            actionResolutionInstance();
        }
        else if(e.getSource() == this.vue.getMenuItemAuteur())
        {
            Auteur vue = new Auteur(this.vue);
            vue.setVisible(true);
        }

    }
}
