package fr.polytechtours.prd.multiagent.Controler;

import java.io.File;

/**
 * Cette classe est le filtre lors de la séclection d'un fichier
 * @author Kake Abdoulaye
 * @version 2.0
 * @since February 18, 2019
 */
public class FileFilter extends javax.swing.filechooser.FileFilter {

    /**
     * La description
     */
    private String description;
    /**
     * L'extension des fichiers acceptés
     */
    private String extension;

    /**
     * Le constructeur de la classe
     * @param desc , la description
     * @param ext , l'extension des fichiers acceptés
     */
    public FileFilter(String desc, String ext)
    {
        description = desc;
        extension = ext.toLowerCase();
    }

    @Override
    /**
     * cette méthode permet de savoir si un fichier est accepté
     */
    public boolean accept(File f) {
        return (f.isDirectory() || f.getName().toLowerCase().endsWith(extension));
    }

    @Override
    public String getDescription() {
        return description;
    }
}
