package fr.polytechtours.prd.multiagent.Model.ApproacheMethod.MetaHeuristique.CommonModelisation;

import fr.polytechtours.prd.multiagent.Model.IterfaceClass.IEvaluate;
import fr.polytechtours.prd.multiagent.Model.Modelisation.ParetoSolution;

import java.util.*;

/**
 * Cette classe contient les différents méthode de comparaison de deux ensembles de solutions donc deux front de Pareto ,
 *  dont la première vient de celle produite par l'agorithme exacte et la deuxième fournit par les heuristiques ou la méta heuristique.
 *
 *  Elle vise, à connaitre le distance moyenne en deux Pareto , le pourcentage de solution exacte trouvé et l'hypervolume entre deux
 *  front de pareto
 * <ol>
 * <li> calculer le pourcentage de solutions de Pareto optimalesobtenues en utilisant la méthode approchée. </li>
 * <li> calculer l’hypervolume entre deuxfronts de Pareto.</li>
 * <li> calculer la distance moyenne entre deuxf ronts de Pareto.</li>
 * </ol>
 * L'utilisateur doit choisir une des méthodes de comparaison.
 *
 * @author Kake Abdoulaye
 * @version 2.0
 * @since Mars 7, 2019
 */
public class ComparaisonSetPareto implements IEvaluate {
    /**
     * To calculate the mean distance between two pareto fronts.<br>
     * In order to get the mean distance:
     * <ol>
     * <li>For each point on the ApproacheMethod front, we try to find the nearest point from the ExactMethod front.</li>
     * <li>Calculate the distance of these two points and add to the total distance</li>
     * <li>Calculate the mean of the distance.</li>
     * </ol>
     *
     * @param frontExact ExactMethod front got by an ExactMethod algorithm
     * @param frontNSGA2 pareto front got by an ApproachMethod algorithm
     * @return the mean distance between two fronts
     */

    @Override
    public double getMeanDistance(Set<ParetoSolution> frontExact, Set<ParetoSolution> frontNSGA2 ) {
        //HashSet<ParetoSolution> frontNSGA2 = (HashSet<ParetoSolution>) this.paretoFront;

        // initiation of iterators
        Iterator<ParetoSolution> iterExact;
        Iterator<ParetoSolution> iterNSGA2 = frontNSGA2.iterator();
        double distanceTotal = 0.0;

        while(iterNSGA2.hasNext()){
            ParetoSolution solutionNSGA2 = iterNSGA2.next();
            double minDistance = Double.MAX_VALUE; // min distance
            iterExact = frontExact.iterator();
            while(iterExact.hasNext()){
                ParetoSolution solutionExact = iterExact.next();
                double distanceTemp = Math.sqrt(Math.pow((solutionNSGA2.valueObjA - solutionExact.valueObjA), 2) + Math.pow((solutionNSGA2.valueObjB - solutionExact.valueObjB), 2));
                if(distanceTemp < minDistance){ // if temp distance is smaller
                    minDistance = distanceTemp;
                }
            }

            distanceTotal += minDistance;
        }

        return (double)(distanceTotal / frontNSGA2.size());
    }

    /**
     * To calculate the percentage of optimal solutions found by the ApproacheMethod method
     *
     * @param frontExact ExactMethod front got by an ExactMethod algorithm
     * @param frontNSGA2 pareto front got by an ApproachMethod algorithm
     * @return percentage of optimal solutions found
     */
    @Override
    public double percentOptimalSolution(Set<ParetoSolution> frontExact,Set<ParetoSolution> frontNSGA2) {
        //HashSet<ParetoSolution> frontNSGA2 = (HashSet<ParetoSolution>) this.paretoFront;

        double numOptimal = 0.0;
        // initiation of iterators
        Iterator<ParetoSolution> iterExact = frontExact.iterator();
        Iterator<ParetoSolution> iterNSGA2;

        while(iterExact.hasNext()){
            ParetoSolution solutionExact = iterExact.next();
            iterNSGA2 = frontNSGA2.iterator();
            while(iterNSGA2.hasNext()){
                ParetoSolution solutionNSGA2 = iterNSGA2.next();
                // if the same solution found
                if(solutionNSGA2.valueObjA == solutionExact.valueObjA && solutionNSGA2.valueObjB == solutionExact.valueObjB){
                    numOptimal++;
                    break;
                }
            }
        }

        return (double)(numOptimal * 100 / frontExact.size());
    }

    /**
     * To calculate the hypervolume between two pareto fronts<br>
     * The hypervolume is the area between the two fronts which presents the interval of two fronts<br>
     * The lower this hypervolume is, the better the performance of this ApproacheMethod is.
     *
     * @param frontExact pareto front got by an ExactMethod algorithm
     * @param frontNSGA2 pareto front got by an ApproachMethod algorithm
     * @return the hypervolume between two fronts
     */
    @Override
    public double getHyperVolume(Set<ParetoSolution> frontExact,Set<ParetoSolution> frontNSGA2) {
        //HashSet<ParetoSolution> frontNSGA = (HashSet<ParetoSolution>) this.paretoFront;

        double hypervolume = 0.0;

        // sort the pareto solutions of front Exact
        List<ParetoSolution> listExact = new ArrayList<ParetoSolution>();
        Iterator<ParetoSolution> iterExact = frontExact.iterator();
        while(iterExact.hasNext()){
            ParetoSolution solutionExact = iterExact.next();
            listExact.add(solutionExact);
        }
        Collections.sort(listExact);

        // sort the pareto solutions of front NSGA
        List<ParetoSolution> listNSGA = new ArrayList<ParetoSolution>();
        Iterator<ParetoSolution> iterNSGA = frontNSGA2.iterator();
        while(iterNSGA.hasNext()){
            ParetoSolution solutionGreedy = iterNSGA.next();
            listNSGA.add(solutionGreedy);
        }
        Collections.sort(listNSGA);

        // calculate square of ExactMethod front
        double squareExact = 0.0;
        for(int i=0; i<listExact.size(); i++){
            if(i == 0){
                squareExact += listExact.get(i).valueObjA * listExact.get(i).valueObjB;
            }
            else{
                squareExact += (listExact.get(i).valueObjA - listExact.get(i-1).valueObjA) * listExact.get(i).valueObjB;
            }
        }

        // calculate square of nsga front
        double squareNSGA = 0.0;
        for(int i=0; i<listNSGA.size(); i++){
            if(i == 0){
                squareNSGA += listNSGA.get(i).valueObjA * listNSGA.get(i).valueObjB;
            }
            else{
                squareNSGA += (listNSGA.get(i).valueObjA - listNSGA.get(i-1).valueObjA) * listNSGA.get(i).valueObjB;
            }
        }

        // sub of two squares
        hypervolume = squareNSGA - squareExact;

        return hypervolume;
    }
}
