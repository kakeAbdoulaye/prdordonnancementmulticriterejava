package fr.polytechtours.prd.multiagent.Model.ApproacheMethod.MetaHeuristique.ModelisationMachine;

import fr.polytechtours.prd.multiagent.Model.ApproacheMethod.MetaHeuristique.CommonModelisation.ComparaisonSetPareto;
import fr.polytechtours.prd.multiagent.Model.ApproacheMethod.MetaHeuristique.MoelisationBinaire.NSGA2_B;
import fr.polytechtours.prd.multiagent.Model.ExactMethod.EpsilonConstraint;
import fr.polytechtours.prd.multiagent.Model.Modelisation.Instance;
import fr.polytechtours.prd.multiagent.Model.Modelisation.Job;
import fr.polytechtours.prd.multiagent.Model.Modelisation.Machine;
import fr.polytechtours.prd.multiagent.Model.Modelisation.ParetoSolution;
import fr.polytechtours.prd.multiagent.Model.Util.Commun;
import fr.polytechtours.prd.multiagent.Model.Util.Timer;

import java.io.File;
import java.util.*;

/**
 * Main Class with main function
 *
 * @author Kake Abdoulaye
 * @version 1.0
 * @since JAN 10, 2019
 *
 */
public class MainTest {

    /**
     * main function to create data and write into a file
     * @param args parameters
     */
    public static void main(String[] args) {
        Instance data = new Instance("Ressource/Instances/Dossier_1/NbJob_50/NbRes_3/NbMac_4/instance-1-50-3-4.data");

        File file = new File("Ressource/Resultat/MethodeExacteText2.data");
        if(file.exists()) file.delete();
        file = new File("Ressource/Resultat/NSGA2_TESTResult.data");
        if(file.exists()) file.delete();

        Timer timer = new Timer();
        timer.setStart(System.currentTimeMillis());
        NSGA2_M nsga2B = new NSGA2_M(data, "Ressource/Resultat/NSGA2_TESTResult.data");
       /*int valeur = 100;
        while(valeur != 0)
        {
            nsga2B.initGroup(data.jobs,data.nbJobs,data.nbJobsA);
            valeur --;
        }*/

       Set<ParetoSolution> front = nsga2B.generateParetoFront();

       timer.setEnd(System.currentTimeMillis());

        for(Iterator<ParetoSolution> iter = front.iterator(); iter.hasNext(); ){
            ParetoSolution solution = iter.next();

            StringBuilder builder = new StringBuilder("[");
            for(int i=0; i<solution.sequence.size(); i++){
                builder.append(solution.sequence.get(i)).append(",");
            }
            builder.deleteCharAt(builder.length() - 1);
            builder.append("]");
            System.out.println("------------------------------------------");
            System.out.println("Jobs to schedule: " + builder.toString());
            System.out.println("Number of jobs rejected of agent A: "+solution.valueObjA+" , "+"Number of jobs rejected of agent B: "+solution.valueObjB);
            System.out.println("------------------------------------------");
        }
        System.out.println("Time consumed: "+timer.calculateTimeConsume());
        EpsilonConstraint epsilonContraint = new EpsilonConstraint(data,"Ressource/Resultat/MethodeExacteText2.data");
        Set<ParetoSolution> frontExact = epsilonContraint.generateParetoFront();
        ComparaisonSetPareto cp = new ComparaisonSetPareto();
        System.out.println("Percentage of optimal solution found: "+String.format("%.2f", cp.percentOptimalSolution(frontExact,front))+"%");
        System.out.println("Mean distance between two pareto fronts: "+String.format("%.2f", cp.getMeanDistance(frontExact,front)));
        System.out.println("Hypervolume between two pareto fronts: "+String.format("%.1f", cp.getHyperVolume(frontExact,front)));

    }

}