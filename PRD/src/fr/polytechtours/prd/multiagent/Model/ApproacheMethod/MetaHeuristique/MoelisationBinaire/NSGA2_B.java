package fr.polytechtours.prd.multiagent.Model.ApproacheMethod.MetaHeuristique.MoelisationBinaire;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import fr.polytechtours.prd.multiagent.Model.IterfaceClass.IAlgorithm;
import fr.polytechtours.prd.multiagent.Model.ApproacheMethod.MetaHeuristique.CommonModelisation.CrowdingDistanceAssignment;
import fr.polytechtours.prd.multiagent.Model.ApproacheMethod.MetaHeuristique.CommonModelisation.NondominatedSetConstructor;
import fr.polytechtours.prd.multiagent.Model.Modelisation.Instance;
import fr.polytechtours.prd.multiagent.Model.Modelisation.Job;
import fr.polytechtours.prd.multiagent.Model.Modelisation.ParetoSolution;

/**
 * This class provides the main loop and several global methods for NSGA2_B.<br>
 * @author Boyang Wang
 * @version 1.0
 * @since Mars 10, 2018
 *
 */
public class NSGA2_B  implements IAlgorithm{
	/**
	 * data object with parameters
	 */
	public Instance data;
	/**
	 * pareto front
	 */
	public Set<ParetoSolution> paretoFront;

	/**
	 *  Le nom du fichier de résultat
	 */
	private String resultFile ;

	/**
	 * Contructeur
	 * @param data Instance
	 * @param nomFihcierResultat , le nom du fichier de resultat
	 */

	public NSGA2_B(Instance data, String nomFihcierResultat)
	{
		this.data = data;
		this.resultFile = nomFihcierResultat;
	}
	
	/*public ArrayList<Integer> initByGreedy(int epsilon){
		Greedy greedy = new Greedy();
		ArrayList<Job> sortedJobs = greedy.sortEpsilon(Instance.jobs);
		ArrayList<Job> solution = greedy.executeEpsilon(sortedJobs, Instance.nbJobsA, Instance.machine, epsilon);
		ArrayList<Integer> gene = new ArrayList<Integer>();
		for(int i=0; i<Instance.nbJobs; i++){
			boolean isIn = false;
			for(int j=0; j<solution.size(); j++){
				if(Instance.jobs.get(i).id == solution.get(j).id){
					gene.add(0);
					isIn = true;
					break;
				}
			}
			if(!isIn){
				gene.add(1);
			}
		}
		
		StringBuilder str = new StringBuilder("");
		for(int i=0; i<gene.size(); i++){
			str.append(gene.get(i)+",");
		}
		System.out.println("gene: " + str.toString());
		return gene;
	}*/

	/**
	 * Method to initialize the population<br>
	 * Here we use random strategy to initialize the genes of an individual:
	 * <ul>
	 * <li>for jobs of agent A, choose randomly one job as scheduled.</li>
	 * <li>for jobs of agent B, choose randomly one job as scheduled.</li>
	 * <li>to verify if this sequence of gene already exists</li>
	 * <li>if this gene already exists, re-do the random process. if not, add the individual into the population</li>
	 * </ul>
	 * We loop the process above until the population is filled
	 * @param jobs jobs to schedule
	 * @param nbJobs number of all jobs
	 * @param nbJobsA number of jobs of agent A
	 * @return population initialized
	 */

	public ArrayList<IndividualBinaire> initGroup(ArrayList<Job> jobs, int nbJobs, int nbJobsA){
		ArrayList<IndividualBinaire> group = new ArrayList<IndividualBinaire>();
		Random random = new Random();
		int size = 0;
		
		// if the population is not filled
		while(size < ConstantBinaire.SIZE_POPULATION){
			IndividualBinaire ind = new IndividualBinaire(data);
			
			// initialize the genes with 1
			for(int j=0; j<nbJobs; j++){
				ind.genes.add(1);
			}
			
			// choose two jobs randomly to schedule, one for agent A and one for agent B
			if(size > 0){
				int index = random.nextInt(nbJobsA);
				ind.genes.set(index, 0);
				index = nbJobsA + random.nextInt(nbJobs - nbJobsA);
				ind.genes.set(index, 0);
			}
			
			
			ind.calculateValueObj();
			ind.validate();
			
			boolean exist = true; // to verify if the same gene exists
			if(ind.valide){ // if the individual is valid
				if(group.size() == 0){
					group.add(ind);
					size++;
				}else{
					for(int k=0; k<group.size(); k++){
						exist = true;
						for(int iter=0; iter<nbJobs; iter++){
							if(ind.genes.get(iter) != group.get(k).genes.get(iter)){
								exist = false;
								break;
							}
						}
						if(exist)break;
					}
					
					if(!exist){
						size++;
						group.add(ind);
					}
				}
			}
			size++;
			group.add(ind);
		}
		
		return group;
	}
	
	/**
	 * selection operation to select one individual to attend the crossover
	 * <ol>
	 * <li>Select two individual p and q randomly in the current population</li>
	 * <li>If p dominates q, return p. Else return q.</li>
	 * </ol>
	 * @param pop population
	 * @return index of individual chosen
	 */

	public int selection(ArrayList<IndividualBinaire> pop){
		Random random = new Random();
		int p = random.nextInt(pop.size());
		int q = random.nextInt(pop.size());
		
		// choose the one which dominated the other
		if(NondominatedSetConstructor.pDomQ(pop.get(p), pop.get(q))){
			return p;
		}
		else if(NondominatedSetConstructor.pDomQ(pop.get(q), pop.get(p))){
			return q;
		}
		else{
			if(random.nextBoolean()){
				return p;
			}
			else{
				return q;
			}
		}
	}
	
	/**
	 * crossover operation to create new children<br>
	 * <ol>
	 * <li>Define the number of gene to cross over randomly</li>
	 * <li>Choose the index randomly of crossover</li>
	 * <li>For the index, do the crossover operation</li>
	 * </ol>
	 * @param dad dad individual
	 * @param mum mom individual
	 * @return two children created by crossover
	 */

	public IndividualBinaire[] crossOver(IndividualBinaire dad, IndividualBinaire mum){
		Random random = new Random();
		IndividualBinaire[] children = new IndividualBinaire[2];
		children[0] = new IndividualBinaire(data);
		children[1] = new IndividualBinaire(data);
		
		int numCross = 1+random.nextInt(dad.genes.size()-1); // number of genes to crossover
		
		// choose index to do the crossover
		ArrayList<Integer> index = new ArrayList<Integer>();
		while(index.size() < numCross){
			Integer pos = random.nextInt(dad.genes.size());
			if(!index.contains(pos)){
				index.add(pos);
			}
		}
		Collections.sort(index);
		
		// do the crossover
		if(random.nextDouble() < ConstantBinaire.PROB_CROSSOVER){
			for(int i=0; i<dad.genes.size(); i++){
				children[0].genes.add(dad.genes.get(i));
				children[1].genes.add(mum.genes.get(i));
			}
			for(int i=0; i<index.size(); i++){
				children[0].genes.set(index.get(i), mum.genes.get(index.get(i)));
				children[1].genes.set(index.get(i), dad.genes.get(index.get(i)));
			}
		}
		else{
			for(int i=0; i<dad.genes.size(); i++){
				children[0].genes.add(dad.genes.get(i));
				children[1].genes.add(mum.genes.get(i));
			}
		}
		
		
		
		//to calculate their values of objective functions
		children[0].calculateValueObj();
		children[1].calculateValueObj();
		
		return children;
	}
	
	/**
	 * Mutation operation<br>
	 * Choose randomly one position to do the mutation operation<br>
	 * Mutation means changing 1 to 0 and 0 to 1
	 * 
	 * @param children children created after crossover
	 * @return children after mutation
	 */
	public IndividualBinaire[] mutation(IndividualBinaire[] children){
		Random random = new Random();
		
		for(int index=0; index<children.length; index++){
			int pos = random.nextInt(children[0].genes.size()); // position to do the mutation
			if(random.nextDouble() < ConstantBinaire.PROB_MUTATION){
				if(children[index].genes.get(pos) == 1){
					children[index].genes.set(pos, 0);
				}else{
					children[index].genes.set(pos, 1);
				}
			}
			
			children[index].calculateValueObj();
		}
		
		// to verify the children's feasibility
		for(int i=0; i<children.length; i++){
			children[i].validate();
		}

		return children;
	}
	
	/**
	 * remove the duplicate individuals from the population
	 * 
	 * @param pop population
	 * @return population without duplicate
	 */

	public ArrayList<IndividualBinaire> removeDuplicateFromArray(ArrayList<IndividualBinaire> pop){
		for (int i = 0; i < pop.size() - 1; i++) {
			for (int j = pop.size() - 1; j > i; j--) {
				if (pop.get(j).valuesObj.get(0) == pop.get(i).valuesObj.get(0) && pop.get(j).valuesObj.get(1) == pop.get(i).valuesObj.get(1)) {
					pop.remove(j);
				}
			}
		}
		return pop;
	}
	
	/**
	 * print the result to console
	 * 
	 * @param pop population
	 * @param generation number of iteration
	 */
	public void outputInd(ArrayList<IndividualBinaire> pop, String generation){
		for(IndividualBinaire ind : pop){
			StringBuilder str = new StringBuilder("");
			str.append("Value A: ").append(ind.valuesObj.get(0)).append(" , ").append("Value B: ").append(ind.valuesObj.get(1));
			str.append(" ---- ");
			str.append(generation);
			System.out.println(str.toString());
			
		}
		System.out.println(generation + " finish");
	}
	
	/**
	 * Main loop for NSGA2_B<br>
	 * The algorithm consists of these steps:
	 * <ol>
	 * <li>Initiation of population</li>
	 * <li>Crossover, mutation</li>
	 * <li>Construct non dominated sets</li>
	 * <li>Calculate crowded distances</li>
	 * <li>Reconstruct the population�� if iteration not end, go to 2</li>
	 * </ol>
	 * @return hashmap with elements:
	 * <ul>
	 * <li>key:paretoFront, type of value:ArrayList of IndividualBinaire, value:pareto front</li>
	 * </ul>
	 */
	@Override
	public HashMap<String, Object> execute() {
		Random random = new Random();
		
		HashMap<String, Object> results = new HashMap<String, Object>();
		
		ArrayList<ArrayList<IndividualBinaire>> nds = null; // save the non dominated set
		
		ArrayList<IndividualBinaire> group = initGroup(data.jobs, data.nbJobs, data.nbJobsA); // initialize the population
		
		
		System.out.println("begin");
		for (int ng = 1; ng <= ConstantBinaire.NUM_ITERATION; ng++) // the start of algorithm
		{
			ArrayList<IndividualBinaire> tempGroup = new ArrayList<IndividualBinaire>(); // temporary population
			
			for (int i = 0; i < group.size(); i++) {
				tempGroup.add(group.get(i));
			}
			
			// System.out.println("\nNO. " + ng + ":");
			for (int i = 0; i < group.size(); i++) {// to the genetic operations
				int dad, mum;
				IndividualBinaire dadMoi = null;
				IndividualBinaire mumMoi = null;
				
				do {
					dad = selection(group);// selection using competition strategy
					mum = selection(group);
				} while (dad == mum); // when the dad and mom are the same
				
				dadMoi = group.get(dad);
				mumMoi = group.get(mum);
				
				IndividualBinaire[] temp = crossOver(dadMoi, mumMoi);// crossover
				temp = mutation(temp);// mutation
				
				for (int j = 0; j < 2; j++) {
					if (temp[j].valide && NondominatedSetConstructor.pDomQ(temp[j], group.get(dad))) {
						group.set(dad, temp[j]);
						break;
					}
					if (temp[j].valide && NondominatedSetConstructor.pDomQ(temp[j], group.get(mum))) {
						group.set(mum, temp[j]);
					}
				}
				
			} // end of one iteration

			group.addAll(tempGroup);// combine the two populations
			
			
			nds = NondominatedSetConstructor.sortB(group);// sort all individuals
			
			
			
			ArrayList<IndividualBinaire> newGroup = new ArrayList<IndividualBinaire>();
			
			int iteration = 0;
			while (iteration < ConstantBinaire.SIZE_POPULATION && (newGroup.size() + removeDuplicateFromArray(nds.get(iteration)).size() <= ConstantBinaire.SIZE_POPULATION) && (removeDuplicateFromArray(nds.get(iteration)).size() != 0)) {
				// when the new population are not all filled
				nds.set(iteration, removeDuplicateFromArray(nds.get(iteration)));
				newGroup.addAll(nds.get(iteration));
				iteration++;
			}
			
			int sizeTemp = newGroup.size();
			
			// if no more individual can be added into the population
			if(nds.get(iteration).size() == 0 && sizeTemp < ConstantBinaire.SIZE_POPULATION){
				int i=0, j=0;
				
				// if the current population are not filled
				while(newGroup.size() < ConstantBinaire.SIZE_POPULATION){
					
					// choose one individual randomly
					j = random.nextInt(nds.get(i).size());
					IndividualBinaire ind = new IndividualBinaire(data);
					ind.genes.addAll(nds.get(i).get(j).genes);
					while(true){
						int k = random.nextInt(ind.genes.size()); // choose one position
						if(ind.genes.get(k) == 0){ 
							ind.genes.set(k, 1); // do not schedule this job to make a new feasible individual 
							break;
						}
					}
					
					ind.calculateValueObj();
					newGroup.add(ind);
				}
			}else if(sizeTemp < ConstantBinaire.SIZE_POPULATION){ // when there are still individuals which can be added
				
				// calculate the crowded distances
				CrowdingDistanceAssignment.distanceCalculatorB(nds.get(iteration));
				
				nds.set(iteration, CrowdingDistanceAssignment.sortByDistanceB(nds.get(iteration)));
				int size = newGroup.size();
				
				//add individuals sorted by crowded distance until the population is filled
				for (int j = 0; j < ConstantBinaire.SIZE_POPULATION - size; j++) {
					newGroup.add(nds.get(iteration).get(j));
				}
			}
			
			
			group = newGroup;
			outputInd(nds.get(0), "G" + ng);
		} // all over;

		results.put("paretoFront", nds.get(0));
		return results;
	}



	@Override
	public Set<ParetoSolution> generateParetoFront() {
		paretoFront = new HashSet<ParetoSolution>();
		HashMap<String, Object> results = this.execute();
		ArrayList<IndividualBinaire> result = (ArrayList<IndividualBinaire>) results.get("paretoFront");
		for(int i=0; i<result.size(); i++){
			ParetoSolution paretoSolution = new ParetoSolution();
			paretoSolution.sequence.addAll(result.get(i).jobsOrdonnances);
			/*for(int m=0;m<result.get(i).jobsOrdonnances.size();m++)
			{
				//ArrayList<Integer> job = new ArrayList<>(result.get(i).jobsOrdonnances.get(m).size());
				for(int j=0; j<result.get(i).genes.size(); j++){
					if(result.get(i).genes.get(j) == 0){
						job.add((data.jobs.get(j).id)
						paretoSolution.sequence.add);
					}
				}

			}*/
			paretoSolution.valueObjA = result.get(i).valuesObj.get(0);
			paretoSolution.valueObjB = result.get(i).valuesObj.get(1);
			
			paretoFront.add(paretoSolution);
		}
		return paretoFront;
	}



	
	
}
