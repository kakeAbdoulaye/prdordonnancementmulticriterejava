package fr.polytechtours.prd.multiagent.Model.ApproacheMethod.Operation;

import java.util.ArrayList;
/**
 * Cette classe contient les fonctions qui fournit une valeur aléatoire
 * @author Kake Abdoulaye
 * @version 2.0
 * @since February 18, 2019
 */
public class RandomOp {

    /**
     * Cette méthode permet de choisir un element dans une liste d'entier de façon
     * aléatoire  et qu'elle soit différente une valeur choisit
     * @param listofMachineId , la liste d'entier
     * @param machineIdNotToUse , la valeur à eviter
     * @return l'element choisit aléatpirement dans la liste d'entier
     */
    public static Integer randomMachineId(ArrayList<Integer> listofMachineId, Integer machineIdNotToUse)
    {
        java.util.Random random = new java.util.Random();
        int index = 0 ;
        int machineid = 0;

        if(machineIdNotToUse == -1)
        {
            index = random.nextInt(listofMachineId.size());
            machineid = listofMachineId.get(index);
        }
        else
        {
            do {
                index = random.nextInt(listofMachineId.size());
                machineid = listofMachineId.get(index);
            } while (machineid == machineIdNotToUse); // when the id and id not to use  are the same
        }

        return machineid;
    }


    /**
     * Cette méthode permet de choisir un element dans une liste d'entier de façon
     * aléatoire
     * @param listofMachineId , la liste d'entier
     * @return l'element choisit aléatpirement dans la liste d'entier
     */
    public static Integer randomMachineId(ArrayList<Integer> listofMachineId)
    {
        return randomMachineId(listofMachineId,-1);
    }


}
