package fr.polytechtours.prd.multiagent.Model.ApproacheMethod.Heuristique;

import com.sun.media.sound.InvalidFormatException;
import fr.polytechtours.prd.multiagent.Model.ApproacheMethod.MetaHeuristique.CommonModelisation.ComparaisonSetPareto;
import fr.polytechtours.prd.multiagent.Model.ApproacheMethod.MetaHeuristique.ModelisationMachine.NSGA2_M;
import fr.polytechtours.prd.multiagent.Model.ExactMethod.EpsilonConstraint;
import fr.polytechtours.prd.multiagent.Model.Modelisation.Instance;
import fr.polytechtours.prd.multiagent.Model.Modelisation.ParetoSolution;
import fr.polytechtours.prd.multiagent.Model.Util.Timer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.ss.usermodel.*;

public class TestSorting {
    public static void main(String[] args){
        String excelFilePath="D:\\Users\\KAKEA\\Documents\\Cours\\Semestre_1\\Projet_recherche_et_developpement\\Tableau_Excel_Analyse\\MéthodeHeuristique.xlsx";
        String instanceName = "Ressource/Instances/Dossier_1/NbJob_60/NbRes_3/NbMac_10/instance-1-60-3-10.data";

        ArrayList<String> listeHeuristique = new ArrayList<>(14);
        ArrayList<Double> listeDuree = new ArrayList<>(14);
        ArrayList<Set<ParetoSolution>> listeFront = new ArrayList<>(14);

        //Ressource/Instances/Dossier_1/NbJob_20/NbRes_3/NbMac_4/instance-1-20-3-4.data
        Instance data = new Instance(instanceName);
        SchedulingMethod.Sorting sort = null;
        SchedulingMethod.Affectation affectation1 = null;
        SchedulingMethod.Affectation affectation2 = null;


        Timer timer = new Timer();
        for(int trie=0;trie<3;trie++)
        {
            if(trie==0) sort = SchedulingMethod.Sorting.MakeSpan;
            if(trie==1) sort = SchedulingMethod.Sorting.Cmax;
            if(trie==2) sort = SchedulingMethod.Sorting.ShotestTime;

            for(int affect1=0;affect1<2;affect1++)
            {
                if(affect1==0) affectation1 = SchedulingMethod.Affectation.resolutionParMachine;
                if(affect1 ==1) affectation1 = SchedulingMethod.Affectation.resolutionParMachineMoinsCharger;
                for(int affect2=0;affect2<2;affect2++)
                {
                    if(affect2==0) affectation2 = SchedulingMethod.Affectation.resolutionParMachine;
                    if(affect2 ==1) affectation2 = SchedulingMethod.Affectation.resolutionParMachineMoinsCharger;

                    File file = new File("Ressource/Resultat/Heuristique_Tri_"+sort.toString()+"_Affectataion1_"+affectation1.toString()+"_Affectation2_"+affectation2.toString()+".data");
                    if(file.exists()) file.delete();

                    timer.setStart(System.currentTimeMillis());
                    SchedulingMethod  ms = new SchedulingMethod(data, sort,
                            "Heuristique : Trie par Cmax avec ressource maximale et Affectation par machine ",
                            affectation1,
                            affectation2,"Ressource/Resultat/Heuristique_Tri_"+sort.toString()+"_Affectataion1_"+affectation1.toString()+"_Affectation2_"+affectation2.toString()+".data");
                    Set<ParetoSolution> front = new HashSet<ParetoSolution>();
                    front = ms.generateParetoFront();
                    timer.setEnd(System.currentTimeMillis());
                    listeFront.add(front);
                    listeHeuristique.add(sort.toString()+"_Affectataion1_"+affectation1.toString()+"_Affectation2_"+affectation2.toString());
                    listeDuree.add(timer.calculateTimeConsume());
                }
            }
        }

        timer.setStart(System.currentTimeMillis());
        NSGA2_M nsga2B = new NSGA2_M(data, "Ressource/Resultat/NSGA2_TESTResult.data");
        Set<ParetoSolution> front = nsga2B.generateParetoFront();
        timer.setEnd(System.currentTimeMillis());
        listeFront.add(front);
        listeHeuristique.add("NSGA2");
        listeDuree.add(timer.calculateTimeConsume());

        System.out.println("finie .....");

        System.out.println("---- Méthode Exacte : ");
        timer.setStart(System.currentTimeMillis());
        EpsilonConstraint epsilonContraint = new EpsilonConstraint(data,"Ressource/Resultat/MéthodeExactetest.data");
        Set<ParetoSolution> frontExact = epsilonContraint.generateParetoFront();
        timer.setEnd(System.currentTimeMillis());
        listeFront.add(frontExact);
        listeHeuristique.add("Méthode Exacte");
        listeDuree.add(timer.calculateTimeConsume());


        File file = new File("Ressource/Resultat/MéthodeExactetest.data");
        if(file.exists()) file.delete();

        try
        {
            File fichierExcel = new File(excelFilePath);
            FileInputStream inputStream = new FileInputStream(fichierExcel);
            Workbook workbook = WorkbookFactory.create(inputStream);
            Sheet feuille = workbook.createSheet(instanceName.split("/")[6]);
            int ligne = 0;
            Row ligneExcel = null;
            Cell celluleExcel = null;

            ligneExcel = feuille.createRow(ligne);
            celluleExcel = ligneExcel.createCell(0, CellType.STRING);
            celluleExcel.setCellValue("Méthode ");
            celluleExcel = ligneExcel.createCell(1, CellType.STRING);
            celluleExcel.setCellValue("A");
            celluleExcel = ligneExcel.createCell(2, CellType.STRING);
            celluleExcel.setCellValue("B");
            celluleExcel = ligneExcel.createCell(3, CellType.STRING);
            celluleExcel.setCellValue("Temps ");
            celluleExcel = ligneExcel.createCell(4, CellType.STRING);
            celluleExcel.setCellValue("%S opt");
            celluleExcel = ligneExcel.createCell(5, CellType.STRING);
            celluleExcel.setCellValue("HyperV");
            celluleExcel = ligneExcel.createCell(6, CellType.STRING);
            celluleExcel.setCellValue("Dist Mean");

            ComparaisonSetPareto cp = new ComparaisonSetPareto();
            for(int index = 0 ; index<listeFront.size()-1 ; index ++) {
                ligneExcel = feuille.createRow(++ligne);
                celluleExcel = ligneExcel.createCell(0, CellType.STRING);
                celluleExcel.setCellValue(listeHeuristique.get(index));
                System.out.println(" -- " + (listeHeuristique.get(index)) + "\n");
                int count = 0 ;
                for (Iterator<ParetoSolution> iter = listeFront.get(index).iterator(); iter.hasNext(); ) {
                    ligneExcel = feuille.createRow(++ligne);
                    ParetoSolution solution = iter.next();
                    celluleExcel = ligneExcel.createCell(1, CellType.NUMERIC);
                    celluleExcel.setCellValue(solution.valueObjA);
                    celluleExcel = ligneExcel.createCell(2, CellType.NUMERIC);
                    celluleExcel.setCellValue(solution.valueObjB);
                   if(count == 0)
                   {
                       celluleExcel = ligneExcel.createCell(3);
                       celluleExcel.setCellValue(listeDuree.get(index));
                       celluleExcel = ligneExcel.createCell(4);
                       celluleExcel.setCellValue(cp.percentOptimalSolution(frontExact, listeFront.get(index)));
                       celluleExcel = ligneExcel.createCell(5);
                       celluleExcel.setCellValue(cp.getHyperVolume(frontExact, listeFront.get(index)));
                       celluleExcel = ligneExcel.createCell(6);
                       celluleExcel.setCellValue(cp.getMeanDistance(frontExact, listeFront.get(index)));
                       count ++ ;
                   }

                }
            }
            ligneExcel = feuille.createRow(++ligne);
            celluleExcel = ligneExcel.createCell(0, CellType.STRING);
            celluleExcel.setCellValue(listeHeuristique.get(13));
            int count = 0 ;
            for (Iterator<ParetoSolution> iter = listeFront.get(13).iterator(); iter.hasNext(); ) {
                ligneExcel = feuille.createRow(++ligne);
                ParetoSolution solution = iter.next();
                celluleExcel = ligneExcel.createCell(1, CellType.NUMERIC);
                celluleExcel.setCellValue(solution.valueObjA);
                celluleExcel = ligneExcel.createCell(2, CellType.NUMERIC);
                celluleExcel.setCellValue(solution.valueObjB);
               if(count == 0)
               {
                   celluleExcel = ligneExcel.createCell(3);
                   celluleExcel.setCellValue(listeDuree.get(13));
                   count++;
               }
            }
            feuille.autoSizeColumn(0);
            feuille.autoSizeColumn(1);
            feuille.autoSizeColumn(2);
            feuille.autoSizeColumn(3);
            feuille.autoSizeColumn(4);
            feuille.autoSizeColumn(5);
            feuille.autoSizeColumn(6);
            FileOutputStream outFile = new FileOutputStream(fichierExcel);
            workbook.write(outFile);
            workbook.close();
            outFile.close();
        } catch (IOException | EncryptedDocumentException
                ex) {
            ex.printStackTrace();
        }



       /* for(Iterator<ParetoSolution> iter = frontExact.iterator(); iter.hasNext(); ){
            ParetoSolution solution = iter.next();
            StringBuilder builder = new StringBuilder("[");
            for(int i=0; i<solution.sequence.size(); i++){
                builder.append(solution.sequence.get(i)).append(",");
            }
            builder.deleteCharAt(builder.length() - 1);
            builder.append("]");
            System.out.println("------------------------------------------");
            System.out.println("Jobs to schedule: " + builder.toString());
            System.out.println("Number of jobs rejected of agent A: "+solution.valueObjA+" , "+"Number of jobs rejected of agent B: "+solution.valueObjB);
            System.out.println("------------------------------------------");
        }
        System.out.println("Time consumed: "+timer.calculateTimeConsume());*/





      /* for(int i =0;i<jobsParMAchine.size();i++)
       {
           for (int j=0;j<jobsParMAchine.get(i).size(); j++)
           {
               System.out.println("Machine : "+i+" Job : "+jobsParMAchine.get(i).get(j));
           }
       }*/
    }
}
