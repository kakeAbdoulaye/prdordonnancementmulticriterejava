package fr.polytechtours.prd.multiagent.Model.ApproacheMethod.MetaHeuristique.ModelisationMachine;

import fr.polytechtours.prd.multiagent.Model.IterfaceClass.Calculate;
import fr.polytechtours.prd.multiagent.Model.ApproacheMethod.MetaHeuristique.CommonModelisation.Individual;
import fr.polytechtours.prd.multiagent.Model.Modelisation.Instance;
import fr.polytechtours.prd.multiagent.Model.Util.Commun;

import java.util.*;

/**
 * IndividualMachine to simulation one solution of NSGA2_B<br>
 * One individual contains :
 * <ul>
 * <li>genes: its genes of size N with -1 for abandoned and Number of machine where the job is scheduled {0,1,2,....,m} </li>
 * <li>fitness: values of objective functions</li>
 * <li>number dominated: the number of individuals which dominates this individual</li>
 * <li>set dominant: set of individuals which this individual dominates</li>
 * <li>crowdedDistance: crowded distance</li>
 * <li>valid: if this individual is feasible or not</li>
 * </ul>
 * @author Kake Abdoulaye
 * @version 1.0
 * @since JAN 1, 2019
 *
 */
public class IndividualMachine extends Individual implements Calculate {

    /**
     * set of individuals which this individual dominates
     */
    public ArrayList<IndividualMachine> setDominant;
    /**
     * constructor
     * @param data data to use
     */
    public IndividualMachine(Instance data)
    {
        super(data);
        setDominant = new ArrayList<IndividualMachine>();

    }
    /**
     * to add one individual into the set dominant
     * @param ind individual
     */
    public void addIndividualDominant(IndividualMachine ind){
        this.setDominant.add(ind);
    }

    @Override
    /**
     * calculate the value of function
     */
    public void calculateValueObj() {
        valuesObj.clear();
        valuesObj.add(0);
        valuesObj.add(0);

        for(int i=0; i<data.nbJobs; i++){
            if(data.jobs.get(i).belongTo.equals("A")){
                if(genes.get(i) == -1|| valide == false){ // si l'element est différent de -1 (job non ordonnancé) ,je ne compte pas si le job est ordonnancé sur une machine 1,2,...,m où m est son numéro
                    valuesObj.set(0, valuesObj.get(0)+1);
                }
            }
            else{
                if(genes.get(i) == -1 || valide == false){
                    valuesObj.set(1, valuesObj.get(1)+1);
                }
            }
        }
    }

    @Override
    /**
     * to verify if the current solution is feasible
     */
    public void validate() {
        this.jobsOrdonnances.clear();

        //Déclaration de variable en local pour améliorer les performances des heuristiques
        int nombre_jobs = data.nbJobs;
        int nombre_ressource = data.machines.get(0).resources.size();
        ArrayList<Integer> s_j = Commun.getHeuresdebut(data);
        ArrayList<Integer> f_j = Commun.getHeuresFin(data);
        ArrayList<ArrayList<Integer>> capaciteRessourceMachine = Commun.getCapaciteRessourceMachine(data);
        ArrayList<ArrayList<Integer>> capaciteRessourceJob = Commun.getCapaciteRessourceJob(data);

        Map<Integer,ArrayList<Integer>> tableauJobs = new HashMap<>();
        int nombreJobAOrdonnance = 0 ;
        for(int index = 0 ; index <nombre_jobs; index++)
        {
            int machineId = genes.get(index);
            if(machineId>=0)
            {
                if(tableauJobs.containsKey(machineId))
                {
                    tableauJobs.get(machineId).add(index);
                }
                else
                {
                    ArrayList<Integer> jobs = new ArrayList<>();
                    jobs.add(index);
                    tableauJobs.put(machineId,jobs);
                }
                nombreJobAOrdonnance++;
            }
        }

        Set<Map.Entry<Integer, ArrayList<Integer>>> setHm = tableauJobs.entrySet();
        Iterator<Map.Entry<Integer, ArrayList<Integer>>> it = setHm.iterator();
        while(it.hasNext()){
            Map.Entry<Integer, ArrayList<Integer>> e = it.next();
            int idMachine = e.getKey();
            ArrayList<Integer> allJobMachine = e.getValue();
            ArrayList<Integer> jobsMachine = new ArrayList<>();
            ArrayList<Integer> Lm= new ArrayList<>(allJobMachine.size());
            for(int in =0 ; in < allJobMachine.size() ; in ++ )
            {
                Lm.add(allJobMachine.get(in));
            }
            while(Lm.size() != 0){
                ArrayList<Integer> Sm = new ArrayList<>();
                for(int i = 0; i < jobsMachine.size(); i++){
                    if(s_j.get(Lm.get(0)) < f_j.get(jobsMachine.get(i)) && f_j.get(Lm.get(0))> s_j.get(jobsMachine.get(i))){
                        Sm.add(jobsMachine.get(i));
                    }
                }

                boolean jobOrdonancable = true;

                for( int i = s_j.get(Lm.get(0)); i < f_j.get(Lm.get(0)); i++){
                    for( int r = 0; r < nombre_ressource ; r++){
                        int sommesRessources = 0;
                        for(int j = 0; j < Sm.size(); j++){
                            if(s_j.get(Sm.get(j)) <= i && f_j.get(Sm.get(j)) > i )
                                sommesRessources += capaciteRessourceJob.get(Sm.get(j)).get(r);
                        }
                        sommesRessources += capaciteRessourceJob.get(Lm.get(0)).get(r);
                        if(sommesRessources > capaciteRessourceMachine.get(idMachine).get(r))
                            jobOrdonancable = false;
                    }
                }

                if (jobOrdonancable){
                    jobsMachine.add(Lm.get(0));
                    tableauJobs.get(idMachine).remove(Lm.get(0));
                }
                Lm.remove(Lm.get(0));

            }

            jobsOrdonnances.add(jobsMachine);
        }
        int count = 0;

        for(int o =0 ; o < jobsOrdonnances.size() ; o++)
        {
            count += jobsOrdonnances.get(o).size();
        }
        //System.out.println("Nombre de job fait : "+count);
        if(count == nombreJobAOrdonnance)
        {
            this.valide = true;
        }
        else
        {
            this.valide = false;
        }
    }
}
