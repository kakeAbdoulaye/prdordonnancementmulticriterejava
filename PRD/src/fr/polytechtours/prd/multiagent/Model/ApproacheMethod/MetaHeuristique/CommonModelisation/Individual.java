package fr.polytechtours.prd.multiagent.Model.ApproacheMethod.MetaHeuristique.CommonModelisation;

import fr.polytechtours.prd.multiagent.Model.Modelisation.Instance;

import java.util.ArrayList;
/**
 * Cette classe abstraite permet de représenter une solution possible
 *
 * @author Kake Abdoulaye
 * @version 2.0
 * @since February 18, 2019
 */
public abstract class Individual {

    /**
     * data to use
     */
    public Instance data;
    /**
     * genes of individual
     */
    public ArrayList<Integer> genes;
    /**
     * values of objective functions
     */
    public ArrayList<Integer> valuesObj;
    /**
     * number of individuals which dominate this individual
     */
    public int numDominated;

    /**
     * crowded distance
     */
    public double crowdedDistance = 0.0;

    /**
     * Jobs Scheduled by machine
     */

    public ArrayList<ArrayList<Integer>> jobsOrdonnances ;


    /**
     * if this individual is feasible
     */
    public boolean valide = true;
    /**
     * constructor
     * @param data data to use
     */
    public Individual(Instance data){
        this.data = data;
        genes = new ArrayList<Integer>();
        valuesObj = new ArrayList<Integer>();
        numDominated = 0;
        jobsOrdonnances = new ArrayList<>();
    }



    /**
     * to change number dominated
     * @param change numer of change
     */
    public void changeNumDominated(int change){
        this.numDominated += change;
    }


}
