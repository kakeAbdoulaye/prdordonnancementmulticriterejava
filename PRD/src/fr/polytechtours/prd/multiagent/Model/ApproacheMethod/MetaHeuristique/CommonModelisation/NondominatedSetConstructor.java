package fr.polytechtours.prd.multiagent.Model.ApproacheMethod.MetaHeuristique.CommonModelisation;

import fr.polytechtours.prd.multiagent.Model.ApproacheMethod.MetaHeuristique.ModelisationMachine.IndividualMachine;
import fr.polytechtours.prd.multiagent.Model.ApproacheMethod.MetaHeuristique.MoelisationBinaire.IndividualBinaire;
import fr.polytechtours.prd.multiagent.Model.Modelisation.ParetoSolution;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * This class provides methods to sort the population by their level of dominant<br>
 * In return, several sets are returned in order of their rank of dominant<br>
 * The lower rank comes first
 * 
 * @author Boyang Wang , kake Abdoulaye
 * @version 1.0
 * Modifier par Kake Abdoulaye le 7 mars 2019
 * @since Mars 8, 2018
 *
 */
public class NondominatedSetConstructor {

	/**
	 * To verify if individual p dominates q.<br>
	 * If p dominates q, all values of objective functions of p are smaller than q
	 * @param p individual
	 * @param q individual
	 * @return true if p dominates q, false if not
	 */
	public static Boolean pDomQ(IndividualBinaire p, IndividualBinaire q){

		ArrayList<Integer> pElems = p.valuesObj;
		ArrayList<Integer> qElems = q.valuesObj;
		for(int i = 0; i < pElems.size(); i++){
			if(pElems.get(i) > qElems.get(i)){
				return false;
			}
		}
		return true;
	}
	/**
	 * To verify if individual p dominates q.<br>
	 * If p dominates q, all values of objective functions of p are smaller than q
	 * @param p individual
	 * @param q individual
	 * @return true if p dominates q, false if not
	 */
	public static Boolean pDomQ(IndividualMachine p, IndividualMachine q){

		ArrayList<Integer> pElems = p.valuesObj;
		ArrayList<Integer> qElems = q.valuesObj;
		for(int i = 0; i < pElems.size(); i++){
			if(pElems.get(i) > qElems.get(i)){
				return false;
			}
		}
		return true;
	}
	/**
	 * To verify if individual p dominates q.<br>
	 * If p dominates q, all values of objective functions of p are smaller than q
	 * @param p ParetoSolution
	 * @param q ParetoSolution
	 * @return true if p dominates q, false if not
	 */
	public static Boolean pDomQ(ParetoSolution p, ParetoSolution q){
		if(p.valueObjA == q.valueObjA && p.valueObjB == q.valueObjB)
		{
			return  false;
		}
		else if(p.valueObjA > q.valueObjA || p.valueObjB > q.valueObjB){
				return false;
		}
		return true;
	}

	/**
	 * Cette méthode de choisir dans un ensemble P de solution de Pareto, un ensemble P' avec P' inclut dans P où P' est
	 * l'ensemble des solutions non dominés. Donc P' représente le front de Pareto optimale.
	 * @param paretoFront , l'ensemble P de solution de Pareto
	 * @return l'ensemble P' des solutions non dominés.
	 */

	public static  HashSet<ParetoSolution> frontPareto_NonDominance(Set<ParetoSolution> paretoFront){
		HashSet<ParetoSolution> paretoFrontNew = new HashSet<ParetoSolution>();
		boolean test = false;

		for(Iterator<ParetoSolution> iterp = paretoFront.iterator(); iterp.hasNext(); )
		{
			ParetoSolution p = iterp.next();
			for(Iterator<ParetoSolution> iterq = paretoFront.iterator(); iterq.hasNext(); )
			{
				ParetoSolution q = iterq.next();
				test = test || pDomQ(q,p);
			}
			if(test == false)
			{
				paretoFrontNew.add(p);
			}
			test = false;
		}

		return paretoFrontNew;
	}

	/**
	 * Cette méthode permet de supprimer les doublons dans un front de pareto.
	 * @param paretoFront , le front de pareto avec des doublons
	 * @return le front de Pareto sans doublons
	 */
	public static HashSet<ParetoSolution> frontPareto_SansDoublons(Set<ParetoSolution> paretoFront){
		HashSet<ParetoSolution> paretoFrontNew = new HashSet<ParetoSolution>();
		boolean test = false;

		for(Iterator<ParetoSolution> iterp = paretoFront.iterator(); iterp.hasNext(); )
		{
			ParetoSolution p = iterp.next();
			for(Iterator<ParetoSolution> iterq = paretoFront.iterator(); iterq.hasNext(); )
			{
				ParetoSolution q = iterq.next();

				if(iterp != iterq)
				{
					if(p.valueObjA == q.valueObjA && p.valueObjB == q.valueObjB)
					{
						test = true;
					}
				}
			}
			if(test == false)
			{
				paretoFrontNew.add(p);
			}
		}

		return paretoFrontNew;
	}

	/**
	 * To sort the population by order of their level of dominant.<br>
	 * One set contains individuals with the same level of dominant.
	 * @param pop population not sorted
	 * @return different sets of different levels of dominant
	 */
	public static ArrayList<ArrayList<IndividualBinaire>> sortB(ArrayList<IndividualBinaire> pop){
		//initialize the non dominated set pn
		ArrayList<ArrayList<IndividualBinaire>> pn = new ArrayList<ArrayList<IndividualBinaire>>();
		for(int i = 0; i < pop.size(); i++){
			pn.add(new ArrayList<IndividualBinaire>());
		}
		
		for(int i=0; i<pop.size(); i++){
			pop.get(i).numDominated = 0;
			pop.get(i).setDominant.clear();
		}
			
		//calculate the number of dominating and the dominating set
		for(int i = 0; i < pop.size(); i++){
			IndividualBinaire pIndividualBinaire = pop.get(i);
			for(int j = 0; j < pop.size(); j++){
				if(j != i){//if the two individuals are different
					IndividualBinaire qIndividualBinaire = pop.get(j);
					if(pDomQ(pIndividualBinaire, qIndividualBinaire)){
						pIndividualBinaire.addIndividualDominant(qIndividualBinaire);
					}
					else if(pDomQ(qIndividualBinaire, pIndividualBinaire)){
						pIndividualBinaire.changeNumDominated(1);
					}
				}
			}
			if(pIndividualBinaire.numDominated == 0)
				pn.get(0).add(pIndividualBinaire);
		}
		
		//calculate pn
		for(int i = 0; pn.get(i).size() != 0; ){
			ArrayList<IndividualBinaire> h = new ArrayList<IndividualBinaire>();
			for(IndividualBinaire pIndividualBinaire : pn.get(i)){
				for(IndividualBinaire qIndividualBinaire : pIndividualBinaire.setDominant){
					qIndividualBinaire.changeNumDominated(-1);
					if(qIndividualBinaire.numDominated == 0)
						h.add(qIndividualBinaire);
				}//end for q
			}// end for p
			i++;
			pn.set(i, h);  
		}
		
		return pn;
	}


    /**
     * To sort the population by order of their level of dominant.<br>
     * One set contains individuals with the same level of dominant.
     * @param pop population not sorted
     * @return different sets of different levels of dominant
     */
    public static ArrayList<ArrayList<IndividualMachine>> sortM(ArrayList<IndividualMachine> pop){
        //initialize the non dominated set pn
        ArrayList<ArrayList<IndividualMachine>> pn = new ArrayList<ArrayList<IndividualMachine>>();
        for(int i = 0; i < pop.size(); i++){
            pn.add(new ArrayList<IndividualMachine>());
        }

        for(int i=0; i<pop.size(); i++){
            pop.get(i).numDominated = 0;
            pop.get(i).setDominant.clear();
        }

        //calculate the number of dominating and the dominating set
        for(int i = 0; i < pop.size(); i++){
            IndividualMachine pIndividualMachine = pop.get(i);
            for(int j = 0; j < pop.size(); j++){
                if(j != i){//if the two individuals are different
                    IndividualMachine qIndividualMachine = pop.get(j);
                    if(pDomQ(pIndividualMachine, qIndividualMachine)){
                        pIndividualMachine.addIndividualDominant(qIndividualMachine);
                    }
                    else if(pDomQ(qIndividualMachine, pIndividualMachine)){
                        pIndividualMachine.changeNumDominated(1);
                    }
                }
            }
            if(pIndividualMachine.numDominated == 0)
                pn.get(0).add(pIndividualMachine);
        }

        //calculate pn
        for(int i = 0; pn.get(i).size() != 0; ){
            ArrayList<IndividualMachine> h = new ArrayList<IndividualMachine>();
            for(IndividualMachine pIndividualMachine : pn.get(i)){
                for(IndividualMachine qIndividualMachine : pIndividualMachine.setDominant){
                    qIndividualMachine.changeNumDominated(-1);
                    if(qIndividualMachine.numDominated == 0)
                        h.add(qIndividualMachine);
                }//end for q
            }// end for p
            i++;
            pn.set(i, h);
        }

        return pn;
    }

}
