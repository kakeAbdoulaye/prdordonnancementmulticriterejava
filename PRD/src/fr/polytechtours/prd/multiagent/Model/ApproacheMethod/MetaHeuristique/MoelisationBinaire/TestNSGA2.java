package fr.polytechtours.prd.multiagent.Model.ApproacheMethod.MetaHeuristique.MoelisationBinaire;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import fr.polytechtours.prd.multiagent.Model.Modelisation.Instance;
import fr.polytechtours.prd.multiagent.Model.Modelisation.Job;
import fr.polytechtours.prd.multiagent.Model.Modelisation.Machine;
import fr.polytechtours.prd.multiagent.Model.Modelisation.ParetoSolution;
import fr.polytechtours.prd.multiagent.Model.Util.Commun;
import fr.polytechtours.prd.multiagent.Model.Util.Timer;

/**
 * Test class for NSGA2_B method
 * 
 * @author Boyang Wang
 * @version 1.0
 * @since Mars 1, 2018
 *
 */
public class TestNSGA2 {

	@SuppressWarnings("unchecked")
	public static void main(String[] args){
		//HashMap<String, Object> hashmap = Commun.ReadDataFromFile("instance-200-2-3-40-N9.data", Job.TYPE_FACTOR_SORT_MAX);//instance-80-2-3-40.data ,instance-8-2-3-1_2agent_2machine.data  instance-200-2-3-40-N9.data
		Instance data = new Instance("instance-200-2-3-40-N9.data");
		/*data.jobs = (ArrayList<Job>) hashmap.get("jobs");
		data.nbMachine = (int)hashmap.get("nombreMachine");
		System.out.println("je teste "+data.nbMachine);
		data.machines = (ArrayList<Machine>) hashmap.get("machine");
		data.nbJobs = (int) hashmap.get("numJob");
		data.nbJobsA = (int) hashmap.get("numJobAgentA");
		data.filename = (String) hashmap.get("filename");
		data.maxEnd = Commun.getMaxEnd(data.jobs);*/



		Timer timer = new Timer();
		timer.setStart(System.currentTimeMillis());
		NSGA2_B nsga2B = new NSGA2_B(data,"hhhhh");
		Set<ParetoSolution> front = nsga2B.generateParetoFront();
		
		timer.setEnd(System.currentTimeMillis());
		
		for(Iterator<ParetoSolution> iter=front.iterator(); iter.hasNext(); ){
			ParetoSolution solution = iter.next();
			StringBuilder builder = new StringBuilder("[");
			for(int i=0; i<solution.sequence.size(); i++){
				builder.append(solution.sequence.get(i)).append(",");
			}
			builder.deleteCharAt(builder.length() - 1);
			builder.append("]");
			System.out.println("------------------------------------------");
			System.out.println("Jobs to schedule: " + builder.toString());
			System.out.println("Number of jobs rejected of agent A: "+solution.valueObjA+" , "+"Number of jobs rejected of agent B: "+solution.valueObjB);
			System.out.println("------------------------------------------");
		}
		System.out.println("Time consumed: "+timer.calculateTimeConsume());
		/*EpsilonConstraint epsilonContraint = new EpsilonConstraint();
		epsilonContraint.loadParam(data);
		Set<ParetoSolution> frontExact = epsilonContraint.generateParetoFront();
		ComparaisonSetPareto cp = new ComparaisonSetPareto();
		System.out.println("Percentage of optimal solution found: "+String.format("%.2f", cp.percentOptimalSolution(frontExact,front))+"%");
		System.out.println("Mean distance between two pareto fronts: "+String.format("%.2f", cp.getMeanDistance(frontExact,front)));
		System.out.println("Hypervolume between two pareto fronts: "+String.format("%.1f", cp.getHyperVolume(frontExact,front)));*/
	}
}
