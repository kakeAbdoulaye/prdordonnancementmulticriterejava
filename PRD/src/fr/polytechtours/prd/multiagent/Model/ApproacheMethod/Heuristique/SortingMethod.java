package fr.polytechtours.prd.multiagent.Model.ApproacheMethod.Heuristique;

import fr.polytechtours.prd.multiagent.Model.Modelisation.Job;

import java.util.ArrayList;

import static fr.polytechtours.prd.multiagent.Model.Util.Commun.isTab_en_ordre;

/**
 * Cette classe contient les différents algorithme  et méthode de trie des jobs
 * L'utilisateur doit choisir une des méthodes de Trie des Jobs.
 *
 * @author Kake Abdoulaye
 * @version 2.0
 * @since February 18, 2019
 */
public class SortingMethod {

    /**
     *  réation du tableau de jobs triés
     */
   private  ArrayList<Job> tableauJobs ;

    /**
     *  Tableau des ids et des calculs
     */
   private int [] [] tableauInitial ;

    /**
     * Liste des jobs à trier
     */
   private  ArrayList<Job> jobs;

    public SortingMethod(ArrayList<Job> jobs)
    {
        this.jobs = jobs;
    }

    /**
     * Cette méthode permet de trier par ordre croissant les élément d'un tableau
     */
    private void trieParOrdreCroissant()
    {
        //Trie par ordre croissant
        boolean tab_en_ordre = false;
        int taille = tableauInitial.length;
        while(!tab_en_ordre)
        {
            tab_en_ordre = true;
            for(int i=0 ; i < taille-1 ; i++)
            {
                if(tableauInitial[i][1] > tableauInitial[i+1][1])
                {
                    tab_en_ordre = isTab_en_ordre(tableauInitial, i);
                }
                else {
                    if(tableauInitial[i][1] == tableauInitial[i+1][1])
                    {
                        if(jobs.get(tableauInitial[i][0]).start >  jobs.get(tableauInitial[i+1][0]).start)
                        {
                            tab_en_ordre = isTab_en_ordre(tableauInitial, i);
                        }
                    }
                }
            }
            taille--;
        }

    }

    /**
     * Cette méthode permet  de Trier par ordre croissant selon la méthode MakeSpan basée sur le produit de la somme des ressources
     *  de chaque job et leur durée
     *
     * @return  La liste des jobs triés
     */
   public  ArrayList<Job>  trierCCmaxSommeRessources(){

        //Ici on créer le tableau de valeur pour CCMax
        //selon la formule sommes des ressources pour un job * (F(i)-S(i))
       tableauInitial =  new int [jobs.size()][2];

        for(int i = 0 ; i<jobs.size() ; i++)
        {
            tableauInitial[i][0]= i;
            tableauInitial[i][1]= 0;

            for (int r = 0; r < jobs.get(i).consumes.size(); r++)
            {
                tableauInitial[i][1] += (jobs.get(i).consumes.get(r) * (jobs.get(i).end-jobs.get(i).start));
            }

        }
        //Trie par ordre croissant de valeur de CCmax du tableau de job
       trieParOrdreCroissant();

        //Création du tableau de jobstriés selon la règle CCmax (sommesRessources*duree)
       jobs = jobTrier();
       return jobs;

    }


    /**
     * Cette méthode permet  de Trier en ordre croissant selon la méthode CCmax basée sur la valeur de ressource maximale de chaque job
     *
     * @return La liste des jobs triés
     */
    public  ArrayList<Job>  trierCCmaxMaxRessources(){

        //Ici on créer le tableau de valeur pour CCMax
        //selon la formule valeur de la ressource la plus importante pour un job * (F(i)-S(i))
        tableauInitial =  new int [jobs.size()][2];

        for(int i = 0 ; i<jobs.size(); i++)
        {
            //On prend en compte la ressource la plus importante pour calculer le CCmax
            int maxRessource = 0;
            tableauInitial[i][0]= i;
            tableauInitial[i][1]= 0;

            for (int r = 0; r < jobs.get(i).consumes.size(); r++)
            {
                if(jobs.get(i).consumes.get(r) > maxRessource)
                {
                    maxRessource = jobs.get(i).consumes.get(r);
                }
            }
            tableauInitial[i][1] = maxRessource*(jobs.get(i).end-jobs.get(i).start);
        }

        //Trie par ordre croissant de valeur de CCmax du tableau de job
       trieParOrdreCroissant();

        //Création du tableau de jobstriés selon la règle CCmax (par rapport à la ressource maximale)
        jobs = jobTrier();
        return jobs;

    }

    /**
     *  Cette méthode permet  de Trier  en ordre croissant selon la méthode SPT  basée sur  la durée  de chaque job
     *
     * @return La liste des jobs triés
     */
    public  ArrayList<Job>  trierDuree(){

        //Ici on créer le tableau de valeur pour CCMax
        //selon la formule sommes des ressources pour un job * (F(i)-S(i))
        tableauInitial =  new int [jobs.size()][2];

        for(int i = 0 ; i<jobs.size() ; i++)
        {
            tableauInitial[i][0]= i;
            tableauInitial[i][1]= (jobs.get(i).end-jobs.get(i).start);
        }

        //Trie par ordre croissant de valeur de CCmax du tableau de job
        trieParOrdreCroissant();
        //Création du tableau de jobstriés selon la règle CCmax (sommesRessources)
        jobs = jobTrier();
        return jobs;

    }

    /**
     * Cette méthode nous permet d'avoir la liste des jobs trier en fonction de leur ordre du plus petit au plus grand
     * @return les jobs triés
     */
    private ArrayList<Job> jobTrier()
    {
        tableauJobs = new ArrayList<>();
        int index = 0 ;
        int count = 0 ;

        for (int i = 0; i < tableauInitial.length; i++){

            while (count < tableauInitial.length)
            {
                if(this.jobs.get(count).id != tableauInitial[i][0])
                {
                    index = i ;
                }
                count ++;

            }
            tableauJobs.add(jobs.get(index));
            //System.out.println(jobs.get(index).id+" - "+tableauInitial[i][1]);
            count = 0;
        }

        return tableauJobs;


    }





}
