package fr.polytechtours.prd.multiagent.Model.ApproacheMethod.MetaHeuristique.CommonModelisation;

import fr.polytechtours.prd.multiagent.Model.ApproacheMethod.MetaHeuristique.ModelisationMachine.ConstantMachine;
import fr.polytechtours.prd.multiagent.Model.ApproacheMethod.MetaHeuristique.ModelisationMachine.IndividualMachine;
import fr.polytechtours.prd.multiagent.Model.ApproacheMethod.MetaHeuristique.MoelisationBinaire.ConstantBinaire;
import fr.polytechtours.prd.multiagent.Model.ApproacheMethod.MetaHeuristique.MoelisationBinaire.IndividualBinaire;

import java.util.ArrayList;

/**
 * This class provides methods to calculate the crowding distances for individuals
 * 
 * @author Boyang Wang
 * @version 1.0
 * @since Mars 1, 2018
 *
 */
public class CrowdingDistanceAssignment {

	/**
	 * to calculate the crowding distances for each individual
	 * @param pop the population
	 */
	public static void distanceCalculatorB(ArrayList<IndividualBinaire> pop){

		// initiation of crowding distances
		int size = pop.size();
		for(int i=0; i<size; i++){
			pop.get(i).crowdedDistance = 0.0;
		}

		//to calculate crowding distances for each individual
		for(int j = 0; j< ConstantBinaire.NUM_OBJ; j++){
			pop = sortB(pop, j);//to sort the individuals in order of their fitness
			pop.get(0).crowdedDistance = pop.get(pop.size() - 1).crowdedDistance = ConstantBinaire.MAX_INT;// the first and the last with the max value

			for(int k=1; k<pop.size() - 2; k++){
				if(maxFunctionObjB(pop, j) == minFunctionObjB(pop, j))
					pop.get(k).crowdedDistance = ConstantBinaire.MAX_INT;
				else
					pop.get(k).crowdedDistance = pop.get(k).crowdedDistance +
						(pop.get(k+1).valuesObj.get(j) - pop.get(k-1).valuesObj.get(j)) / (maxFunctionObjB(pop, j) - minFunctionObjB(pop, j));
			}
		}
	}
	/**
	 * to calculate the crowding distances for each individual
	 * @param pop the population
	 */
	public static void distanceCalculatorM(ArrayList<IndividualMachine> pop){

		// initiation of crowding distances
		int size = pop.size();
		for(int i=0; i<size; i++){
			pop.get(i).crowdedDistance = 0.0;
		}

		//to calculate crowding distances for each individual
		for(int j = 0; j< ConstantMachine.NUM_OBJ; j++){
			pop = sortM(pop, j);//to sort the individuals in order of their fitness
			pop.get(0).crowdedDistance = pop.get(pop.size() - 1).crowdedDistance = ConstantMachine.MAX_INT;// the first and the last with the max value

			for(int k=1; k<pop.size() - 2; k++){
				if(maxFunctionObjM(pop, j) == minFunctionObjM(pop, j))
					pop.get(k).crowdedDistance = ConstantMachine.MAX_INT;
				else
					pop.get(k).crowdedDistance = pop.get(k).crowdedDistance +
							(pop.get(k+1).valuesObj.get(j) - pop.get(k-1).valuesObj.get(j)) / (maxFunctionObjM(pop, j) - minFunctionObjM(pop, j));
			}
		}
	}




		/**
         * to sort the individuals in order of their fitness<br>
         * the fitness if calculated by the objective function
         * @param popNonSorted the population not sorted
         * @param numFunctionObj index of objective function
         * @return list of individuals sorted
         */
	public static ArrayList<IndividualBinaire> sortB(ArrayList<IndividualBinaire> popNonSorted, int numFunctionObj) {

		IndividualBinaire temp;
		ArrayList<IndividualBinaire> pop = new ArrayList<IndividualBinaire>();
		pop.addAll(popNonSorted);
		for(int i=0;i<pop.size()-1;i++){
			for(int j=0;j<pop.size()-1-i;j++){
				if(pop.get(j).valuesObj.get(numFunctionObj) > pop.get(j+1).valuesObj.get(numFunctionObj)){
					temp = pop.get(j);
					pop.set(j,pop.get(j+1));
					pop.set(j+1, temp);
				}
			}
		}
		
		return pop;
	}
	/**
	 * to sort the individuals in order of their fitness<br>
	 * the fitness if calculated by the objective function
	 * @param popNonSorted the population not sorted
	 * @param numFunctionObj index of objective function
	 * @return list of individuals sorted
	 */
	public static ArrayList<IndividualMachine> sortM(ArrayList<IndividualMachine> popNonSorted, int numFunctionObj) {

		IndividualMachine temp;
		ArrayList<IndividualMachine> pop = new ArrayList<IndividualMachine>();
		pop.addAll(popNonSorted);
		for(int i=0;i<pop.size()-1;i++){
			for(int j=0;j<pop.size()-1-i;j++){
				if(pop.get(j).valuesObj.get(numFunctionObj) > pop.get(j+1).valuesObj.get(numFunctionObj)){
					temp = pop.get(j);
					pop.set(j,pop.get(j+1));
					pop.set(j+1, temp);
				}
			}
		}

		return pop;
	}


	
	/**
	 * Sort all individuals by their crowding distances in descent order
	 * @param popNonSorted population not sorted
	 * @return list of individuals sorted
	 */
	public static ArrayList<IndividualBinaire> sortByDistanceB(ArrayList<IndividualBinaire> popNonSorted){
		IndividualBinaire temp;
		ArrayList<IndividualBinaire> pop = new ArrayList<IndividualBinaire>();
		pop.addAll(popNonSorted);
		for(int i=0;i<pop.size()-1;i++){
			for(int j=0;j<pop.size()-1-i;j++){
				if(pop.get(j).crowdedDistance < pop.get(j+1).crowdedDistance){
					temp = pop.get(j);
					pop.set(j,pop.get(j+1));
					pop.set(j+1, temp);
				}
			}
		}
		
		return pop;
	}
	/**
	 * Sort all individuals by their crowding distances in descent order
	 * @param popNonSorted population not sorted
	 * @return list of individuals sorted
	 */
	public static ArrayList<IndividualMachine> sortByDistanceM(ArrayList<IndividualMachine> popNonSorted){
		IndividualMachine temp;
		ArrayList<IndividualMachine> pop = new ArrayList<IndividualMachine>();
		pop.addAll(popNonSorted);
		for(int i=0;i<pop.size()-1;i++){
			for(int j=0;j<pop.size()-1-i;j++){
				if(pop.get(j).crowdedDistance < pop.get(j+1).crowdedDistance){
					temp = pop.get(j);
					pop.set(j,pop.get(j+1));
					pop.set(j+1, temp);
				}
			}
		}

		return pop;
	}

	
	/**
	 * Get the max value of the objective function
	 * @param pop population
	 * @param numFunctionObj index of objective function
	 * @return max value
	 */
	public static int maxFunctionObjB(ArrayList<IndividualBinaire> pop, int numFunctionObj){
		int max = -1;
		for(int i=0; i<pop.size(); i++){
			if(pop.get(i).valuesObj.get(numFunctionObj) > max){
				max = pop.get(i).valuesObj.get(numFunctionObj);
			}
		}
		return max;
	}
	/**
	 * Get the max value of the objective function
	 * @param pop population
	 * @param numFunctionObj index of objective function
	 * @return max value
	 */
	public static int maxFunctionObjM(ArrayList<IndividualMachine> pop, int numFunctionObj){
		int max = -1;
		for(int i=0; i<pop.size(); i++){
			if(pop.get(i).valuesObj.get(numFunctionObj) > max){
				max = pop.get(i).valuesObj.get(numFunctionObj);
			}
		}
		return max;
	}
	
	/**
	 * Get the min value of the objective function
	 * @param pop population
	 * @param numFunctionObj index of objective function
	 * @return min value
	 */
	public static int minFunctionObjB(ArrayList<IndividualBinaire> pop, int numFunctionObj){
		int min = ConstantBinaire.MAX_INT;
		for(int i=0; i<pop.size(); i++){
			if(pop.get(i).valuesObj.get(numFunctionObj) < min){
				min = pop.get(i).valuesObj.get(numFunctionObj);
			}
		}
		return min;
	}
	/**
	 * Get the min value of the objective function
	 * @param pop population
	 * @param numFunctionObj index of objective function
	 * @return min value
	 */
	public static int minFunctionObjM(ArrayList<IndividualMachine> pop, int numFunctionObj){
		int min = ConstantMachine.MAX_INT;
		for(int i=0; i<pop.size(); i++){
			if(pop.get(i).valuesObj.get(numFunctionObj) < min){
				min = pop.get(i).valuesObj.get(numFunctionObj);
			}
		}
		return min;
	}

} 
