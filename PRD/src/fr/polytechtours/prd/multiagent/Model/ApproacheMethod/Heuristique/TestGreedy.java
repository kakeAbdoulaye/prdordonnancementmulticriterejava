package fr.polytechtours.prd.multiagent.Model.ApproacheMethod.Heuristique;

/**
 * Test class for Greedy method
 * 
 * @author Boyang Wang
 * @version 1.0
 * @since Feb 1, 2018
 *
 */
public class TestGreedy {

	@SuppressWarnings("unchecked")
	public static void main(String[] args){
		/*HashMap<String, Object> hashmap = Commun.ReadDataFromFile("instance-80-2-3-40.data", Job.TYPE_FACTOR_SORT_MAX);instance-8-2-3-1_2agent_2machine.data
		Instance data = new Instance();
		data.jobs = (ArrayList<Job>) hashmap.get("jobs");
		data.machine = (Machine)hashmap.get("machine");
		data.nbJobs = (int) hashmap.get("numJob");
		data.nbJobsA = (int) hashmap.get("numJobAgentA");
		data.typeGreedy = Greedy.GREEDY_EPSILON;
		data.epsilon = data.nbJobsA/2;
		
		Timer timer = new Timer();
		timer.setStart(System.currentTimeMillis());
		Greedy greedy = new Greedy();
		greedy.loadParam(data);
		Set<ParetoSolution> front = greedy.generateParetoFront();
		
		timer.setEnd(System.currentTimeMillis());
		
		for(Iterator<ParetoSolution> iter=front.iterator(); iter.hasNext(); ){
			ParetoSolution solution = iter.next();
			StringBuilder builder = new StringBuilder("[");
			for(int i=0; i<solution.sequence.size(); i++){
				builder.append(solution.sequence.get(i)).append(",");
			}
			builder.deleteCharAt(builder.length() - 1);
			builder.append("]");
			System.out.println("------------------------------------------");
			System.out.println("Jobs to schedule: " + builder.toString());
			System.out.println("Number of jobs rejected of agent A: "+solution.valueObjA+" , "+"Number of jobs rejected of agent B: "+solution.valueObjB);
			System.out.println("------------------------------------------");
		}
		System.out.println("Time consumed: "+timer.calculateTimeConsume());
		EpsilonConstraint epsilonContraint = new EpsilonConstraint();
		epsilonContraint.loadParam(data);
		Set<ParetoSolution> frontExact = epsilonContraint.generateParetoFront();
		System.out.println("Percentage of optimal solution found: "+String.format("%.2f",greedy.percentOptimalSolution(frontExact))+"%");
		System.out.println("Mean distance between two pareto fronts: "+String.format("%.2f", greedy.getMeanDistance(frontExact)));
		System.out.println("Hypervolume between two pareto fronts: "+String.format("%.1f", greedy.getHyperVolume(frontExact)));*/
	}
}
