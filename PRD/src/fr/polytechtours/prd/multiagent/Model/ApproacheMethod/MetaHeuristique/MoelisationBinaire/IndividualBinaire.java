package fr.polytechtours.prd.multiagent.Model.ApproacheMethod.MetaHeuristique.MoelisationBinaire;

import java.util.ArrayList;

import fr.polytechtours.prd.multiagent.Model.IterfaceClass.Calculate;
import fr.polytechtours.prd.multiagent.Model.ApproacheMethod.MetaHeuristique.CommonModelisation.Individual;
import fr.polytechtours.prd.multiagent.Model.Modelisation.Instance;

/**
 * IndividualBinaire to simulation one solution of NSGA2_B<br>
 * One individual contains :
 * <ul>
 * <li>genes: its genes of size N with 0 for scheduled and 1 for abandoned</li>
 * <li>fitness: values of objective functions</li>
 * <li>number dominated: the number of individuals which dominates this individual</li>
 * <li>set dominant: set of individuals which this individual dominates</li>
 * <li>crowdedDistance: crowded distance</li>
 * <li>valid: if this individual is feasible or not</li>
 * </ul>
 * @author Boyang Wang
 * @version 1.0
 * @since Mars 6, 2018
 *
 */
public class IndividualBinaire extends Individual implements Calculate {

	/**
	 * set of individuals which this individual dominates
	 */
	public ArrayList<IndividualBinaire> setDominant;
	/**
	 * constructor
	 * @param data data to use
	 */
	public IndividualBinaire(Instance data){
		super(data);
		setDominant = new ArrayList<IndividualBinaire>();
	}

	/**
	 * to add one individual into the set dominant
	 * @param ind individual
	 */
	public void addIndividualDominant(IndividualBinaire ind){
		this.setDominant.add(ind);
	}

	@Override
	public void calculateValueObj() {
		valuesObj.clear();
		valuesObj.add(0);
		valuesObj.add(0);
		
		for(int i=0; i<data.nbJobs; i++){
			if(data.jobs.get(i).belongTo.equals("A")){
				if(genes.get(i) == 1){
					valuesObj.set(0, valuesObj.get(0)+1);
				}
			}
			else{
				if(genes.get(i) == 1){
					valuesObj.set(1, valuesObj.get(1)+1);
				}
			}
		}
	}

	@Override
	public  void validate()
	{
		//Déclaration de variable en local pour améliorer les performances des heuristiques
		int nombre_machine = data.nbMachine ;
		int nombre_jobs = data.nbJobs;
		int nombre_ressource = data.machines.get(0).resources.size();
		ArrayList<Integer> s_j = new ArrayList<>(nombre_jobs); // date de début des jobs
		for(int i =0 ;i<nombre_jobs;i++)
		{
			s_j.add(data.jobs.get(i).start);
		}
		ArrayList<Integer> f_j = new ArrayList<>(nombre_jobs); // date de fin des jobs
		for(int i =0 ;i<nombre_jobs;i++)
		{
			f_j.add(data.jobs.get(i).end);
		}
		ArrayList<ArrayList<Integer>> capaciteRessourceMachine = new ArrayList<ArrayList<Integer>>(nombre_machine); // tableau des ressouces maximale de chaque machine
		for(int i =0 ;i<nombre_machine;i++)
		{
			capaciteRessourceMachine.add(new ArrayList<>(nombre_ressource));
			for(int j =0 ;j<nombre_ressource;j++)
			{
				capaciteRessourceMachine.get(i).add(data.machines.get(i).resources.get(j));
			}
		}
		ArrayList<ArrayList<Integer>> capaciteRessourceJob = new ArrayList<ArrayList<Integer>>(nombre_jobs); // tableau des ressouces  de chaque job , tableauRessourcesJobs
		for(int i =0 ;i<nombre_jobs;i++)
		{
			capaciteRessourceJob.add(new ArrayList<>(nombre_ressource));
			for(int j =0 ;j<nombre_ressource;j++)
			{
				capaciteRessourceJob.get(i).add(data.jobs.get(i).consumes.get(j));
			}
		}
		ArrayList<Integer> tableauJobs = new ArrayList<Integer>(1);
		int nombreJobAOrdonnance = 0 ;
		for(int index = 0 ; index <nombre_jobs; index++)
		{
			if(genes.get(index)==0)
			{
				tableauJobs.add(index);
				nombreJobAOrdonnance++;
				//System.out.print("Job_ordonnancable : " + tableauJobs.get(ou)+"\t");
				//ou++;
			}
		}
		//System.out.println("Nombre_Job : "+tableauJobs.size());
		//ArrayList<ArrayList<Integer>> jobsOrdonnances  = new ArrayList<>(); // job ordonnnacé par machine
		for(int m = 0; m < nombre_machine ; m++){
			ArrayList<Integer> jobsMachine = new ArrayList<>();
			ArrayList<Integer> Lm= new ArrayList<>(tableauJobs.size());
			for(int in =0 ; in < tableauJobs.size() ; in ++ )
			{
				Lm.add(tableauJobs.get(in));
			}
			//System.out.println("First element : : :" +Lm.size());

			while(Lm.size() != 0){
				ArrayList<Integer> Sm = new ArrayList<>();
				for(int i = 0; i < jobsMachine.size(); i++){
					if(s_j.get(Lm.get(0)) < f_j.get(jobsMachine.get(i)) && f_j.get(Lm.get(0))> s_j.get(jobsMachine.get(i))){
						Sm.add(jobsMachine.get(i));
					}
				}

				boolean jobOrdonancable = true;

				for( int i = s_j.get(Lm.get(0)); i < f_j.get(Lm.get(0)); i++){
					for( int r = 0; r < nombre_ressource ; r++){
						int sommesRessources = 0;
						for(int j = 0; j < Sm.size(); j++){
							if(s_j.get(Sm.get(j)) <= i && f_j.get(Sm.get(j)) > i )
								sommesRessources += capaciteRessourceJob.get(Sm.get(j)).get(r);
						}
						sommesRessources += capaciteRessourceJob.get(Lm.get(0)).get(r);
						// cout << "Sommes Ressources : " << sommesRessources << endl;
						if(sommesRessources > capaciteRessourceMachine.get(m).get(r))
							jobOrdonancable = false;
						// cout << jobOrdonancable << endl;
					}
				}

				if (jobOrdonancable){
					jobsMachine.add(Lm.get(0));
					tableauJobs.remove(Lm.get(0));
				}
				Lm.remove(Lm.get(0));
			}
			jobsOrdonnances.add(jobsMachine);
		}
		int count = 0;
		for(int o =0 ; o < jobsOrdonnances.size() ; o++)
		{
			count += jobsOrdonnances.get(o).size();
		}
		//System.out.println("Nombre de job fait : "+count);
		if(count == nombreJobAOrdonnance)
		{
			this.valide = true;
		}
		else
		{
			this.valide = false;
		}

	}
}
