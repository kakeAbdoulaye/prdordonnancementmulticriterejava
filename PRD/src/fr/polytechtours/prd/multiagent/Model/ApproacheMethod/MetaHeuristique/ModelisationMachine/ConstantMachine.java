package fr.polytechtours.prd.multiagent.Model.ApproacheMethod.MetaHeuristique.ModelisationMachine;

/**
 * All constants as configuration of NSGA2_M
 * @author kake Abdoulaye
 * @version 1.0
 * @since Jan 10, 2019
 *
 */
public class ConstantMachine {
    /**
     * number of objective function
     */
    public static final int NUM_OBJ = 2;

    /**
     * max int
     */
    public static final int MAX_INT = 65535;

    /**
     * number of iteration
     */
    public static final int NUM_ITERATION = 500;

    /**
     * size of population
     */
    public static final int SIZE_POPULATION = 100;

    /**
     * probability of crossover
     */
    public static final double PROB_CROSSOVER = 0.8;

    /**
     * probability of mutation to happen
     */
    public static final double PROB_MUTATION = 0.8;

    /**
     * Number of individual , we compare before we select
     */
    public static final int NOMBRE_SELECTION = 10;
}
