package fr.polytechtours.prd.multiagent.Model.ApproacheMethod.MetaHeuristique.ModelisationMachine;


import fr.polytechtours.prd.multiagent.Model.IterfaceClass.IANSGA2;
import fr.polytechtours.prd.multiagent.Model.IterfaceClass.IAlgorithm;
import fr.polytechtours.prd.multiagent.Model.ApproacheMethod.MetaHeuristique.CommonModelisation.CrowdingDistanceAssignment;
import fr.polytechtours.prd.multiagent.Model.ApproacheMethod.MetaHeuristique.CommonModelisation.NondominatedSetConstructor;
import fr.polytechtours.prd.multiagent.Model.Modelisation.Instance;
import fr.polytechtours.prd.multiagent.Model.Modelisation.Job;
import fr.polytechtours.prd.multiagent.Model.Modelisation.ParetoSolution;
import fr.polytechtours.prd.multiagent.Model.ApproacheMethod.Operation.RandomOp;
import fr.polytechtours.prd.multiagent.Model.Util.Commun;
import fr.polytechtours.prd.multiagent.Model.Util.Timer;

import java.io.File;
import java.util.*;

/**
 * This class provides the main loop and several global methods for NSGA2_M.<br>
 * @author Kake Abdoulaye
 * @version 1.0
 * @since JAn 1, 2018
 *
 */
public class NSGA2_M implements IAlgorithm , IANSGA2 {

    /**
     * data object with parameters
     */
    public Instance data;
    /**
     * pareto front
     */
    public Set<ParetoSolution> paretoFront;

    public String nomfichierResultat ="Null";

    /**
     *  Constructeur
     * @param data , l'instance à resoudre
     * @param nomfichierResultat , le nom du fichier de resultat
     */
    public NSGA2_M(Instance data ,String nomfichierResultat)
    {
        this.data = data;
        this.nomfichierResultat = nomfichierResultat;
    }

    /**
     *  Constructeur de la classe
     * @param data , l'instance à résoudre
     */
    public NSGA2_M(Instance data)
    {
        this.data = data;
        this.nomfichierResultat = System.getProperty("user.dir")+"/Ressource/Resultat/defaultinstance.data";
    }
    /**
     * Method to initialize the population<br>
     * Here we use random strategy to initialize the genes of an individual:
     * <ul>
     * <li>for jobs of agent A, choose randomly one job as scheduled on a random machine .</li>
     * <li>for jobs of agent B, choose randomly one job as scheduled on a random machine .</li>
     * <li>to verify if this sequence of gene already exists</li>
     * <li>if this gene already exists, re-do the random process. if not, add the individual into the population</li>
     * </ul>
     * We loop the process above until the population is filled
     * @param jobs jobs to schedule
     * @param nbJobs number of all jobs
     * @param nbJobsA number of jobs of agent A
     * @return population initialized
     */
    @Override
    public ArrayList<IndividualMachine> initGroup(ArrayList<Job> jobs, int nbJobs, int nbJobsA){
        ArrayList<IndividualMachine> group = new ArrayList<IndividualMachine>();
        ArrayList<Integer> listeOfIdMachine = new ArrayList<>(data.nbMachine);
        Random random = new Random();
        int size = 0;

        // fill id machine
        for(int index=0;index<data.nbMachine;index++)
        {
            listeOfIdMachine.add(index);
        }

        // if the population is not filled
        while(size < ConstantMachine.SIZE_POPULATION){
            IndividualMachine ind = new IndividualMachine(data);

            // initialize the genes with -1 that mean neither job are scheduled
            for(int j=0; j<nbJobs; j++){
                ind.genes.add(-1);
            }

            if(size >= 0){
                ArrayList<Integer> listeIndex;
                int nombreJobAOrd = random.nextInt(nbJobs)+1;
                listeIndex = new ArrayList<>(nombreJobAOrd);
                //System.out.println("NombreJob : "+nombreJobAOrd);
                int index = 0 ;
                while(nombreJobAOrd != 0)
                {
                    do {
                        index = random.nextInt(nbJobs);
                    }while (listeIndex.contains(index));

                    //System.out.println(index);
                    ind.genes.set(index, RandomOp.randomMachineId(listeOfIdMachine));
                    listeIndex.add(index);
                    nombreJobAOrd -- ;
                }

            }

            ind.validate();
            ind.calculateValueObj();


            boolean exist = true; // to verify if the same gene exists
            if(ind.valide){ // if the individual is valid
                //System.out.println(size+" - "+ind.valuesObj.get(0)+" - "+ind.valuesObj.get(1));
                if(group.size() == 0){
                    group.add(ind);
                    size++;
                }else{
                    for(int k=0; k<group.size(); k++){
                        exist = true;
                        for(int iter=0; iter<nbJobs; iter++){
                            if(ind.genes.get(iter) != group.get(k).genes.get(iter)){
                                exist = false;
                                break;
                            }
                        }
                        if(exist)break;
                    }

                    if(!exist){
                        size++;
                        group.add(ind);
                    }
                    else
                    {
                        size++;
                        group.add(ind);
                    }
                }

                //System.out.println(size+" - "+ind.valuesObj.get(0)+" - "+ind.valuesObj.get(1));
                //System.out.println(ind.valuesObj.get(0)+"\t"+ind.valuesObj.get(1));

            }

        }

        return group;
    }
    /**
     * selection operation to select one individual to attend the crossover
     * <ol>
     * <li>Select nombreP individuals randomly in the current population</li>
     * <li>We return the best individu among nombreP individuals .</li>
     * </ol>
     * @param pop population
     * @return index of individual chosen
     */

    @Override
    public int selection(ArrayList<IndividualMachine> pop,int nombreP){
        Random random = new Random();
        ArrayList<Integer> listeIndexSelect = new ArrayList<>(nombreP);
        int index = 0 ;
        for(int i=0;i<nombreP;i++)
        {
            do {
                index = random.nextInt(pop.size());
            }while (listeIndexSelect.contains(index));

            listeIndexSelect.add(index);
        }

        index = listeIndexSelect.get(0);
        for(int i=1;i<nombreP;i++)
        {
            if(NondominatedSetConstructor.pDomQ(pop.get(index), pop.get(listeIndexSelect.get(i))))
            {
                index = index;
            }
            else if(NondominatedSetConstructor.pDomQ(pop.get(listeIndexSelect.get(i)), pop.get(index)))
            {
                index = listeIndexSelect.get(i);
            }
            else{
                if(random.nextBoolean()){
                    index = random.nextInt(pop.size());
                }
                else{
                    index = i;
                }
            }
        }
        return index;
    }

    /**
     * crossover operation to create new children<br>
     * <ol>
     * <li>Define the number of gene to cross over randomly</li>
     * <li>Choose the index randomly of crossover</li>
     * <li>For the index, do the crossover operation</li>
     * </ol>
     * @param dad dad individual
     * @param mum mom individual
     * @return two children created by crossover
     */
    @Override
    public IndividualMachine[] crossOver(IndividualMachine dad, IndividualMachine mum){
        Random random = new Random();
        IndividualMachine[] children = new IndividualMachine[2];
        children[0] = new IndividualMachine(data);
        children[1] = new IndividualMachine(data);

        int numCross = 1+random.nextInt(dad.genes.size()-1); // number of genes to crossover

        // choose index to do the crossover
        ArrayList<Integer> index = new ArrayList<Integer>();
        while(index.size() < numCross){
            Integer pos = random.nextInt(dad.genes.size());
            if(!index.contains(pos)){
                index.add(pos);
            }
        }
        Collections.sort(index);

        // do the crossover
        if(random.nextDouble() < ConstantMachine.PROB_CROSSOVER){
            for(int i=0; i<dad.genes.size(); i++){
                children[0].genes.add(dad.genes.get(i));
                children[1].genes.add(mum.genes.get(i));
            }
            for(int i=0; i<index.size(); i++){
                children[0].genes.set(index.get(i), mum.genes.get(index.get(i)));
                children[1].genes.set(index.get(i), dad.genes.get(index.get(i)));
            }
        }
        else{
            for(int i=0; i<dad.genes.size(); i++){
                children[0].genes.add(dad.genes.get(i));
                children[1].genes.add(mum.genes.get(i));
            }
        }

        //to calculate their values of objective functions
        children[0].calculateValueObj();
        children[1].calculateValueObj();

        return children;
    }

    /**
     * Mutation operation<br>
     * Choose randomly one position to do the mutation operation<br>
     * Mutation means changing -1 to a number of machine {0,1,...,m} or a number of machine to a other number of machine except the previous number
     *
     * @param children children created after crossover
     * @return children after mutation
     */
    @Override
    public IndividualMachine[] mutation(IndividualMachine[] children){
        ArrayList<Integer> listeOfIdMachine = new ArrayList<>(data.nbMachine);
        Random random = new Random();

        // fill id machine
        for(int index=0;index<data.nbMachine;index++)
        {
            listeOfIdMachine.add(index);
        }

        for(int index=0; index<children.length; index++){
            int pos = random.nextInt(children[0].genes.size()); // position to do the mutation
            if(random.nextDouble() < ConstantMachine.PROB_MUTATION){
                int machineId = children[index].genes.get(pos);
                if(listeOfIdMachine.size() > 1)
                {
                    children[index].genes.set(pos, RandomOp.randomMachineId(listeOfIdMachine,machineId));
                }
                else
                {
                    if(machineId >=0)
                    {
                        children[index].genes.set(pos, -1);
                    }
                    else
                    {
                        children[index].genes.set(pos, RandomOp.randomMachineId(listeOfIdMachine,machineId));
                    }
                }
            }

            // to verify the children's feasibility
            children[index].validate();
            children[index].calculateValueObj();
        }
        return children;
    }

    /**
     * remove the duplicate individuals from the population
     *
     * @param pop population
     * @return population without duplicate
     */

    @Override
    public ArrayList<IndividualMachine> removeDuplicateFromArray(ArrayList<IndividualMachine> pop){
        for (int i = 0; i < pop.size() - 1; i++) {
            for (int j = pop.size() - 1; j > i; j--) {
                if (pop.get(j).valuesObj.get(0) == pop.get(i).valuesObj.get(0) && pop.get(j).valuesObj.get(1) == pop.get(i).valuesObj.get(1)) {
                    pop.remove(j);
                }
            }
        }
        return pop;
    }

    /**
     * print the result to console
     *
     * @param pop population
     * @param generation number of iteration
     */
    public void outputInd(ArrayList<IndividualMachine> pop, String generation){
        for(IndividualMachine ind : pop){
            StringBuilder str = new StringBuilder("");
            str.append("Value A: ").append(ind.valuesObj.get(0)).append(" , ").append("Value B: ").append(ind.valuesObj.get(1));
            str.append(" ---- ");
            str.append(generation);
            System.out.println(str.toString());

        }
        System.out.println(generation + " finish");
    }

    /**
     * Main loop for NSGA2_M <br>
     * The algorithm consists of these steps:
     * <ol>
     * <li>Initiation of population</li>
     * <li>Crossover, mutation</li>
     * <li>Construct non dominated sets</li>
     * <li>Calculate crowded distances</li>
     * <li>Reconstruct the population�� if iteration not end, go to 2</li>
     * </ol>
     * @return hashmap with elements:
     * <ul>
     * <li>key:paretoFront, type of value:ArrayList of IndividualMachine, value:pareto front</li>
     * </ul>
     */
    @Override
    public HashMap<String, Object> execute() {
        Random random = new Random();

        HashMap<String, Object> results = new HashMap<String, Object>();

        ArrayList<ArrayList<IndividualMachine>> nds = null; // save the non dominated set

        ArrayList<IndividualMachine> group = initGroup(data.jobs, data.nbJobs, data.nbJobsA); // initialize the population


        System.out.println("begin");
        for (int ng = 1; ng <= ConstantMachine.NUM_ITERATION; ng++) // the start of algorithm
        {
            ArrayList<IndividualMachine> tempGroup = new ArrayList<IndividualMachine>(); // temporary population

            for (int i = 0; i < group.size(); i++) {
                tempGroup.add(group.get(i));
            }

            for (int i = 0; i < group.size(); i++) {// to the genetic operations
                int dad, mum;
                IndividualMachine dadMoi = null;
                IndividualMachine mumMoi = null;

                do {
                    dad = selection(group,ConstantMachine.NOMBRE_SELECTION);// selection using competition strategy
                    mum = selection(group,ConstantMachine.NOMBRE_SELECTION);
                } while (dad == mum); // when the dad and mom are the same

                dadMoi = group.get(dad);
                mumMoi = group.get(mum);

                IndividualMachine[] temp = crossOver(dadMoi, mumMoi);// crossover
                temp = mutation(temp);// mutation

                for (int j = 0; j < 2; j++) {
                    if (temp[j].valide && NondominatedSetConstructor.pDomQ(temp[j], group.get(dad))) {
                        group.set(dad, temp[j]);
                        break;
                    }
                    if (temp[j].valide && NondominatedSetConstructor.pDomQ(temp[j], group.get(mum))) {
                        group.set(mum, temp[j]);
                    }
                }

            } // end of one iteration

            group.addAll(tempGroup);// combine the two populations


            nds = NondominatedSetConstructor.sortM(group);// sort all individuals



            ArrayList<IndividualMachine> newGroup = new ArrayList<IndividualMachine>();

            int iteration = 0;
            while (iteration < ConstantMachine.SIZE_POPULATION && (newGroup.size() + removeDuplicateFromArray(nds.get(iteration)).size() <= ConstantMachine.SIZE_POPULATION) && (removeDuplicateFromArray(nds.get(iteration)).size() != 0)) {
                // when the new population are not all filled
                nds.set(iteration, removeDuplicateFromArray(nds.get(iteration)));
                newGroup.addAll(nds.get(iteration));
                iteration++;
            }

            int sizeTemp = newGroup.size();

            // if no more individual can be added into the population
            if(nds.get(iteration).size() == 0 && sizeTemp < ConstantMachine.SIZE_POPULATION){
                int i=0, j=0;

                // if the current population are not filled
                while(newGroup.size() < ConstantMachine.SIZE_POPULATION){

                    // choose one individual randomly
                    j = random.nextInt(nds.get(i).size());
                    IndividualMachine ind = new IndividualMachine(data);
                    ind.genes.addAll(nds.get(i).get(j).genes);
                    while(true){
                        int k = random.nextInt(ind.genes.size()); // choose one position
                        if(ind.genes.get(k) >= 0){
                            ind.genes.set(k, -1); // do not schedule this job to make a new feasible individual
                            break;
                        }
                    }

                    ind.validate();
                    ind.calculateValueObj();
                    newGroup.add(ind);
                }
            }else if(sizeTemp < ConstantMachine.SIZE_POPULATION){ // when there are still individuals which can be added

                // calculate the crowded distances
                CrowdingDistanceAssignment.distanceCalculatorM(nds.get(iteration));

                nds.set(iteration, CrowdingDistanceAssignment.sortByDistanceM(nds.get(iteration)));
                int size = newGroup.size();

                //add individuals sorted by crowded distance until the population is filled
                for (int j = 0; j < ConstantMachine.SIZE_POPULATION - size; j++) {
                    newGroup.add(nds.get(iteration).get(j));
                }
            }


            group = newGroup;
            // outputInd(nds.get(0), "G" + ng);
        } // all over;

        results.put("paretoFront", nds.get(0));
        return results;
    }

    @Override
    /**
     * Cette méthode permet de générer un front de Pareto
     */
    public Set<ParetoSolution> generateParetoFront() {
        Timer timer = new Timer();
        timer.setStart(System.currentTimeMillis());
        paretoFront = new HashSet<ParetoSolution>();
        HashMap<String, Object> results = this.execute();
        ArrayList<IndividualMachine> result = (ArrayList<IndividualMachine>) results.get("paretoFront");

        for(int i=0; i<result.size(); i++){
            ParetoSolution paretoSolution = new ParetoSolution();
            paretoSolution.sequence.addAll(result.get(i).jobsOrdonnances);
            paretoSolution.valueObjA = result.get(i).valuesObj.get(0);
            paretoSolution.valueObjB = result.get(i).valuesObj.get(1);
            timer.setEnd(System.currentTimeMillis());
            Commun.sauvegarderResultatMultiCritere_2D(data,result.get(i).jobsOrdonnances,"Optimale Solution NSGA2",this.nomfichierResultat,timer.calculateTimeConsume(),paretoSolution.valueObjA,paretoSolution.valueObjB);
            paretoFront.add(paretoSolution);
        }
       return NondominatedSetConstructor.frontPareto_NonDominance(paretoFront);
        //return paretoFront;
        }


}
