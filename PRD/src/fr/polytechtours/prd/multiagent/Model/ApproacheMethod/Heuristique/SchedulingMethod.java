package fr.polytechtours.prd.multiagent.Model.ApproacheMethod.Heuristique;

import fr.polytechtours.prd.multiagent.Model.ApproacheMethod.MetaHeuristique.CommonModelisation.NondominatedSetConstructor;
import fr.polytechtours.prd.multiagent.Model.IterfaceClass.IAlgorithm;
import fr.polytechtours.prd.multiagent.Model.Modelisation.Instance;
import fr.polytechtours.prd.multiagent.Model.Modelisation.Job;
import fr.polytechtours.prd.multiagent.Model.Modelisation.ParetoSolution;
import fr.polytechtours.prd.multiagent.Model.Util.Commun;
import fr.polytechtours.prd.multiagent.Model.Util.Timer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import static fr.polytechtours.prd.multiagent.Model.Util.Commun.isTab_en_ordre;
import static fr.polytechtours.prd.multiagent.Model.Util.Commun.sommesRessource;

/**
 * Cette classe contient les différents algorithme  et méthode d'affectation des jobs triés sur
 * les machines et représente la classe Heuristique pour le cas multicritère et multimachine
 * <ol>
 * <li> Affection des jobs triés sur les machines en privilégiant la machine la moins chargée </li>
 * <li> Affection des jobs triés sur les machines en commençant par la 1ere puis la deuxlème tant que c'est possible</li>
 * </ol>
 * L'utilisateur doit choisir une des méthodes d'affectation.
 *
 * @author Kake Abdoulaye
 * @version 2.0
 * @since February 18, 2019
 */
public class SchedulingMethod implements IAlgorithm {

    /**
     * date de début des jobs
     */
    private ArrayList<Integer> s_j;
    /**
     * date de fin des jobs
     */
    private ArrayList<Integer> f_j ;

    /**
     * tableau des ressouces maximale de chaque machine
     */
    private ArrayList<ArrayList<Integer>> capaciteRessourceMachine ;

    /**
     * tableau des ressouces  de chaque job , tableauRessourcesJobs
     */
    private ArrayList<ArrayList<Integer>> capaciteRessourceJob ;

    /**
     * nombre de machine
     */
    private int nombre_machine ;
    /**
     * nombre de ressource
     */
    private int nombre_ressource;
    /**
     * la liste des jobs de A
     */
    private ArrayList<Job> listeA ;
    /**
     * la liste des jobs de B
     */
    private ArrayList<Job> listeB ;
    /**
     * l'instance des jobs
     */
    private Instance instance;
    /**
     * le nom du fichier de resultat
     */
    private  String fichierResultat;
    /**
     *  Le front de pareto
     */
    private HashSet<ParetoSolution> paretoFront;
    /**
     * La première affectation
     */
    private Affectation affecte1 ;
    /**
     * La deuxième affectation
     */
    private Affectation affecte2 ;
    /**
     * Le nom de l'heuristique
     */
    private String nomheuristique;

    /**
     * Constructeur de la classe
     *@param data l'instance à  utiliser contenant les jobs à trier  avant l'affectation sur les machines
     *@param sort le type de trie à appliquer à la liste de jobs
     * @param nomHeuristique , le nom de l'heuristique
     * @param affecte1 , la 1ere méthodde d'affectation
     * @param affecte2 , la 2eme méthode d'affectation
     * @param resultFile , le nom du fichier de résultat
     */
    public SchedulingMethod(Instance data , Sorting sort ,String nomHeuristique , Affectation affecte1 , Affectation affecte2, String resultFile)
    {
        // trie des jobs de l'agent A et de l'agent B
        sortingJob(data,sort);

        nombre_machine = data.nbMachine;
        nombre_ressource = data.machines.get(0).resources.size();
        s_j = Commun.getHeuresdebut(data);
        f_j = Commun.getHeuresFin(data);
        capaciteRessourceMachine = Commun.getCapaciteRessourceMachine(data);
        capaciteRessourceJob = Commun.getCapaciteRessourceJob(data);
        this.instance = data;
        this.fichierResultat = resultFile;
        this.affecte1 = affecte1;
        this.affecte2 = affecte2;
        this.nomheuristique = nomHeuristique;
        this.paretoFront = new HashSet<>();
    }

    /**
     * Le contructeur 2 de la classe
     * @param data ,l'instance à  utiliser contenant les jobs à trier  avant l'affectation sur les machines
     * @param sort , le type de trie à appliquer à la liste de jobs
     */
    public SchedulingMethod(Instance data , Sorting sort )
    {
        this(data,sort,"Heuristique affectation par machine",Affectation.resolutionParMachine, Affectation.resolutionParMachine,"Defaultnull");
    }


    /**
     *  Cette méthode permet de Trier les jobs d'une instance , donc trier les jobs de l'agent A et de l'agent B séparemement
     *  en fonction  de la méthode de trie choisie
     * @param data , l'instance
     * @param sort , méthode de trie à suivre
     *
     *  la liste des jobs triés pour l'agent A et pour l'agent B
     */
    private void sortingJob(Instance data, Sorting sort)
    {
        listeA = new ArrayList<>();
        listeB = new ArrayList<>();
        for(int i = 0 ; i<data.nbJobs;i++)
        {
            if(data.jobs.get(i).belongTo.equalsIgnoreCase("A"))
            {
                listeA.add(data.jobs.get(i));
            }
            else if(data.jobs.get(i).belongTo.equalsIgnoreCase("B"))
            {
                listeB.add(data.jobs.get(i));
            }
        }
        SortingMethod method  = new SortingMethod(listeA);
        SortingMethod method2  = new SortingMethod(listeB);

        // Trier les jobs en fonction du choix de tri
        if (sort.equals(Sorting.Cmax)) {
            listeA = method.trierCCmaxMaxRessources();
            listeB = method2.trierCCmaxMaxRessources();
        } else if (sort.equals(Sorting.MakeSpan)) {
            listeA = method.trierCCmaxSommeRessources();
            listeB = method2.trierCCmaxSommeRessources();
        } else if (sort.equals(Sorting.ShotestTime)) {
            listeA = method.trierDuree();
            listeB = method2.trierDuree();
        } else {
           // jobsSorted = data.jobs;
        }
    }

    /**
     *  Cette méthode permet d'affecter les jobs sur les différentes machines en fonction de la politique d'affectation
     * @param listeJob , la liste des jobs à affecter
     * @param affecte , la politique d'affectation des jobs
     * @return la liste des jobs ordonnancé par machine , chaque machine a une liste de Job
     */
    public ArrayList<ArrayList<Integer>> affectationJobs(ArrayList<Job> listeJob,Affectation affecte)
    {
        ArrayList<ArrayList<Integer>> jobsOrdonance = null;
        if(affecte.equals(Affectation.resolutionParMachineMoinsCharger))
        {
            jobsOrdonance = resolveMachineLessUsedMachine(listeJob);
        }
        else if(affecte.equals(Affectation.resolutionParMachine))
        {
            jobsOrdonance = resolveByMachine(listeJob);
        }
        return jobsOrdonance;
    }



    /**
     * @return la liste des jobs ordonnancés par machine
     * @brief Affectation  des jobs sur les machines privilégiant la machine la moins chargée
     */

    private ArrayList<ArrayList<Integer>> resolveMachineLessUsedMachine(ArrayList<Job> jobsSorted) {

        ArrayList<ArrayList<Integer>> jobsOrdonnances = new ArrayList<>();


        for (int m = 0; m < nombre_machine; m++) {
            ArrayList<Integer> jobmachine = new ArrayList<Integer>(1);
            jobsOrdonnances.add(jobmachine);
        }

        while (jobsSorted.size() != 0) {
            int[][] chargeEnsemblesMachine = new int[nombre_machine][2];
            for (int m = 0; m < nombre_machine; m++) {
                chargeEnsemblesMachine[m][0] = m;
                int charge = 0;
                for (int i = s_j.get(jobsSorted.get(0).id); i < f_j.get(jobsSorted.get(0).id); i++) {
                    for (int r = 0; r < nombre_ressource; r++) {
                        for (int j = 0; j < jobsOrdonnances.get(m).size(); j++) {
                            if (s_j.get(jobsOrdonnances.get(m).get(j)) <= i && f_j.get(jobsOrdonnances.get(m).get(j)) > i) {
                                charge += capaciteRessourceJob.get(jobsOrdonnances.get(m).get(j)).get(r);
                            }

                        }
                    }
                }
                chargeEnsemblesMachine[m][1] = charge;
            }
            //Trie à bulle des machines suivant la charge sur chacune d'elles
            boolean tab_en_ordre = false;
            int taille = nombre_machine;
            while (!tab_en_ordre) {
                tab_en_ordre = true;
                for (int i = 0; i < taille - 1; i++)
                    if (chargeEnsemblesMachine[i][1] > chargeEnsemblesMachine[i + 1][1]) {
                        tab_en_ordre = isTab_en_ordre(chargeEnsemblesMachine, i);
                    }
                taille--;
            }

            Job numeroJob = jobsSorted.get(0);

            for (int m = 0; m < chargeEnsemblesMachine.length; m++) {
                if (jobsSorted.size() != 0 && numeroJob.id == jobsSorted.get(0).id) {

                    ArrayList<Integer> Sm = new ArrayList<>();
                    for (int i = 0; i < jobsOrdonnances.get(chargeEnsemblesMachine[m][0]).size(); i++) {

                        if (s_j.get(jobsSorted.get(0).id) < f_j.get(jobsOrdonnances.get(chargeEnsemblesMachine[m][0]).get(i)) &&
                                f_j.get(jobsSorted.get(0).id) > s_j.get(jobsOrdonnances.get(chargeEnsemblesMachine[m][0]).get(i))) {
                            Sm.add(jobsOrdonnances.get(chargeEnsemblesMachine[m][0]).get(i));
                        }

                    }

                    boolean jobOrdonancable = true;

                    for (int i = s_j.get(jobsSorted.get(0).id); i < f_j.get(jobsSorted.get(0).id); i++) {
                        for (int r = 0; r < nombre_ressource; r++) {
                            int sommesRessources = sommesRessource(s_j,f_j,capaciteRessourceJob,Sm,i,r);
                            sommesRessources += capaciteRessourceJob.get(jobsSorted.get(0).id).get(r);
                            if (sommesRessources > capaciteRessourceMachine.get(chargeEnsemblesMachine[m][0]).get(r))
                                jobOrdonancable = false;
                        }
                    }

                    if (jobOrdonancable) {
                        jobsOrdonnances.get(chargeEnsemblesMachine[m][0]).add(jobsSorted.get(0).id);
                        jobsSorted.remove(numeroJob);
                    }
                }
            }

            if (jobsSorted.size() != 0 && numeroJob.id == jobsSorted.get(0).id) {
                jobsSorted.remove(numeroJob);
            }
        }

        return jobsOrdonnances;
    }



    /**
     * Affectation privilégiant la machine par machine en commençant par la premiere machine , puis la deuxième ainsi de suite
     * @return la liste des jobs ordonnancés par machine
     *
     */
    private ArrayList<ArrayList<Integer>> resolveByMachine(ArrayList<Job> jobsSorted) {
        ArrayList<ArrayList<Integer>> jobsOrdonnances = new ArrayList<>();
        for (int m = 0; m < nombre_machine; m++) {
            ArrayList<Integer> jobsMachine = new ArrayList<>();

            ArrayList<Job> Lm = new ArrayList<>(jobsSorted);

            while (Lm.size() != 0) {
                ArrayList<Integer> Sm = new ArrayList<>();
                for (int i = 0; i < jobsMachine.size(); i++) {
                    if (s_j.get(Lm.get(0).id) < f_j.get(jobsMachine.get(i)) && f_j.get(Lm.get(0).id) > s_j.get(jobsMachine.get(i))) {
                        Sm.add(jobsMachine.get(i));
                    }
                }

                boolean jobOrdonancable = true;

                for (int i = s_j.get(Lm.get(0).id); i < f_j.get(Lm.get(0).id); i++) {
                    for (int r = 0; r < nombre_ressource; r++) {
                        int sommesRessources = sommesRessource(s_j,f_j,capaciteRessourceJob,Sm,i,r);
                        sommesRessources += capaciteRessourceJob.get(Lm.get(0).id).get(r);
                        if (sommesRessources > capaciteRessourceMachine.get(m).get(r))
                            jobOrdonancable = false;
                    }
                }

                if (jobOrdonancable) {
                    jobsMachine.add(Lm.get(0).id);
                    jobsSorted.remove(Lm.get(0));
                }
                Lm.remove(Lm.get(0));
            }
            jobsOrdonnances.add(jobsMachine);
        }
        return jobsOrdonnances;
    }



    /**
     * To execute the algorithm.<br>
     * Before executing the algorithm, user should always load the parameters first.<br>
     * This method returns only one Pareto solution for certain algorithms, in order to get the pareto front
     * We should always call the method <strong>generateParetoFront()</strong> in Interface {@link IAlgorithm}
     *  On this case the algorithm sort all jobs after schedule them on differents machin at the end xrite the result on a file.
     *
     * @return important results got during the execution of algorithm
     */
    @Override
    public HashMap<String, Object> execute() {
        HashMap<String, Object> results = new HashMap<String, Object>();
        paretoFront = new HashSet<ParetoSolution>();
        ArrayList<Job> listeJobOrdonnanceA = new ArrayList<>();
        int valeurObjA = 0 ;
        int prendre ;
        Timer timer = new Timer();
        ArrayList<ArrayList<Integer>> ordonnance = affectationJobs(listeA,affecte1);
        ArrayList<ArrayList<ArrayList<Integer>>> jobsOrdonnancesAgents ;
        for(int index = 0 ; index < ordonnance.size() ; index++)
        {
            valeurObjA += ordonnance.get(index).size();
            for(int indexj =0 ;indexj < ordonnance.get(index).size();indexj ++)
            {
                listeJobOrdonnanceA.add(instance.jobs.get(ordonnance.get(index).get(indexj)));
            }
        }

        ArrayList<Job> tousJobOrdonnace = new ArrayList<>();


        for(int valeur = 0 ; valeur <= valeurObjA ; valeur++)
        {
            ParetoSolution paretoSolution = new ParetoSolution();
            timer.setStart(System.currentTimeMillis());
            prendre = 0;
            for(int count = valeur;count >0 ;count--)
            {
                tousJobOrdonnace.add(listeJobOrdonnanceA.get(prendre));
                prendre++;
            }
            tousJobOrdonnace.addAll(listeB);

            ordonnance = affectationJobs(tousJobOrdonnace,affecte2);
            tousJobOrdonnace.clear();
            jobsOrdonnancesAgents = Commun.transform2DTo3D(instance,ordonnance);
            // Calcul du nombre de job de chaque Agent
            int calcul = 0;
            ArrayList<Integer> valueObj = new ArrayList<>();
            for(int i = 0; i < jobsOrdonnancesAgents.size(); i++){
                for (int machine= 0 ; machine < instance.nbMachine; machine++) {
                    calcul += jobsOrdonnancesAgents.get(i).get(machine).size();
                }
                if(i==0)
                    valueObj.add(instance.nbJobsA-calcul);
                else if(i==1)
                    valueObj.add((instance.nbJobs-instance.nbJobsA)-calcul);
                calcul = 0 ;
            }
            paretoSolution.valueObjA = valueObj.get(0);
            paretoSolution.valueObjB = valueObj.get(1);
            for ( int machine= 0 ; machine < instance.nbMachine; machine++) {
                paretoSolution.sequence.add(jobsOrdonnancesAgents.get(0).get(machine));
                paretoSolution.sequence.add(jobsOrdonnancesAgents.get(1).get(machine));
            }
            paretoFront.add(paretoSolution);
            timer.setEnd(System.currentTimeMillis());
            Commun.sauvegarderResultatMultiCritere(instance,jobsOrdonnancesAgents,this.nomheuristique,fichierResultat,timer.calculateTimeConsume(),valueObj.get(0),valueObj.get(1));
            ordonnance.clear();
        }

        results.put("paretoFront",paretoFront);

        return results;
    }


    /**
     * To generate the pareto front with parameters adapted.<br>
     * This method should be called after the loading of parameters with method <strong>loadParam</strong> in Interface {@link IAlgorithm}
     *
     * @return set of pareto solutions of type {@link ParetoSolution}
     */
    @Override
    public   Set<ParetoSolution> generateParetoFront() {
        HashMap<String, Object> results = this.execute();
        return NondominatedSetConstructor.frontPareto_NonDominance(paretoFront);
    }


    /***
     * Enumération des différents méthode de Tri
     */
    public enum Sorting {
        MakeSpan,
        Cmax,
        ShotestTime
    }

    /**
     * Enumération des différents méthode d'affectation des jobs sur les machines
     */
    public enum Affectation{
        resolutionParMachine,
        resolutionParMachineMoinsCharger
    }

}
