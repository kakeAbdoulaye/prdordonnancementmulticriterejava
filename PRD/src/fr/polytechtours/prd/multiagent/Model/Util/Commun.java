package fr.polytechtours.prd.multiagent.Model.Util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import fr.polytechtours.prd.multiagent.Model.Modelisation.Instance;
import fr.polytechtours.prd.multiagent.Model.Modelisation.Job;
import fr.polytechtours.prd.multiagent.Model.Modelisation.Machine;

/**
 * 
 * This class propose the common methods for the general use
 * 
 * Specially, the method for creating jobs and saving to files, 
 * the method for reading jobs from files,
 * the method for getting the end time maximum of a sequence of jobs
 * 
 * @author Boyang Wang ,Kake Abdoulaye
 * @version 1.2
 * @since Nov 30, 2017
 * Modifié : Jan 9 2019 by Kake Abdoulaye
 * 
 */
public class Commun {
	
	/**
	 * Method used to get the end time maximum of a sequence of jobs sorted
	 * 
	 * @param jobs a list of jobs
	 * @return end time max of a sequence of jobs
	 */
	public static int getMaxEnd(ArrayList<Job> jobs){
		int maxEnd = jobs.get(0).end;
		for(int i=0; i<jobs.size(); i++){
			if(jobs.get(i).end > maxEnd) {
				maxEnd = jobs.get(i).end;
			}
		}
		
		return maxEnd;
	}

	/**
	 * Cette méthode permet de créer des instances contenant les valeurs pseudo aléatoire
	 *  en fonction des paramètres.
	 *
	 * @param nbInstance le nombre d'instance à créer
	 * @param nbJobs ,  le nombre de jobs
	 * @param nbRessources , le nombre de ressources
	 * @param percJobAgentA , le pourcentage de job de l'agent A
	 * @param nbMachine , le nombre de machine
	 * @param horizon , l'horizon des dates de début et de fin des jobs
	 */
	public static void createInstanceFile(int nbInstance,int nbJobs, int nbRessources, int percJobAgentA , int nbMachine , int horizon) {
		int dossier = 1;
		String cheminDossier = "Ressource/Instances/Dossier_"+dossier+"/NbJob_"+nbJobs+"/NbRes_"+nbRessources+"/NbMac_"+nbMachine;
		String cheminFichier="";
		File repertoireDossier = new File(cheminDossier);
		boolean exit = repertoireDossier.exists();
		if(exit == false)
		{
			repertoireDossier.mkdirs();
		}
		else
		{

			while(repertoireDossier.exists() && dossier < 1000)
			{
				dossier++;
				cheminDossier = "Ressource/Instances/Dossier_"+dossier+"/NbJob_"+nbJobs+"/NbRes_"+nbRessources+"/NbMac_"+nbMachine;
				repertoireDossier = new File(cheminDossier);
			}
			repertoireDossier.mkdirs();
		}

		for(int nbDossier=0;nbDossier<nbInstance; nbDossier++)
		{

			cheminFichier = cheminDossier+"/instance-"+(nbDossier+1)+"-"+nbJobs+"-"+nbRessources+"-"+nbMachine+".data";
			createRandomJobsAndResources(nbJobs,nbRessources,percJobAgentA,nbMachine,horizon,cheminFichier);
		}
	}

	/**
	 * Method to create random jobs and to save a file
	 * The total amount of each resource is fixed to 1000
	 * Each consumption of resource is set randomly between 1 and 1000
	 * Each start time is set randomly between 1 and Horizon mns (one day)
	 * Each end time is set randomly between s+1 and Horizon-s
	 *
	 * @param nbJobs total number of jobs
	 * @param nbResouorces total number of resources
	 * @param percJobAgentA percentage of jobs which belongs to agent A
	 * @param nbMachine total number of machins
	 * @param horizon , the limit of date of every job
	 * @param filename file name to save jobs
	 */
	public static void createRandomJobsAndResources(int nbJobs, int nbResouorces, int percJobAgentA , int nbMachine , int horizon, String filename){
		ArrayList<Job> jobs = new ArrayList<Job>();
		ArrayList<Machine> machines = new ArrayList<>(nbMachine);

		for(int m=0; m<nbMachine; m++)
		{
			Machine machine = new Machine();

			for(int i=0; i<nbResouorces; i++)
			{
				machine.resources.add(1000);
			}
			machines.add(machine);
		}

		
		int nbJobsAgentA = nbJobs*percJobAgentA / 100;
		Random random = new Random();
		for(int i=0 ;i<nbJobs; i++){
			Job job = new Job();
			job.id = i;
			job.start = random.nextInt(horizon);//1440
			if(job.start == horizon - 1) //1439
				job.end = horizon; //1440
			else
				job.end = job.start + 1 + random.nextInt((horizon - job.start - 1));
			for(int j=0; j<nbResouorces; j++){
				job.consumes.add(1 + random.nextInt(1000));
			}
			if(i < nbJobsAgentA){
				job.belongTo = "A";
			}
			else{
				job.belongTo = "B";
			}
			jobs.add(job);
		}
		
		File file = new File(filename);
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(file));
			bw.write(nbJobs+" "+machines.get(0).resources.size()+" "+nbJobsAgentA+" "+nbMachine);
			bw.newLine();
			for(int m=0; m<machines.size(); m++) {
				for (int i = 0; i < machines.get(m).resources.size(); i++) {
					bw.write(machines.get(m).resources.get(i) + " ");
				}
				bw.newLine();
			}
			for(int i=0; i<jobs.size(); i++){
				bw.write(jobs.get(i).id + " " + jobs.get(i).start + " " + jobs.get(i).end + " " + jobs.get(i).belongTo + " ");
				for(int j=0; j<machines.get(0).resources.size(); j++){
					bw.write(jobs.get(i).consumes.get(j) + " ");
				}
				if(i != jobs.size() -1)
					bw.newLine();
			}
			bw.flush();
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Read from a file to load all the jobs saved in this file
	 * 
	 * The result to return is of type the HashMap: 
	 * the keys are machine, jobs which are related to a machine and a list of jobs, 
	 * numJob for number of job, numResource for number of resources,
	 * numJobAgentA for number of job of agent A
	 * 
	 * @param fileName name of file to read
	 * @param typeOfSort the type of sorting
	 * @see Job
	 * @return a HashMap with a machine (key:machine) and a list of jobs (key:jobs)
	 */
	public static HashMap<String, Object> ReadDataFromFile(String fileName, int typeOfSort){
		
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		try {
			@SuppressWarnings("resource")
			BufferedReader reader = new BufferedReader(new FileReader(fileName));
			String line = null;
			
			line=reader.readLine();
			String item[] = line.split(" ");
			int numJob = Integer.parseInt(item[0]);
			int numResource = Integer.parseInt(item[1]);
			int numJobAgentA = Integer.parseInt(item[2]);
			int nombreMachine = Integer.parseInt(item[3]);
			result.put("numJob", numJob);
			result.put("numResource", numResource);
			result.put("numJobAgentA", numJobAgentA);
			result.put("nombreMachine",nombreMachine);
			result.put("filename",fileName);

			ArrayList<Machine> machines = new ArrayList<Machine>(nombreMachine);
			for(int index = 0 ; index < nombreMachine ; index++)
			{
				line=reader.readLine();
				item = line.split(" ");
				Machine machine = new Machine();
				for(int i=0; i<numResource; i++){
					machine.resources.add(Integer.parseInt(item[i]));
				}
				machines.add(machine);
			}
			result.put("machine", machines);
			
			List<Job> jobs = new ArrayList<Job>();
            while((line=reader.readLine())!=null){ 
                item = line.split(" ");
                Job job = new Job();
                job.id = Integer.parseInt(item[0]);
                job.start = Integer.parseInt(item[1]);
                job.end = Integer.parseInt(item[2]);
				//System.out.println(job.id+" : "+job.start+" - "+ job.end);
                job.belongTo = item[3];
                for(int i=0; i<numResource; i++){
                	job.consumes.add(Integer.parseInt(item[i+4]));
                }
                job.weight = 1;
                job.calculateFactorOfSort(typeOfSort);
                jobs.add(job);
            }
            result.put("jobs", jobs);
            
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return result;
	}

	/**
	 * Cette méthode Permet de sauvegarder le résultat d'une résolution d'instance dans un fichier de résultat
	 *
	 * @param instance L'instance que l'on a résolu
	 * @param jobsOrdonnances Le tableau de jobs ordonancés
	 * @param typeResolution Le type de résolution
	 * @param fichierResultat Le fichier où seront stockés les résulats
	 * @param dureeExecution La durée d'execution de la résolution
	 * @param valeurObjA , la valeur objectif de l'agent A
	 * @param valeurObjB , la valeur objectif de l'agent B
	 */
	public static void sauvegarderResultatMultiCritere(Instance instance, ArrayList<ArrayList<ArrayList<Integer>>>jobsOrdonnances, String typeResolution, String fichierResultat, double dureeExecution, double valeurObjA, double valeurObjB){

		File file = new File(fichierResultat);
		try {
			FileWriter writer = new FileWriter(file,true);
			writer.write("Instance : "+instance.filename+"\n");
			writer.write("Type de résolution  : "+typeResolution+"\n");
			writer.write("Optimal Value (A - B) = "+valeurObjA+" - "+valeurObjB+"\n");
			writer.write("temps écoulé (en secondes): "+dureeExecution+"\n");
			writer.write("IdAgent\tIdMachine\tN°Job\tSi\tFi"+"\n");
			int idJob=0;
			for ( int agent = 0 ; agent < 2 ; agent++) {
				for ( int machine= 0 ; machine < jobsOrdonnances.get(agent).size(); machine++) {
					for(int job=0; job<jobsOrdonnances.get(agent).get(machine).size(); job++){
						idJob=jobsOrdonnances.get(agent).get(machine).get(job);
						writer.write((agent+1)+"\t"+(machine+1)+"\t"+(idJob+1)+"\t"+instance.jobs.get(idJob).start+"\t"+instance.jobs.get(idJob).end+"\t\n");
					}
				}
			}
			writer.write("IdAgent\tNombre Job Ordonnancé\n");
			int calcul = 0;
			for(int i = 0; i < jobsOrdonnances.size(); i++){
				for (int machine= 0 ; machine < jobsOrdonnances.get(i).size(); machine++) {
					calcul += jobsOrdonnances.get(i).get(machine).size();
				}
				writer.write(i+1+"\t"+calcul+"\n");
				calcul = 0 ;
			}

			writer.flush();
			if (writer != null)
				writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Cette méthode permet d'écrire les résultats dans un fichier text
	 * @param instance , l'instance contenant les données
	 * @param jobsOrdonnances , la liste des jobs ordonnancés par machine
	 * @param typeResolution , le type de résolution : le nom de l'heuristique ou de la méta heuristique
	 * @param fichierResultat , le nom du fichier de résultat
	 * @param dureeExecution , la durée de l'exécution
	 * @param valeurObjA , la valeur objective de l'agent A
	 * @param valeurObjB , la valeur objective de l'agent B
	 */
	public static void sauvegarderResultatMultiCritere_2D(Instance instance, ArrayList<ArrayList<Integer>>jobsOrdonnances, String typeResolution, String fichierResultat, double dureeExecution, double valeurObjA, double valeurObjB){

		ArrayList<ArrayList<ArrayList<Integer>>> jobsOrdonnancesAgents = transform2DTo3D(instance,jobsOrdonnances);
		sauvegarderResultatMultiCritere(instance,jobsOrdonnancesAgents,typeResolution,fichierResultat,dureeExecution,valeurObjA,valeurObjB);

	}

	/**
	 * Cette méthode permet de transformer une liste de 2 Dimensions contenant les jobs rangés par machine à une liste à 3 Dimensions contenant les jobs par agent puis par machine
	 * @param instance , l'instance contenant les données
	 * @param jobsOrdonnances ,  la liste de 2 Dimensions contenant les jobs rangés par machine
	 * @return la nouvelle liste 3D contenant les jobs par agent puis par machine
	 */
	public static ArrayList<ArrayList<ArrayList<Integer>>> transform2DTo3D(Instance instance, ArrayList<ArrayList<Integer>>jobsOrdonnances)
	{
		ArrayList<ArrayList<ArrayList<Integer>>> jobsOrdonnancesAgents = new ArrayList<>(2);
		for ( int agent = 0 ; agent < 2 ; agent++) {
			ArrayList<ArrayList<Integer>> ma = new ArrayList<>();
			for ( int machine= 0 ; machine < jobsOrdonnances.size(); machine++) {
				ArrayList<Integer> jobsMachines = new ArrayList<>();
				for(int job=0; job<jobsOrdonnances.get(machine).size() ; job++){
					if (getJobOwner(instance,jobsOrdonnances.get(machine).get(job)) == agent)
					{
						jobsMachines.add(jobsOrdonnances.get(machine).get(job));
					}
				}
				ma.add(jobsMachines);
			}

			jobsOrdonnancesAgents.add(ma);
		}
		return jobsOrdonnancesAgents;

	}

	/**
	 * Cette méthode permet de recupérer les dates de début de chaque Jobs d'une instance données
	 * @param instance , l'instance
	 * @return la liste des dates de débuts des jobs
	 */
	public static ArrayList<Integer> getHeuresdebut(Instance instance)
	{
		int nombre_jobs = instance.nbJobs;
		ArrayList<Integer> debutJobs = new ArrayList<>(nombre_jobs); // date de début des jobs
		for (int index = 0; index < nombre_jobs; index++) {
			debutJobs.add(instance.jobs.get(index).start);
		}

		return debutJobs;
	}
	/**
	 * Cette méthode permet de recupérer les dates de fin de chaque Jobs d'une instance données
	 * @param instance , l'instance
	 * @return la liste des dates de fin des jobs
	 */
	public static ArrayList<Integer> getHeuresFin(Instance instance)
	{
		int nombre_jobs = instance.nbJobs;
		ArrayList<Integer> finJobs = new ArrayList<>(nombre_jobs); // date de fin des jobs
		for (int index = 0; index < nombre_jobs; index++) {
			finJobs.add(instance.jobs.get(index).end);
		}

		return finJobs;
	}

	/**
	 * Cette méthode permet de recupérer les capacités des ressources de chaque machine
	 * @param instance , l'instance (données)
	 * @return la liste des capacités des ressources par machine
	 */
	public  static  ArrayList<ArrayList<Integer>> getCapaciteRessourceMachine(Instance instance)
	{
		int nombre_machine = instance.nbMachine;
		int nombre_ressource = instance.machines.get(0).resources.size();


		ArrayList<ArrayList<Integer>> capaciteRessourceMachine = new ArrayList<ArrayList<Integer>>(nombre_machine); // tableau des ressouces maximale de chaque machine
		for (int machine = 0; machine < nombre_machine; machine++) {
			capaciteRessourceMachine.add(new ArrayList<>(nombre_ressource));
			for (int ressource = 0; ressource < nombre_ressource; ressource++) {
				capaciteRessourceMachine.get(machine).add(instance.machines.get(machine).resources.get(ressource));
			}
		}

		return capaciteRessourceMachine;
	}

	/**
	 * Cette méthode permet de recupérer les capacités des ressources de chaque job
	 * @param instance , l'instance (données)
	 * @return la liste des capacités des ressources par job
	 */
	public  static  ArrayList<ArrayList<Integer>> getCapaciteRessourceJob(Instance instance)
	{
		int nombre_jobs = instance.nbJobs;
		int nombre_ressource = instance.machines.get(0).resources.size();


		ArrayList<ArrayList<Integer>> capaciteRessourceJob = new ArrayList<ArrayList<Integer>>(nombre_jobs); // tableau des ressouces  de chaque job , tableauRessourcesJobs
		for (int job = 0; job < nombre_jobs; job++) {
			capaciteRessourceJob.add(new ArrayList<>(nombre_ressource));
			for (int ressource = 0; ressource < nombre_ressource; ressource++) {
				capaciteRessourceJob.get(job).add(instance.jobs.get(job).consumes.get(ressource));
			}
		}

		return capaciteRessourceJob;
	}

	/**
	 * Cette méthode permet de savoir à quel Agent appartient un Job
	 * @param data , l'instance  contenant la liste des jobs
	 * @param idJob , l'identifiant du job
	 * @return l'identifiant du propriétaire ou de l'agent
	 */

	public static int getJobOwner(Instance data, int idJob)
	{
		if(data.jobs.get(idJob).belongTo.equalsIgnoreCase("A"))
		{
			return 0;
		}
		else if(data.jobs.get(idJob).belongTo.equalsIgnoreCase("B"))
		{
			return 1;
		}
		else
			return 0;
	}
	/**
	 * Cette méthode permet de permuter  les colonnes d'un éléments i d'un tableau d'entier
	 * @param chargeEnsemblesMachine , le tablea d'entier
	 * @param i , la ligne i
	 * @return Vrai ou faux
	 */
	public static boolean isTab_en_ordre(int[][] chargeEnsemblesMachine, int i) {
		boolean tab_en_ordre;
		int tempId = chargeEnsemblesMachine[i][0];
		int tempCharge = chargeEnsemblesMachine[i][1];
		chargeEnsemblesMachine[i][0] = chargeEnsemblesMachine[i + 1][0];
		chargeEnsemblesMachine[i][1] = chargeEnsemblesMachine[i + 1][1];
		chargeEnsemblesMachine[i + 1][0] = tempId;
		chargeEnsemblesMachine[i + 1][1] = tempCharge;
		tab_en_ordre = false;
		return tab_en_ordre;
	}

	/**
	 *  Cette méthode permet de calculer la consommation en ressource
	 * @param s_j , la liste de date de départ de chaque job
	 * @param f_j , la liste de date de fin de chaque job
	 * @param capaciteRessourceJob , la liste de quantité de ressource de chaque job
	 * @param Sm , la liste des jobs déjà ordonancé
	 * @param i , le numéro de machine
	 * @param r , le numéro de la ressource
	 * @return la consommation en ressource
	 */
	public static int sommesRessource(ArrayList<Integer> s_j, ArrayList<Integer> f_j,ArrayList<ArrayList<Integer>> capaciteRessourceJob,ArrayList<Integer> Sm, int i , int r)
	{
		int sommesRessources = 0;
		for (int j = 0; j < Sm.size(); j++) {
			if (s_j.get(Sm.get(j)) <= i && f_j.get(Sm.get(j)) > i)
				sommesRessources += capaciteRessourceJob.get(Sm.get(j)).get(r);
		}
		return sommesRessources;
	}
}
