package fr.polytechtours.prd.multiagent.Model.ExactMethod;

import fr.polytechtours.prd.multiagent.Model.Modelisation.Instance;
import fr.polytechtours.prd.multiagent.Model.Modelisation.ParetoSolution;
import fr.polytechtours.prd.multiagent.Model.Util.Timer;

import java.io.File;
import java.util.Iterator;
import java.util.Set;

/**
 * Test class for ExactMethod methods
 *
 * @author Boyang Wang
 * @version 1.0
 * @since Feb 1, 2018
 *
 */
public class TestExact {

	public static void main(String[] args){
		//
		Instance data = new Instance("Ressource/Instances/Dossier_1/NbJob_20/NbRes_3/NbMac_4/instance-1-20-3-4.data");
		File file = new File("Ressource/Resultat/MethodeExacteText2.data");
		if(file.exists()) file.delete();

		Timer timer = new Timer();
		timer.setStart(System.currentTimeMillis());
		EpsilonConstraint epsilonContraint = new EpsilonConstraint(data,"Ressource/Resultat/MethodeExacteText2.data");
		Set<ParetoSolution> front = epsilonContraint.generateParetoFront();
		timer.setEnd(System.currentTimeMillis());

		for(Iterator<ParetoSolution> iter = front.iterator(); iter.hasNext(); ){
			ParetoSolution solution = iter.next();
			StringBuilder builder = new StringBuilder("[");
			for(int i=0; i<solution.sequence.size(); i++){
				builder.append(solution.sequence.get(i)).append(",");
			}
			builder.deleteCharAt(builder.length() - 1);
			builder.append("]");
			System.out.println("------------------------------------------");
			System.out.println("Jobs to schedule: " + builder.toString());
			System.out.println("Number of jobs rejected of agent A: "+solution.valueObjA+" , "+"Number of jobs rejected of agent B: "+solution.valueObjB);
			System.out.println("------------------------------------------");
		}
		System.out.println("Time consumed: "+timer.calculateTimeConsume());


	}
}
