package fr.polytechtours.prd.multiagent.Model.ExactMethod;


import java.io.File;
import java.util.*;

import fr.polytechtours.prd.multiagent.Model.IterfaceClass.IAlgorithm;
import fr.polytechtours.prd.multiagent.Model.ApproacheMethod.MetaHeuristique.CommonModelisation.NondominatedSetConstructor;
import fr.polytechtours.prd.multiagent.Model.Modelisation.Instance;
import fr.polytechtours.prd.multiagent.Model.Modelisation.ParetoSolution;
import fr.polytechtours.prd.multiagent.Model.Util.Commun;
import ilog.concert.*;
import ilog.cplex.IloCplex;

/**
 * Implementation of algorithm Epsilon Constraint <br>
 * This algorithm is an ExactMethod method to solve the multi-objectifs problems<br>
 * To solve the problem, we use the library of CPLEX as the solver.<br>
 * In order to get the optimal pareto front, we need to solve the symmetric problem
 *
 * @author Boyang Wang , Kake Abdoulaye
 * @version 1.0
 * @since Jan 1, 2018
 *
 */
public class EpsilonConstraint implements IAlgorithm{
    /**
     * data object with parameters
     */
    public Instance data;
    /**
     * pareto front
     */
    public Set<ParetoSolution> paretoFront;


    /**
     * fontions objectif de A
     */
    private IloIntExpr numberJobA;

    /**
     * Fonction objectif de B
     */
    private IloIntExpr numberJobB;

    /**
     * Valeur ojective de A
     */
    private int calculA= 0;

    /**
     * valeur Objective de B
     */
    private  int calculB= 0;

    /**
     * La variable X pour les jobs
     */
    private IloIntVar [][] X ;

    /**
     * Le nom du fichier resultat
     */
    private String nomfichierResultat="";


    /**
     * Constructeur
     * @param data , les instances
     * @param nomfichierResultat , le nom du fichier de résultat
     */
    public EpsilonConstraint(Instance data , String nomfichierResultat)
    {
        this.nomfichierResultat = nomfichierResultat;
        this.data = data;
    }

    /**
     * Cette méthode permet d'initialiser le model
     * @return un model CPLEX , initailiser
     */
    public IloCplex createModel()
    {
        IloCplex model = null;

        //get the max end time of all jobs
        int T = Commun.getMaxEnd(data.jobs);

        try
        {
            //initiation of cplex object
            model = new IloCplex();
            model.setOut(null); // ne pas afficher les etapes dans la console

            //initiation of x as 0 for scheduled and 1 for not scheduled
            X = new IloIntVar[data.jobs.size()][data.nbMachine];
            for(int job=0;job<data.jobs.size();job++)
            {
                for(int machine =0;machine<data.nbMachine;machine++)
                {
                    X[job][machine] = model.intVar(0,1);
                }
            }


            //Ici on créer les variables Y qui valent 1 si le jobs i est exécuté à l'instant t sur une machine m
            IloIntVar [][][] Y = new IloIntVar[data.jobs.size()][data.nbMachine][T+1];
            for(int job=0;job<data.jobs.size();job++)
            {
                for(int machine =0;machine<data.nbMachine;machine++)
                {
                    for(int t =0;t<T+1;t++)
                    {
                        Y[job][machine][t] = model.intVar(0,1);
                    }
                }
            }

            //Ici on ajoute la contrainte qui vérifie que si un jobs est exécuté,
            //il est exécuté durant la période [S[i],F[i][
            for (int job=0 ; job< data.jobs.size(); job++) {
                for (int machine= 0 ; machine < data.nbMachine; machine++) {
                    IloIntExpr temps_total_execution_job = model.intExpr() ;
                    for (int temps=data.jobs.get(job).start ; temps < data.jobs.get(job).end ; temps++) {
                        temps_total_execution_job = model.sum(Y[job][machine][temps],temps_total_execution_job);
                    }
                    model.addEq(temps_total_execution_job,model.prod((data.jobs.get(job).end - data.jobs.get(job).start),model.diff(1,X[job][machine])));
                }
            }

            //Ici on ajoute la contrainte qui vérifie que si un jobs est exécuté,
            //pour chaque t les valeurs de ressources sur la machine m ne sont pas dépassée
            for ( int ressource=0; ressource < data.jobs.get(0).consumes.size() ; ressource++) {
                for ( int machine = 0 ; machine < data.nbMachine ; machine++) {
                    for (int temps= 0 ; temps < T+1 ; temps++) {
                        IloIntExpr capacite_ressources = model.intExpr();
                        for ( int job=0 ; job < data.jobs.size() ; job++) {
                            capacite_ressources = model.sum(model.prod(data.jobs.get(job).consumes.get(ressource),Y[job][machine][temps]),capacite_ressources);
                        }
                        model.addLe(capacite_ressources,data.machines.get(machine).resources.get(ressource));
                    }
                }
            }

            //Ici on ajoute la contrainte qui vérifie qu'un job ne peut être exécuter au maximum que sur une seule machine
            for(int job=0 ; job< data.jobs.size() ; job++)
            {
                IloIntExpr unicite_job = model.intExpr();
                for (int machine= 0 ; machine < data.nbMachine ; machine++)
                {
                    unicite_job = model.sum(model.diff(1,X[job][machine]),unicite_job);
                }
                model.addLe(unicite_job,1);
            }



        }
        catch (IloException e)
        {
            System.out.println("Concert exception caught: " + e);
        }
        return  model;
    }

    /**
     * Function to execute the algorithm Epsilon Constraint<br>
     * We use CPLEX to solve the problem
     *
     * @return a hashmap with keys as :
     * <ul>
     * <li>"solved" : boolean, whether the problem has been solved</li>
     * <li>"obj_value" : int, optimal value of the objectif function</li>
     * <li>"epsilon" : int, value of epsilon</li>
     * <li>"result_x" : int[], sequence of result</li>
     * </ul>
     */
    public HashMap<String, Object> execute(){

        //initiation of variables
        boolean solved = false;
        HashMap<String, Object> results = new HashMap<String, Object>();
        int nombreJobA = data.nbJobsA ; /*(data.nbJobs * this.pourcentagesParAgent.get(0))/100;*/
        int nombreJobB = data.nbJobs - data.nbJobsA ; /*(data.nbJobs * this.pourcentagesParAgent.get(0))/100;*/


        try {

            // création du model
            IloCplex model = createModel();
            double debut = model.getCplexTime();
            //  Agent A

            //Ici, on crée la fonction objective de l'agent A qui correspond à minimiser le nombre de jobs rejetés
            numberJobA = model.intExpr();
            for(int job=0 ; job< nombreJobA; job++) //  pourcentagesParAgent[0] : le % de l'agent A
            {
                for (int machine= 0 ; machine <data.nbMachine; machine++)
                {
                    numberJobA = model.sum(X[job][machine],numberJobA);
                }
            }
            IloObjective objectiveA = model.minimize(numberJobA);


            //Agent B
            //Ici, on crée la fonction objective de l'agent B qui correspond à minimiser le nombre de jobs rejetés
            numberJobB = model.intExpr();
            for(int job=nombreJobA; job< data.nbJobs; job++) //  pourcentagesParAgent[0] : le % de l'agent A
            {
                for (int machine= 0 ; machine <data.nbMachine; machine++)
                {
                    numberJobB= model.sum(X[job][machine],numberJobB);
                }
            }
            IloObjective objectiveB = model.minimize(numberJobB);

            // Valeur de A
            model.add(objectiveA);
            //cplex.setModel(Modelisation);
            //cplex.extract(Modelisation);
            //model.setParam(IloCplex.Param.TimeLimit,1800);
            model.setParam(IloCplex.Param.Emphasis.MIP,4);
            int valeurObjA = 0;
            if (model.solve()) {
                valeurObjA = (int) model.getObjValue();
                //System.out.println("valeurA : " + valeurObjA);
            }
            model.remove(objectiveA);

            //Valeur de B
            model.add(objectiveB);
            //cplex.setModel(Modelisation);
            //cplex.extract(Modelisation);
            //model.setParam(IloCplex.Param.TimeLimit,1800);
            model.setParam(IloCplex.Param.Emphasis.MIP,4);
            int valeurObjB = 0;
            if (model.solve()) {
                valeurObjB = (int) model.getObjValue();
            }
            model.remove(objectiveB);

            // Calcul de la limite
            IloIntExpr expAbound ;
            expAbound  = model.addLe(numberJobB,valeurObjB);
            model.add((IloAddable) expAbound);
            model.add(objectiveA);
            model.setParam(IloCplex.Param.Emphasis.MIP,4);
            int valeurObjAbound = 0;
            if (model.solve()) {
                valeurObjAbound = (int) model.getObjValue();
                // System.out.println("valeurAbound : " + valeurObjAbound);
            }
            model.remove(objectiveA);


            // Le calcul de la solution optimale avec la methode epsilon -contrainte
            paretoFront = new HashSet<ParetoSolution>();
            for(int i=valeurObjA ; i<=  valeurObjAbound; i++)
            {
                ArrayList<ArrayList<ArrayList<Integer>>> jobsOrdonnances = new ArrayList<>(2);

                IloCplex model1 = createModel();
                int val = i ;
                IloIntExpr exp ;
                exp = model1.addLe(numberJobA,val);
                model1.add((IloAddable) exp);
                //IloObjective objective = model.minimize(numberJobB);
                model1.add(objectiveB);

                //model1.setParam(IloCplex.Param.TimeLimit,1800);
                model1.setParam(IloCplex.Param.Emphasis.MIP,4);
                if (model1.solve()) {
                    int valeur_B = (int)model1.getObjValue();
                    int start = 0 ;
                    int end = 0 ;
                    ParetoSolution paretoSolution = new ParetoSolution();

                    for ( int agent = 0 ; agent < 2 ; agent++) {

                        /*(data.nbJobs * (pourcentagesParAgent.get(agent))/100);*/
                        end = start + ((agent==0)? nombreJobA:nombreJobB);
                        ArrayList<ArrayList<Integer>> ma = new ArrayList<>();
                        jobsOrdonnances.add(ma);
                        for ( int machine= 0 ; machine < data.nbMachine; machine++) {
                            ArrayList<Integer> jobsMachines = new ArrayList<>();
                            for(int job=start; job<end ; job++){
                                if (model1.getValue(X[job][machine]) == 0.0)
                                {
                                    //System.out.print("Agent : "+agent+" - M : "+machine+" - J : "+job+"\t");
                                    jobsMachines.add(job);
                                }
                            }
                            jobsOrdonnances.get(agent).add(jobsMachines);
                            // System.out.println();
                        }
                        start = end;
                    }

                    for ( int machine= 0 ; machine < data.nbMachine; machine++) {
                        paretoSolution.sequence.add(jobsOrdonnances.get(0).get(machine));
                        paretoSolution.sequence.add(jobsOrdonnances.get(1).get(machine));
                    }

                    paretoFront.add(paretoSolution);

                    for (int machine= 0 ; machine < data.nbMachine ; machine++) {
                        calculA += jobsOrdonnances.get(0).get(machine).size();
                        calculB += jobsOrdonnances.get(1).get(machine).size();
                    }
                    paretoSolution.valueObjA = data.nbJobsA - calculA;
                    paretoSolution.valueObjB = (data.nbJobs - data.nbJobsA) - calculB;
                    calculA =0;
                    calculB = 0;

                    double fin = model1.getCplexTime();
                    String status = model1.getStatus().toString();

                    model1.clearModel();

                    model1.end();

                    Commun.sauvegarderResultatMultiCritere(this.data,jobsOrdonnances,"Méthode "+status+"Indexée Temps ",this.nomfichierResultat,fin-debut,paretoSolution.valueObjA,paretoSolution.valueObjB);

                }

            }

            model.clearModel();
            model.end();

        } catch (IloException e) {
            e.printStackTrace();
        }
        return results;
    }

    @Override
    /**
     * Cette méthode permet de générer un front de Patero de la méthode exacte
     */
    public Set<ParetoSolution> generateParetoFront() {
        Set<ParetoSolution> paretoFrontv = new HashSet<ParetoSolution>();

        HashMap<String, Object> result = this.execute();
        //paretoFrontv = paretoFront;
        paretoFrontv = NondominatedSetConstructor.frontPareto_NonDominance(paretoFront);
        return paretoFrontv;
    }





}
