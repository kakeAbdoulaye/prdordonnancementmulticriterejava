package fr.polytechtours.prd.multiagent.Model.Modelisation;

import java.util.ArrayList;
import java.util.HashMap;

import fr.polytechtours.prd.multiagent.Model.ApproacheMethod.Heuristique.Greedy;
import fr.polytechtours.prd.multiagent.Model.Util.Commun;

/**
 * Instance to use for parameters<br>
 * In order to execute the algorithms, user should create this object by initializing all necessary parameters
 * 
 * @author Boyang Wang
 * @version 2.0
 * @since Mars 23, 2018
 *
 */
public class Instance {
	/**
	 * list of jobs
	 */
	public ArrayList<Job> jobs = new ArrayList<Job>();
	/**
	 * machine with resources
	 */
	public ArrayList<Machine> machines = new ArrayList<Machine>();

	/**
	 * number of jobs
	 */
	public int nbJobs = 0;
	/**
	 * number of jobs of agent A
	 */
	public int nbJobsA = 0;

	/**
	 * number of available machine
	 */
	public int nbMachine;
	/**
	 * max end time of all jobs
	 */
	public int maxEnd = 0;
	/**
	 * agent label
	 */
	public String agent = "A";
	/**
	 * value of epsilon used by epsilon constraint
	 */
	public int epsilon = 0;
	/**
	 * value of weight used by linear combination
	 */
	public double weight = 0.0;
	/**
	 * type of greedy algorithm
	 */
	public int typeGreedy = Greedy.GREEDY_EPSILON;

	/**
	 * Name of the data source file
	 */
	public String filename;


	/**
	 * Constructeur de la classe
	 * @param instanceFilename , le nom du fichier d'instance
	 */
	public Instance(String instanceFilename)
	{
		HashMap<String, Object> hashmap = Commun.ReadDataFromFile(instanceFilename, Job.TYPE_FACTOR_SORT_MAX);//instance-80-2-3-40.data ,instance-8-2-3-1_2agent_2machine.data
		jobs = (ArrayList<Job>) hashmap.get("jobs");
		nbMachine = (int)hashmap.get("nombreMachine");
		machines = (ArrayList<Machine>) hashmap.get("machine");
		nbJobs = (int) hashmap.get("numJob");
		nbJobsA = (int) hashmap.get("numJobAgentA");
		filename = (String)hashmap.get("filename");
		maxEnd = Commun.getMaxEnd(jobs);
	}

}
