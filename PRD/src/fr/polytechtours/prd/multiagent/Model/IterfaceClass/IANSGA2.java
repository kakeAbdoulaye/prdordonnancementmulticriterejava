package fr.polytechtours.prd.multiagent.Model.IterfaceClass;

import fr.polytechtours.prd.multiagent.Model.ApproacheMethod.MetaHeuristique.CommonModelisation.Individual;
import fr.polytechtours.prd.multiagent.Model.ApproacheMethod.MetaHeuristique.ModelisationMachine.IndividualMachine;
import fr.polytechtours.prd.multiagent.Model.Modelisation.Job;

import java.util.ArrayList;

/**
 * Cette Interface, montre les différentes étapes lors d'un alogorithme génétique
 * @author kake Abdoulaye
 */

public interface IANSGA2 {

    /**
     * Method to initialize the population<br>
     * Here we use random strategy to initialize the genes of an individual:
     * <ul>
     * <li>for jobs of agent A, choose randomly one job as scheduled.</li>
     * <li>for jobs of agent B, choose randomly one job as scheduled.</li>
     * <li>to verify if this sequence of gene already exists</li>
     * <li>if this gene already exists, re-do the random process. if not, add the individual into the population</li>
     * </ul>
     * We loop the process above until the population is filled
     * @param jobs jobs to schedule
     * @param nbJobs number of all jobs
     * @param nbJobsA number of jobs of agent A
     * @return population initialized
     */
    public ArrayList<IndividualMachine> initGroup(ArrayList<Job> jobs, int nbJobs, int nbJobsA);


    /**
     * selection operation to select one individual to attend the crossover
     * <ol>
     * <li>Select two individual p and q randomly in the current population</li>
     * <li>If p dominates q, return p. Else return q.</li>
     * </ol>
     * @param pop population
     * @param nombreP number of individual we chose to compare before we return the best one
     * @return index of individual chosen
     */
    public int selection(ArrayList<IndividualMachine> pop,int nombreP);


    /**
     * crossover operation to create new children<br>
     * <ol>
     * <li>Define the number of gene to cross over randomly</li>
     * <li>Choose the index randomly of crossover</li>
     * <li>For the index, do the crossover operation</li>
     * </ol>
     * @param dad dad individual
     * @param mum mom individual
     * @return two children created by crossover
     */
    public Individual[] crossOver(IndividualMachine dad, IndividualMachine mum);


    /**
     * Mutation operation<br>
     * Choose randomly one position to do the mutation operation<br>
     * Mutation means changing 1 to 0 and 0 to 1
     *
     * @param children children created after crossover
     * @return children after mutation
     */
    public Individual[] mutation(IndividualMachine[] children);


    /**
     * remove the duplicate individuals from the population
     *
     * @param pop population
     * @return population without duplicate
     */
    public ArrayList<IndividualMachine> removeDuplicateFromArray(ArrayList<IndividualMachine> pop);





}
