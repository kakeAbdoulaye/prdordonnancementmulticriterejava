package fr.polytechtours.prd.multiagent;

import fr.polytechtours.prd.multiagent.Model.Util.Commun;

/**
 * Main Class with main function
 * 
 * @author Boyang Wang
 * @version 1.0
 * @since Nov 20, 2018
 *
 */
public class Main {

	/**
	 * main function to create data and write into a file 
	 * @param args parameters
	 */
	public static void main(String[] args) {
		Commun.createRandomJobsAndResources(12, 3, 50, 3,1440,"instance-12-2-3-50_kake.data");
		//instance-100-2-3-40.data
	}

}
