package fr.polytechtours.prd.multiagent.View;

import fr.polytechtours.prd.multiagent.Controler.ApplicationCentraleControler;
import fr.polytechtours.prd.multiagent.Controler.GenerationInstanceControler;
import fr.polytechtours.prd.multiagent.Controler.ResolutionInstanceControler;

import javax.swing.*;


/**
 * Cette classe permet de lancer et d'afficher la vue de l'application centrale
 * @author Kake Abdoulaye
 */
public class MainView {

    public static void main(String [] args) throws ClassNotFoundException, UnsupportedLookAndFeelException, InstantiationException, IllegalAccessException {
        // Style de la fenetre
        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {

                /*ResolutionInstance vue = new ResolutionInstance();
                ResolutionInstanceControler controler = new ResolutionInstanceControler(vue);
                controler.affiche();*/

                ApplicationCentrale vue = new ApplicationCentrale();
                ApplicationCentraleControler controler = new ApplicationCentraleControler(vue);
                controler.affiche();

            }
        });
    }
}
