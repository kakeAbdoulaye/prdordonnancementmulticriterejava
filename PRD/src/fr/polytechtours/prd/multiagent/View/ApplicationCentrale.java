/*
 * Created by JFormDesigner on Mon Feb 18 21:37:19 CET 2019
 */

package fr.polytechtours.prd.multiagent.View;

import java.awt.*;
import javax.swing.*;
import javax.swing.GroupLayout;

/**
 * Cette classe représente la vue de l'application centrale
 * @author Kake Abdoulaye
 */
public class ApplicationCentrale extends JFrame {
    public ApplicationCentrale() {
        initComponents();
    }

    public JMenuItem getMenuItemGenerationInstance() {
        return menuItemGenerationInstance;
    }

    public JMenuItem getMenuItemResolutionInstance() {
        return menuItemResolutionInstance;
    }

    public JMenuItem getMenuItemComparaison() {
        return menuItemComparaison;
    }

    public JMenuItem getMenuItemQuitter() {
        return menuItemQuitter;
    }

    public JMenu getMenuAide() {
        return menuAide;
    }

    public JMenuItem getMenuItemAuteur() {
        return menuItemAuteur;
    }

    /**
     * Initialisation de l'interface
     */
    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Kake Abdoulaye
        menuBar1 = new JMenuBar();
        menuAction = new JMenu();
        menuItemGenerationInstance = new JMenuItem();
        menuItemResolutionInstance = new JMenuItem();
        menuItemComparaison = new JMenuItem();
        menuItemAuteur = new JMenuItem();
        menuItemQuitter = new JMenuItem();
        menuAide = new JMenu();
        separator1 = new JPopupMenu.Separator();

        //======== this ========
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        Container contentPane = getContentPane();

        //======== menuBar1 ========
        {

            //======== menuAction ========
            {
                menuAction.setText("Action");
                menuAction.setIcon(new ImageIcon("Ressource/Icons/icons8-bouclier-35.png"));

                //---- menuItemGenerationInstance ----
                menuItemGenerationInstance.setText("G\u00e9n\u00e9ration d'Instance");
                menuItemGenerationInstance.setIcon(new ImageIcon("Ressource/Icons/icons8-biotech-35.png"));
                menuAction.add(menuItemGenerationInstance);

                //---- menuItemResolutionInstance ----
                menuItemResolutionInstance.setText("R\u00e9solution des Instances");
                menuItemResolutionInstance.setIcon(new ImageIcon("Ressource/Icons/icons8-tube-\u00e0-essai-35.png"));
                menuAction.add(menuItemResolutionInstance);

                //---- menuItemComparaison ----
                menuItemComparaison.setText("Comparaison des r\u00e9sultats");
                menuItemComparaison.setIcon(new ImageIcon("Ressource/Icons/icons8-statistiques-30.png"));
                menuAction.add(menuItemComparaison);

                //---- menuItemAuteur ----
                menuItemAuteur.setText("Auteurs");
                menuItemAuteur.setIcon(new ImageIcon("Ressource/Icons/icons8-utilisateur-homme-35.png"));
                menuAction.add(menuItemAuteur);

                //---- menuItemQuitter ----
                menuItemQuitter.setText("Quitter l'application");
                menuItemQuitter.setIcon(new ImageIcon("Ressource/Icons/icons8-fermer-30.png"));
                menuAction.add(menuItemQuitter);
            }
            menuBar1.add(menuAction);

            //======== menuAide ========
            {
                menuAide.setText("Aide");
                menuAide.setIcon(new ImageIcon("Ressource/Icons/icons8-bouee-de-sauvetage-35.png"));
            }
            menuBar1.add(menuAide);
            menuBar1.add(separator1);
        }
        setJMenuBar(menuBar1);

        GroupLayout contentPaneLayout = new GroupLayout(contentPane);
        contentPane.setLayout(contentPaneLayout);
        contentPaneLayout.setHorizontalGroup(
            contentPaneLayout.createParallelGroup()
                .addGap(0, 748, Short.MAX_VALUE)
        );
        contentPaneLayout.setVerticalGroup(
            contentPaneLayout.createParallelGroup()
                .addGap(0, 542, Short.MAX_VALUE)
        );
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Kake Abdoulaye
    private JMenuBar menuBar1;
    private JMenu menuAction;
    private JMenuItem menuItemGenerationInstance;
    private JMenuItem menuItemResolutionInstance;
    private JMenuItem menuItemComparaison;
    private JMenuItem menuItemAuteur;
    private JMenuItem menuItemQuitter;
    private JMenu menuAide;
    private JPopupMenu.Separator separator1;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
