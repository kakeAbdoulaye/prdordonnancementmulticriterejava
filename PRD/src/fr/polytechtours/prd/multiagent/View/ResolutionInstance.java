/*
 * Created by JFormDesigner on Tue Feb 19 13:44:19 CET 2019
 */

package fr.polytechtours.prd.multiagent.View;

import java.awt.*;
import javax.swing.*;
import javax.swing.GroupLayout;
import javax.swing.border.*;
import javax.swing.tree.*;

/**
 * Cette classe réprésente la vue lier à la résolution d'instance
 * @author Kake Abdoulaye
 */
public class ResolutionInstance extends JDialog {
    /**
     * Constructeur de la classe
     * @param owner , la fénètre mère
     */
    public ResolutionInstance(Frame owner) {
        super(owner);
        initComponents();
    }


    public JTextField getTextFieldCheminFichierChoisi() {
        return textFieldCheminFichierChoisi;
    }

    public JButton getButtonChoisirFichier() {
        return buttonChoisirFichier;
    }

    public JCheckBox getCheckBoxResIndexTemp() {
        return checkBoxResIndexTemp;
    }

    public JCheckBox getCheckBoxResIndexJob() {
        return checkBoxResIndexJob;
    }

    public JCheckBox getCheckBoxTri1AffMBM() {
        return checkBoxTri1AffMBM;
    }

    public JCheckBox getCheckBoxTri1AffPMC() {
        return checkBoxTri1AffPMC;
    }

    public JCheckBox getCheckBoxTri2AffMBM() {
        return checkBoxTri2AffMBM;
    }

    public JCheckBox getCheckBoxTri2AffPMC() {
        return checkBoxTri2AffPMC;
    }

    public JCheckBox getCheckBoxTri3AffMBM() {
        return checkBoxTri3AffMBM;
    }

    public JCheckBox getCheckBoxTri3AffPMC() {
        return checkBoxTri3AffPMC;
    }

    public JCheckBox getCheckBoxNSGA2() {
        return checkBoxNSGA2;
    }

    public JButton getButtonToutDecocher() {
        return buttonToutDecocher;
    }

    public JButton getButtonToutCocher() {
        return buttonToutCocher;
    }

    public JButton getButtonResoudre() {
        return ButtonResoudre;
    }

    public JButton getButtonAnnuler() {
        return ButtonAnnuler;
    }


    /**
     * Cette méthode permet d'initialiser le panel et les boutons de la vue
     */
    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Kake Abdoulaye
        dialogPane = new JPanel();
        contentPanel = new JPanel();
        label1 = new JLabel();
        label2 = new JLabel();
        textFieldCheminFichierChoisi = new JTextField();
        buttonChoisirFichier = new JButton();
        panel2 = new JPanel();
        buttonToutDecocher = new JButton();
        buttonToutCocher = new JButton();
        label4 = new JLabel();
        checkBoxResIndexTemp = new JCheckBox();
        checkBoxResIndexJob = new JCheckBox();
        label5 = new JLabel();
        label6 = new JLabel();
        label7 = new JLabel();
        checkBoxTri1AffMBM = new JCheckBox();
        checkBoxTri1AffPMC = new JCheckBox();
        label8 = new JLabel();
        checkBoxTri2AffMBM = new JCheckBox();
        checkBoxTri2AffPMC = new JCheckBox();
        label9 = new JLabel();
        checkBoxTri3AffMBM = new JCheckBox();
        checkBoxTri3AffPMC = new JCheckBox();
        label10 = new JLabel();
        checkBoxNSGA2 = new JCheckBox();
        label3 = new JLabel();
        label11 = new JLabel();
        buttonBar = new JPanel();
        ButtonResoudre = new JButton();
        ButtonAnnuler = new JButton();

        //======== this ========
        setAutoRequestFocus(false);
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setModal(true);
        setResizable(false);
        Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());

        //======== dialogPane ========
        {
            dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));

            // JFormDesigner evaluation mark
            dialogPane.setBorder(new javax.swing.border.CompoundBorder(
                new javax.swing.border.TitledBorder(new javax.swing.border.EmptyBorder(0, 0, 0, 0),
                    " ", javax.swing.border.TitledBorder.CENTER,
                    javax.swing.border.TitledBorder.BOTTOM, new java.awt.Font("Dialog", java.awt.Font.BOLD, 12),
                    java.awt.Color.red), dialogPane.getBorder())); dialogPane.addPropertyChangeListener(new java.beans.PropertyChangeListener(){public void propertyChange(java.beans.PropertyChangeEvent e){if("border".equals(e.getPropertyName()))throw new RuntimeException();}});

            dialogPane.setLayout(new BorderLayout());

            //======== contentPanel ========
            {
                contentPanel.setPreferredSize(new Dimension(1120, 559));
                contentPanel.setMinimumSize(new Dimension(1105, 542));

                //---- label1 ----
                label1.setText("R\u00e9solution D'instances");
                label1.setFont(label1.getFont().deriveFont(Font.BOLD|Font.ITALIC, label1.getFont().getSize() + 11f));

                //---- label2 ----
                label2.setText("Fichier Choisi : ");
                label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD));

                //---- buttonChoisirFichier ----
                buttonChoisirFichier.setText("Choisir");

                //======== panel2 ========
                {
                    panel2.setBackground(SystemColor.window);

                    //---- buttonToutDecocher ----
                    buttonToutDecocher.setText("Tout D\u00e9cocher");

                    //---- buttonToutCocher ----
                    buttonToutCocher.setText("Tout Cocher");

                    //---- label4 ----
                    label4.setText("M\u00e9thodes Exactes");

                    //---- checkBoxResIndexTemp ----
                    checkBoxResIndexTemp.setText("R\u00e9solution exact index\u00e9e  Temps");

                    //---- checkBoxResIndexJob ----
                    checkBoxResIndexJob.setText("R\u00e9solution exact index\u00e9e Job");

                    //---- label5 ----
                    label5.setText("M\u00e9thode Approch\u00e9es");

                    //---- label6 ----
                    label6.setText("Heuristiques");

                    //---- label7 ----
                    label7.setText("Trie Cmax  bas\u00e9 sur la somme des ressource de chaque Job");

                    //---- checkBoxTri1AffMBM ----
                    checkBoxTri1AffMBM.setText("Affectation machine par machine");

                    //---- checkBoxTri1AffPMC ----
                    checkBoxTri1AffPMC.setText("Affectation priorisant la machine la moins charg\u00e9e");

                    //---- label8 ----
                    label8.setText("Trie  Cmax bas\u00e9 sur la ressource maximale de chaque Job");

                    //---- checkBoxTri2AffMBM ----
                    checkBoxTri2AffMBM.setText("Affectation machine par machine");

                    //---- checkBoxTri2AffPMC ----
                    checkBoxTri2AffPMC.setText("Affectation priorisant la machine la moins charg\u00e9e");

                    //---- label9 ----
                    label9.setText("Trie sur La dur\u00e9e de chaque Job");

                    //---- checkBoxTri3AffMBM ----
                    checkBoxTri3AffMBM.setText("Affectation machine par machine");

                    //---- checkBoxTri3AffPMC ----
                    checkBoxTri3AffPMC.setText("Affectation priorisant la machine la moins charg\u00e9e");

                    //---- label10 ----
                    label10.setText("M\u00e9ta-Heuristiques");

                    //---- checkBoxNSGA2 ----
                    checkBoxNSGA2.setText("Algorithme G\u00e9n\u00e9tique NSGA2");

                    //---- label3 ----
                    label3.setText("Fonctionnalit\u00e9");
                    label3.setFont(label3.getFont().deriveFont(Font.BOLD|Font.ITALIC, label3.getFont().getSize() + 8f));

                    GroupLayout panel2Layout = new GroupLayout(panel2);
                    panel2.setLayout(panel2Layout);
                    panel2Layout.setHorizontalGroup(
                        panel2Layout.createParallelGroup()
                            .addGroup(panel2Layout.createSequentialGroup()
                                .addGroup(panel2Layout.createParallelGroup()
                                    .addGroup(panel2Layout.createSequentialGroup()
                                        .addGap(33, 33, 33)
                                        .addGroup(panel2Layout.createParallelGroup()
                                            .addComponent(label4)
                                            .addComponent(label5)))
                                    .addGroup(panel2Layout.createSequentialGroup()
                                        .addGap(105, 105, 105)
                                        .addGroup(panel2Layout.createParallelGroup()
                                            .addComponent(label7)
                                            .addGroup(panel2Layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                                                .addComponent(label8)
                                                .addGroup(panel2Layout.createParallelGroup()
                                                    .addComponent(checkBoxTri1AffPMC)
                                                    .addComponent(checkBoxTri1AffMBM))
                                                .addGroup(panel2Layout.createParallelGroup()
                                                    .addComponent(checkBoxTri2AffPMC)
                                                    .addComponent(checkBoxTri2AffMBM)))
                                            .addComponent(label9)))
                                    .addGroup(panel2Layout.createSequentialGroup()
                                        .addGap(112, 112, 112)
                                        .addComponent(checkBoxNSGA2))
                                    .addGroup(panel2Layout.createSequentialGroup()
                                        .addGap(135, 135, 135)
                                        .addGroup(panel2Layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                                            .addGroup(panel2Layout.createSequentialGroup()
                                                .addComponent(buttonToutDecocher)
                                                .addGap(18, 18, 18)
                                                .addComponent(buttonToutCocher)
                                                .addGap(54, 54, 54))
                                            .addGroup(panel2Layout.createParallelGroup()
                                                .addComponent(checkBoxTri3AffMBM)
                                                .addComponent(checkBoxTri3AffPMC)))))
                                .addGap(0, 6, Short.MAX_VALUE))
                            .addGroup(panel2Layout.createSequentialGroup()
                                .addGroup(panel2Layout.createParallelGroup()
                                    .addGroup(panel2Layout.createSequentialGroup()
                                        .addGap(70, 70, 70)
                                        .addGroup(panel2Layout.createParallelGroup()
                                            .addComponent(label10)
                                            .addComponent(checkBoxResIndexTemp)
                                            .addComponent(label6)
                                            .addComponent(checkBoxResIndexJob)))
                                    .addGroup(panel2Layout.createSequentialGroup()
                                        .addGap(130, 130, 130)
                                        .addComponent(label3, GroupLayout.PREFERRED_SIZE, 165, GroupLayout.PREFERRED_SIZE)))
                                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    );
                    panel2Layout.setVerticalGroup(
                        panel2Layout.createParallelGroup()
                            .addGroup(GroupLayout.Alignment.TRAILING, panel2Layout.createSequentialGroup()
                                .addComponent(label3)
                                .addGap(9, 9, 9)
                                .addComponent(label4)
                                .addGap(5, 5, 5)
                                .addComponent(checkBoxResIndexTemp)
                                .addGap(4, 4, 4)
                                .addComponent(checkBoxResIndexJob)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(label5)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(label6)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(label7)
                                .addGap(3, 3, 3)
                                .addComponent(checkBoxTri1AffMBM)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(checkBoxTri1AffPMC)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(label8)
                                .addGap(5, 5, 5)
                                .addComponent(checkBoxTri2AffMBM)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(checkBoxTri2AffPMC)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(label9)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(checkBoxTri3AffMBM)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(checkBoxTri3AffPMC)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(label10)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(checkBoxNSGA2)
                                .addGap(18, 18, 18)
                                .addGroup(panel2Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                    .addComponent(buttonToutDecocher)
                                    .addComponent(buttonToutCocher))
                                .addContainerGap(80, Short.MAX_VALUE))
                    );
                }

                //---- label11 ----
                label11.setIcon(new ImageIcon("Ressource/Icons/icons8-tube-\u00e0-essai-100.png"));

                GroupLayout contentPanelLayout = new GroupLayout(contentPanel);
                contentPanel.setLayout(contentPanelLayout);
                contentPanelLayout.setHorizontalGroup(
                    contentPanelLayout.createParallelGroup()
                        .addGroup(contentPanelLayout.createSequentialGroup()
                            .addGap(15, 15, 15)
                            .addComponent(panel2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                            .addGroup(contentPanelLayout.createParallelGroup()
                                .addGroup(contentPanelLayout.createSequentialGroup()
                                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(contentPanelLayout.createParallelGroup()
                                        .addGroup(GroupLayout.Alignment.TRAILING, contentPanelLayout.createSequentialGroup()
                                            .addComponent(label2)
                                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 26, Short.MAX_VALUE)
                                            .addComponent(textFieldCheminFichierChoisi, GroupLayout.PREFERRED_SIZE, 416, GroupLayout.PREFERRED_SIZE))
                                        .addComponent(buttonChoisirFichier, GroupLayout.Alignment.TRAILING))
                                    .addGap(29, 29, 29))
                                .addGroup(GroupLayout.Alignment.TRAILING, contentPanelLayout.createSequentialGroup()
                                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 241, Short.MAX_VALUE)
                                    .addComponent(label11, GroupLayout.PREFERRED_SIZE, 132, GroupLayout.PREFERRED_SIZE)
                                    .addGap(213, 213, 213))))
                        .addGroup(GroupLayout.Alignment.TRAILING, contentPanelLayout.createSequentialGroup()
                            .addContainerGap(703, Short.MAX_VALUE)
                            .addComponent(label1, GroupLayout.PREFERRED_SIZE, 289, GroupLayout.PREFERRED_SIZE)
                            .addGap(128, 128, 128))
                );
                contentPanelLayout.setVerticalGroup(
                    contentPanelLayout.createParallelGroup()
                        .addGroup(contentPanelLayout.createSequentialGroup()
                            .addComponent(label1)
                            .addGap(23, 23, 23)
                            .addGroup(contentPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(textFieldCheminFichierChoisi, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addComponent(label2))
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(buttonChoisirFichier)
                            .addGap(101, 101, 101)
                            .addComponent(label11, GroupLayout.PREFERRED_SIZE, 211, GroupLayout.PREFERRED_SIZE)
                            .addContainerGap(158, Short.MAX_VALUE))
                        .addGroup(GroupLayout.Alignment.TRAILING, contentPanelLayout.createSequentialGroup()
                            .addGap(0, 37, Short.MAX_VALUE)
                            .addComponent(panel2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                );
            }
            dialogPane.add(contentPanel, BorderLayout.WEST);

            //======== buttonBar ========
            {
                buttonBar.setBorder(new EmptyBorder(12, 0, 0, 0));
                buttonBar.setMinimumSize(new Dimension(1115, 42));
                buttonBar.setPreferredSize(new Dimension(1115, 42));
                buttonBar.setLayout(new GridBagLayout());
                ((GridBagLayout)buttonBar.getLayout()).columnWidths = new int[] {1116, 85, 80};
                ((GridBagLayout)buttonBar.getLayout()).columnWeights = new double[] {1.0, 0.0, 0.0};

                //---- ButtonResoudre ----
                ButtonResoudre.setText("Resoudre");
                buttonBar.add(ButtonResoudre, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 5), 0, 0));

                //---- ButtonAnnuler ----
                ButtonAnnuler.setText("Annuler");
                buttonBar.add(ButtonAnnuler, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 0), 0, 0));
            }
            dialogPane.add(buttonBar, BorderLayout.SOUTH);
        }
        contentPane.add(dialogPane, BorderLayout.CENTER);
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Kake Abdoulaye
    private JPanel dialogPane;
    private JPanel contentPanel;
    private JLabel label1;
    private JLabel label2;
    private JTextField textFieldCheminFichierChoisi;
    private JButton buttonChoisirFichier;
    private JPanel panel2;
    private JButton buttonToutDecocher;
    private JButton buttonToutCocher;
    private JLabel label4;
    private JCheckBox checkBoxResIndexTemp;
    private JCheckBox checkBoxResIndexJob;
    private JLabel label5;
    private JLabel label6;
    private JLabel label7;
    private JCheckBox checkBoxTri1AffMBM;
    private JCheckBox checkBoxTri1AffPMC;
    private JLabel label8;
    private JCheckBox checkBoxTri2AffMBM;
    private JCheckBox checkBoxTri2AffPMC;
    private JLabel label9;
    private JCheckBox checkBoxTri3AffMBM;
    private JCheckBox checkBoxTri3AffPMC;
    private JLabel label10;
    private JCheckBox checkBoxNSGA2;
    private JLabel label3;
    private JLabel label11;
    private JPanel buttonBar;
    private JButton ButtonResoudre;
    private JButton ButtonAnnuler;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
