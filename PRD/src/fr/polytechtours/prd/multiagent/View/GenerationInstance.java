/*
 * Created by JFormDesigner on Mon Feb 18 20:20:14 CET 2019
 */

package fr.polytechtours.prd.multiagent.View;

import fr.polytechtours.prd.multiagent.Controler.GenerationInstanceControler;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.GroupLayout;
import javax.swing.LayoutStyle;
import javax.swing.border.*;

/**
 * Cette classe représente la vue de la génération d'instance
 * @author Kake Abdoulaye
 */
public class GenerationInstance extends JDialog {

    /**
     *  Constructeur de la classe
     * @param owner , la fénètre mère
     */
    public GenerationInstance(Frame owner) {
        super(owner);
        initComponents();
    }


    /**
     * Cette méthode permet d'initialiser le panel et les boutons de la vue
     */
    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Kake Abdoulaye
        dialogPane = new JPanel();
        contentPanel = new JPanel();
        labelTitre = new JLabel();
        labelNombreJobs = new JLabel();
        spinnerNombreJobs = new JSpinner();
        labelPourcentageA = new JLabel();
        spinnerPourcentageAgentA = new JSpinner();
        labelNombreRessource = new JLabel();
        spinnerNombreRessource = new JSpinner();
        labelNombreMachine = new JLabel();
        spinnerNombreMachine = new JSpinner();
        spinnerNombreInstance = new JSpinner();
        labelNombreInstance = new JLabel();
        spinnerHorizon = new JSpinner();
        labelHorizon = new JLabel();
        comboBoxChoixUnite = new JComboBox<>();
        labelPourcentageSigne = new JLabel();
        labelInfoBulle = new JLabel();
        buttonBar = new JPanel();
        okButton = new JButton();
        cancelButton = new JButton();

        //======== this ========
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setModal(true);
        setResizable(false);
        setTitle("G\u00e9n\u00e9ration d'instance de Jobs");
        Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());

        //======== dialogPane ========
        {
            dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));

            // JFormDesigner evaluation mark
            dialogPane.setBorder(new javax.swing.border.CompoundBorder(
                new javax.swing.border.TitledBorder(new javax.swing.border.EmptyBorder(0, 0, 0, 0),
                    " ", javax.swing.border.TitledBorder.CENTER,
                    javax.swing.border.TitledBorder.BOTTOM, new java.awt.Font("Dialog", java.awt.Font.BOLD, 12),
                    java.awt.Color.red), dialogPane.getBorder())); dialogPane.addPropertyChangeListener(new java.beans.PropertyChangeListener(){public void propertyChange(java.beans.PropertyChangeEvent e){if("border".equals(e.getPropertyName()))throw new RuntimeException();}});

            dialogPane.setLayout(new BorderLayout());

            //======== contentPanel ========
            {

                //---- labelTitre ----
                labelTitre.setText("           G\u00e9n\u00e9ration d'Instance");
                labelTitre.setFont(new Font("Ubuntu", Font.BOLD|Font.ITALIC, labelTitre.getFont().getSize() + 21));

                //---- labelNombreJobs ----
                labelNombreJobs.setText("Nombre de Jobs :");
                labelNombreJobs.setFont(labelNombreJobs.getFont().deriveFont(Font.BOLD|Font.ITALIC));

                //---- spinnerNombreJobs ----
                spinnerNombreJobs.setModel(new SpinnerNumberModel(1, 1, null, 1));

                //---- labelPourcentageA ----
                labelPourcentageA.setText("Pourcentage de Jobs de l'agent A : ");
                labelPourcentageA.setFont(labelPourcentageA.getFont().deriveFont(Font.BOLD|Font.ITALIC));

                //---- spinnerPourcentageAgentA ----
                spinnerPourcentageAgentA.setModel(new SpinnerNumberModel(100, 1, 100, 1));

                //---- labelNombreRessource ----
                labelNombreRessource.setText("Nombre de Ressources : ");
                labelNombreRessource.setFont(labelNombreRessource.getFont().deriveFont(Font.BOLD|Font.ITALIC));

                //---- spinnerNombreRessource ----
                spinnerNombreRessource.setModel(new SpinnerNumberModel(3, 1, null, 1));

                //---- labelNombreMachine ----
                labelNombreMachine.setText("Nombre de Machines : ");
                labelNombreMachine.setFont(labelNombreMachine.getFont().deriveFont(Font.BOLD|Font.ITALIC));

                //---- spinnerNombreMachine ----
                spinnerNombreMachine.setModel(new SpinnerNumberModel(1, 1, null, 1));

                //---- spinnerNombreInstance ----
                spinnerNombreInstance.setModel(new SpinnerNumberModel(1, 1, null, 1));

                //---- labelNombreInstance ----
                labelNombreInstance.setText("Nombre d'Instance \u00e0 G\u00e9n\u00e9rer : ");
                labelNombreInstance.setFont(labelNombreInstance.getFont().deriveFont(Font.BOLD|Font.ITALIC));

                //---- spinnerHorizon ----
                spinnerHorizon.setModel(new SpinnerNumberModel(1440, 1, null, 1));

                //---- labelHorizon ----
                labelHorizon.setText("Horizon de Planification : ");
                labelHorizon.setFont(labelHorizon.getFont().deriveFont(Font.BOLD|Font.ITALIC));

                //---- comboBoxChoixUnite ----
                comboBoxChoixUnite.setModel(new DefaultComboBoxModel<>(new String[] {
                    "secondes",
                    "minutes",
                    "heures"
                }));

                //---- labelPourcentageSigne ----
                labelPourcentageSigne.setText("%");
                labelPourcentageSigne.setFont(labelPourcentageSigne.getFont().deriveFont(Font.BOLD|Font.ITALIC, labelPourcentageSigne.getFont().getSize() + 2f));

                //---- labelInfoBulle ----
                labelInfoBulle.setIcon(new ImageIcon("Ressource/Icons/icons8-info-30.png"));
                labelInfoBulle.setToolTipText("Si c'est un seul agent, mettez 100 %");

                GroupLayout contentPanelLayout = new GroupLayout(contentPanel);
                contentPanel.setLayout(contentPanelLayout);
                contentPanelLayout.setHorizontalGroup(
                    contentPanelLayout.createParallelGroup()
                        .addGroup(contentPanelLayout.createSequentialGroup()
                            .addGroup(contentPanelLayout.createParallelGroup()
                                .addGroup(contentPanelLayout.createSequentialGroup()
                                    .addGap(27, 27, 27)
                                    .addGroup(contentPanelLayout.createParallelGroup()
                                        .addComponent(labelNombreRessource)
                                        .addComponent(labelNombreMachine)
                                        .addComponent(labelNombreInstance)
                                        .addComponent(labelHorizon)
                                        .addGroup(contentPanelLayout.createSequentialGroup()
                                            .addGroup(contentPanelLayout.createParallelGroup()
                                                .addComponent(labelNombreJobs, GroupLayout.PREFERRED_SIZE, 157, GroupLayout.PREFERRED_SIZE)
                                                .addComponent(labelPourcentageA))
                                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(labelInfoBulle)))
                                    .addGap(20, 20, 20)
                                    .addGroup(contentPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                        .addComponent(spinnerNombreJobs, GroupLayout.DEFAULT_SIZE, 143, Short.MAX_VALUE)
                                        .addComponent(spinnerPourcentageAgentA, GroupLayout.DEFAULT_SIZE, 143, Short.MAX_VALUE)
                                        .addComponent(spinnerNombreRessource, GroupLayout.DEFAULT_SIZE, 143, Short.MAX_VALUE)
                                        .addComponent(spinnerNombreMachine, GroupLayout.DEFAULT_SIZE, 143, Short.MAX_VALUE)
                                        .addComponent(spinnerNombreInstance, GroupLayout.DEFAULT_SIZE, 143, Short.MAX_VALUE)
                                        .addComponent(spinnerHorizon, GroupLayout.DEFAULT_SIZE, 143, Short.MAX_VALUE))
                                    .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addGroup(contentPanelLayout.createParallelGroup()
                                        .addComponent(comboBoxChoixUnite, GroupLayout.DEFAULT_SIZE, 116, Short.MAX_VALUE)
                                        .addGroup(contentPanelLayout.createSequentialGroup()
                                            .addComponent(labelPourcentageSigne, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                                            .addGap(0, 91, Short.MAX_VALUE))))
                                .addGroup(contentPanelLayout.createSequentialGroup()
                                    .addContainerGap()
                                    .addComponent(labelTitre, GroupLayout.DEFAULT_SIZE, 587, Short.MAX_VALUE)))
                            .addContainerGap())
                );
                contentPanelLayout.setVerticalGroup(
                    contentPanelLayout.createParallelGroup()
                        .addGroup(contentPanelLayout.createSequentialGroup()
                            .addContainerGap()
                            .addComponent(labelTitre, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                            .addGap(33, 33, 33)
                            .addGroup(contentPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(labelNombreJobs, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                                .addComponent(spinnerNombreJobs, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(contentPanelLayout.createParallelGroup()
                                .addGroup(contentPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                    .addComponent(spinnerPourcentageAgentA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                    .addComponent(labelPourcentageSigne))
                                .addComponent(labelPourcentageA)
                                .addComponent(labelInfoBulle))
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(contentPanelLayout.createParallelGroup()
                                .addComponent(labelNombreRessource)
                                .addComponent(spinnerNombreRessource, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(contentPanelLayout.createParallelGroup()
                                .addComponent(labelNombreMachine)
                                .addComponent(spinnerNombreMachine, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(contentPanelLayout.createParallelGroup()
                                .addComponent(labelNombreInstance)
                                .addComponent(spinnerNombreInstance, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(contentPanelLayout.createParallelGroup()
                                .addComponent(labelHorizon, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE)
                                .addGroup(contentPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                    .addComponent(spinnerHorizon, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                    .addComponent(comboBoxChoixUnite, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                            .addContainerGap(18, Short.MAX_VALUE))
                );
            }
            dialogPane.add(contentPanel, BorderLayout.CENTER);

            //======== buttonBar ========
            {
                buttonBar.setBorder(new EmptyBorder(12, 0, 0, 0));
                buttonBar.setLayout(new GridBagLayout());
                ((GridBagLayout)buttonBar.getLayout()).columnWidths = new int[] {0, 85, 80};
                ((GridBagLayout)buttonBar.getLayout()).columnWeights = new double[] {1.0, 0.0, 0.0};

                //---- okButton ----
                okButton.setText("OK");
                buttonBar.add(okButton, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 5), 0, 0));

                //---- cancelButton ----
                cancelButton.setText("Annuler");
                buttonBar.add(cancelButton, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 0), 0, 0));
            }
            dialogPane.add(buttonBar, BorderLayout.SOUTH);
        }
        contentPane.add(dialogPane, BorderLayout.CENTER);
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Kake Abdoulaye
    private JPanel dialogPane;
    private JPanel contentPanel;
    private JLabel labelTitre;
    private JLabel labelNombreJobs;
    private JSpinner spinnerNombreJobs;
    private JLabel labelPourcentageA;
    private JSpinner spinnerPourcentageAgentA;
    private JLabel labelNombreRessource;
    private JSpinner spinnerNombreRessource;
    private JLabel labelNombreMachine;
    private JSpinner spinnerNombreMachine;
    private JSpinner spinnerNombreInstance;
    private JLabel labelNombreInstance;
    private JSpinner spinnerHorizon;
    private JLabel labelHorizon;
    private JComboBox<String> comboBoxChoixUnite;
    private JLabel labelPourcentageSigne;
    private JLabel labelInfoBulle;
    private JPanel buttonBar;
    private JButton okButton;
    private JButton cancelButton;

    public JSpinner getSpinnerNombreJobs() {
        return spinnerNombreJobs;
    }

    public JSpinner getSpinnerPourcentageAgentA() {
        return spinnerPourcentageAgentA;
    }

    public JSpinner getSpinnerNombreRessource() {
        return spinnerNombreRessource;
    }

    public JSpinner getSpinnerNombreMachine() {
        return spinnerNombreMachine;
    }

    public JSpinner getSpinnerNombreInstance() {
        return spinnerNombreInstance;
    }

    public JSpinner getSpinnerHorizon() {
        return spinnerHorizon;
    }

    public JComboBox<String> getComboBoxChoixUnite() {
        return comboBoxChoixUnite;
    }

    public JButton getOkButton() {
        return okButton;
    }

    public JButton getCancelButton() {
        return cancelButton;
    }
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
